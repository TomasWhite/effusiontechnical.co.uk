<?php
  require (".\\inc\\cardFunctions.php");
  require (".\\inc\\salesorder.php");

  $cardData = readSessionCardData();
  $boxColor = "green";
  
  //Page has been reached from a valid Pament details page display green
  function  initialiseSend() {
    global $cardData;
    
    $cardData['authStatus'] = 'send';
    saveSessionCardData($cardData);
  }
  
  // display red
  function  initialiseAuth() {
    global $cardData;
    global $boxColor;

    $boxColor = 'red';
    $cardData['authStatus'] = 'auth';
    saveSessionCardData($cardData);
  }

  // Start the auth
  function  processCard() {
    global $cardData;
    
    $order = new olct_salesOrder();
    if ($order->checkAllReservations() == false) {
      $order->clear();
      showError(LANG_TIMEOUT);
    }

    $cardData['authStatus'] = 'process';
    saveSessionCardData($cardData);
    session_start();

// -------------- MERCHANT PROCESSING ----------------------
    //YESPAY
    if (SITE_AUTHPROCESS == 'yespay') {
      include(CONFIG_PATH . '\inc\cc\yespayauth.php');
      if (!$authorised)
      {
        redirect('payment', 'e=' . $cardData['error'], true);
      }
    }

    //REALEX
    if (SITE_AUTHPROCESS == 'realex') {
      include(CONFIG_PATH . '\inc\cc\realexauth.php');
      if (!$authorised)
      {
        redirect('payment', 'e=' . $cardData['error'], true);
      }
    }   
// -------------- MERCHANT PROCESSING ----------------------

    if (!$order->post($cardData)) {
 //     if (SITE_AUTHPROCESS == 'yespay') {
 //       yesPayReversal($cardData);
 //     }
      redirect('payment', 'e=' . LANG_COULDNOTPROCESS, true);
    } else {
      redirect('success', '', true);
    }
  }

  if ($cardData['authStatus'] == 'init') {
    initialiseSend();
  } elseif ($cardData['authStatus'] == 'send') {
    initialiseAuth();
  } elseif ($cardData['authStatus'] == 'auth') {
    processCard();
  } else {  
    showError(LANG_PROCESSINTERUPT);
  }

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">

<head>
<title><?=SITE_METATITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta name="keywords" content="<?=CONFIG_METAKEYS . ',' . SITE_METAKEYS?>" />
<meta name="description" content="<?=SITE_METADESC?>" />
<meta name="author" content="<?=CONFIG_METAAUTHOR?>" />
<link rel="stylesheet" href="/sites/<?=SITE_CIRCUITCODE?>/stylesheet.css" type="text/css" media="all" />
<link rel="SHORTCUT ICON" href="/favicon.ico" type="image/x-icon" />
</head>

<?php
  if ($cardData['authStatus'] == 'send'){
?>
<script language="javascript"> 
  var timerID = null;
  var timeLoop = 0;
  var tick = 22;
  
  function timerAction() {
    var remaining;
    remaining = 20 - timeLoop;
    if (remaining < 0) {
      remaining = 0;
    } 
    table = document.all['time'].innerHTML = remaining;  
  }  

  function timer() {
    timeLoop++;
    timerAction();
    if (timeLoop == tick) {
      document.location = "<?=makeurl('process', '', true)?>" 
    } 
    timerID = window.setTimeout("timer()", 1000);    
  } 
</script>
<?php
} else {
?>
<script language="javascript"> 
  var timeLoop = 2;
  function timer() {
    timeLoop--;
    if (timeLoop == 0) {
      document.location = "<?=makeurl('process', '', true)?>" 
    } 
    timerID = window.setTimeout("timer()", 1000);    
  } 
</script>
<?php
}
?>

<body onLoad="setTimeout(<?=quoteStr('timer()')?>,1000)">

<?php
  if ($cardData['authStatus'] == 'send' && function_exists('htmlProcessStart')) {
    htmlProcessStart();
  } elseif ($cardData['authStatus'] == 'auth' && function_exists('htmlProcessAuth')) {
    htmlProcessAuth();
  } else {
?>

<table cellspacing="5" border="0" width="450" align="center" id="info" bgcolor="<?=$boxColor?>" style="border:2px solid black">
<?php
  if ($cardData['authStatus'] == 'send') {
?>
    <tr><td colspan="2"><font size="4">
      <strong><?=LANG_PROCESSAMOUNT?>&nbsp;<?=currencyStr($cardData['value'])?></strong><br/><br/><?=LANG_PROCESSSTART?>
    </font></td></tr><tr><td>&nbsp;</td></tr>
    <tr><td colspan="2" align="Center" style="font-size:20px"><span id="time">20</span></td></tr>
    <tr>
      <td align="left"><a href="<?=makeurl('cancel', '', true)?>"><img border="0" src="/images/<?=SITE_LANGUAGECODE?>/cancel.gif"></a></td>
      <td align="right"><a href="<?=makeurl('process', '', true)?>"><img border="0" src="/images/<?=SITE_LANGUAGECODE?>/continue.gif"></a></td>
    </tr>
<?
  } else {
?>
    <tr><td><font size="4">
      <strong><?=LANG_PROCESSAUTH?></strong><br/><br/><?=LANG_PROCESSWARN?><br/><br/>
    </font></td></tr>
<?
  }  
}  
?>
  
<body>
</body>
</html>