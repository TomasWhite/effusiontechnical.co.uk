<?php
  require_once('./inc/phpmailer/class.phpmailer.php' );
  require (".\\inc\\objects.php");

  $email          = strval($_SESSION['email']);
  $name           = strval($_SESSION['name']);
  $total          = intval($_SESSION['total']);
  $fee            = intval($_SESSION['fee']);
  $giftValue      = intval($_SESSION['giftCardValue']);
  $creditValue    = intval($total - $giftValue);
  $audit          = $_SESSION['audit'];
  $summary        = unserialize($_SESSION['orderSummary']);
  $loyaltyPoints  = intval($_SESSION['loyaltyPoints']);
  $loyaltyBalance = intval($_SESSION['loyaltyBalance']);
  $loyaltyNumber  = $_SESSION['loyaltyCardNumber'];
  
  if (!isset($audit)) {
    showError(LANG_ORDERPROCESSED);
  }
  
  $mailsuccess = true;

    include siteFile('siteemail.php');
    $mail = new mosPHPMailer(); 
    $mail->Host     = CONFIG_EMAILHOST;
    $mail->Mailer   = "smtp";    
    $mail->SMTPAuth = true; // turn on SMTP authentication
    $mail->Username = CONFIG_EMAILUSER; // SMTP username
    $mail->Password = CONFIG_EMAILPASS; // SMTP password
    $mail->From     = SITE_EMAILFROMADDR;
    $mail->FromName = SITE_EMAILFROMNAME;
    $mail->Subject  = SITE_EMAILSUBJECT;
    $mail->Body     = getSiteEmail();
    $mail->AddAddress($email, $name);
    if($mail->Send()) {
      $mailsuccess = true;
    } else {
      $mailsuccess = false;
    }  

 
  $code = $_SESSION['performanceCode'];
  $performance = $olct_session->getPerformanceEvent($code);
  $event = $performance->event;

  session_destroy();
  session_write_close();
  
  include siteFile('siteheader.php');
  include siteFile('siteSuccess.php'); 
  include siteFile('sitefooter.php');
?>  