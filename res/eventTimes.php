<?php
  require ("./inc/objects.php");

  $startDate = $today;
  if (defined('SITE_SHOWALLMOVIES')) {
    $endDate = $today + 24 * 60 * 60 * 365; // (add 365 days)
  } else {
    $endDate   = $today;
    $caption   = LANG_TODAY . ': ' . strftime('%A %d %B %Y ', $today);
    $dateParam = $_GET['date'];
    if (!isset($_GET['date'])) { 
      $dateParam = '0';
    } else {
      $dateParam = $_GET['date'];
    }  

    if ($dateParam == '1') {
      $endDate = incDate($startDate, 7);
      $caption = LANG_THISWEEK . ': ' . strftime('%A %d %b %Y', $startDate) . ' - ' . strftime('%A %d %b %Y', $endDate);
    } else {
      if ($dateParam > '0') {
        $startDate = strtotime($dateParam);
        $endDate = $startDate;
        $caption = LANG_DATE . ': ' . strftime('%A %d %B %Y ', $startDate);
      }  
    }
  }
  
  //---------------------
  function dateSelectDropDown()
  {
    global $dateParam, $olct_session;
    
    $res = '<select name="date" id="date" onChange="this.form.submit();">';
	
	if (SITE_SHOWTODAY == '1') 
	{
	  $res .= '<option value="0"';
      if ($dateParam == '0') {
        $res .= ' selected';
      }  
      $res .= '>-- ' . LANG_TODAY . ' --</Option>';
	}  
	
	if (SITE_SHOWTHISWEEK == '1') 
	{
	  $res .= '<option value="1"';
	  if ($dateParam == '1') {
        $res .= ' selected';
      } 
      $res .= '>-- ' . LANG_THISWEEK . ' --</Option>';
    }
	
    $dates = $olct_session->getScheduleDates();
    $dCnt = count($dates);
    for ($i=0; $i < $dCnt; $i++) {
      $dte = $dates[$i];
      $cDate = date('Ymd', $dte);
      $res .= '<option value="' . $cDate . '"';
      
      if ($cDate == $dateParam) { 
        $res .= ' SELECTED';
      }
      $res .= '>' . strftime('%a %d %B %Y', $dte) . '</Option>';
    }
    return $res;
  }    
  
  $events = $olct_session->getEventList($startDate, $endDate);
  $eventCount = count($events);

  include siteFile('siteheader.php');
  include siteFile('siteEventTimes.php');
  include siteFile('sitefooter.php');
?>
