<?php
  require (".\\inc\\objects.php");
  require (".\\inc\\salesOrder.php");

  $err = '';

  $performanceCode = $_GET['perfCode'];
  if ($performanceCode == null) {
    $performanceCode = $_SESSION[TAG_PERFORMANCECODE];
  }
  
  $performance = $olct_session->getPerformanceEvent($performanceCode);
  
  if ($performance == null) {
    redirect(makeURL('error', 'err=' . urlencode(LANG_PERFOFFSALE)));
  }
  $_SESSION[TAG_PERFORMANCECODE] = $performance->code;
  
  $event = $performance->event;
  $priceCard = $performance->getPriceCard();
  
  if ($priceCard == null || count($priceCard) == 0) {
    redirect(makeURL('error', 'err=' . urlencode(LANG_NOTICKETS)));
  }
    
  //Process Ticket Types
  if ($_GET['action'] == 'process') {
    $order = new olct_salesOrder();
    $order->clear();
    
    $ticketCount = 0;
    foreach ($_GET as $key=>$val) {
      $qty = intval($val); 
      if (strpos($key, '|') != 0) {
        $ticket = $priceCard->getTicket($key);
        if ($ticket != null && $qty > 0) {
          $performanceItem = $order->sellTicket($performance, $ticket, intval($val));
          if ($performanceItem != null) {
            $ticketCount = $ticketCount + intval($val);
          } else { 
            $err = LANG_TICKETTYPENOTAVAIL;
          }  
        }
      } 
    }

    if ($ticketCount == 0) { 
      $err = LANG_QTYREQUIRED;
    } else {  
      if ($ticketCount > SITE_MAXTICKETSELECTION) {
        $err = 'Upto ' . SITE_MAXTICKETSELECTION . ' ' . LANG_MAXTICKETS;
      }
    }      
    
    if ($err == '') {
      if ($order->reservePerformanceSeats($performanceItem)) {
        if ($performance->allocated == true) {
		  redirect('seats');
        } else {  
          redirect('payment', '', true);
        }  
      } else {
        $order->removePerformance($performanceItem);
        $err = $order->errorText;
        if ($order->errorCode == ERR_PERFORMANCEINVALID) {
          showError($order->errorText);
        }  
      }
    }
  }    
  
  include siteFile('siteheader.php');
  include siteFile('siteSelectTickets.php');
  include siteFile('sitefooter.php');
?>
