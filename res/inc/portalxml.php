<?php
define('WHITESPACE', '                                                ');
require_once (".\\inc\\xmlconstants.php");

function xmlTimestampToTime($ts) {
  if (strlen($ts) != 14) {
    return 0;
  } else {  
    return mktime(intval(substr($ts, 8, 2)), intval(substr($ts, 10, 2)), intval(substr($ts, 12, 2)), 
                  intval(substr($ts, 4, 2)), intval(substr($ts, 6, 2)), intval(substr($ts, 0, 4)));
  }  
}  

function xmlDateToTime($ts) {
  if (strlen($ts) != 8) {
    return 0;
  } else {  
    return mktime(0, 0, 0, intval(substr($ts, 4, 2)), intval(substr($ts, 6, 2)), intval(substr($ts, 0, 4)));
  }  
}  

//------------------------------------------------------------------------------
function quote_split($str, $delim = ' ', $singleQuote = false) {
  $array = array();
  $split = str_split($str);
  $quoteChar = $singleQuote == true ? chr(39) : chr(34);

  $quoted = false;
  $outWord = '';
  $len = count($split);
  
  foreach($split as $ch) {
    if ($ch == $quoteChar) {
      $quoted = !$quoted;
    } elseif ($ch == $delim  && !$quoted) {
      $array[] = trim($outWord);
      $outWord = '';
    } else {  
      $outWord .= $ch;
    }
  }

  $array[] = trim($outWord);
  return $array;
}

/*=================================================================================================
TXMLNode
==================================================================================================*/

class XMLNode {
  var $__lastFind = '';
  var $__findName = '';
  var $__attributes;   
  var $__nodeData;
  var $__parent;
  var $__value;
  var $nodeKey = '';
  var $__nodeCounter = 1;
  var $name;

  //--------------------------------------------------------------------------------------------------
  public function __construct($aName, &$aParent = null) {
    $this->name = $aName;
    $this->__parent = $aParent;
    $this->__attributes = array();
    $this->__nodeData = array();
  }

  //--------------------------------------------------------------------------------------------------  
  public function __destruct() {
    unset($this->__attributes);
    unset($this->__nodeData);
  }

  //--------------------------------------------------------------------------------------------------
  public function clearNode() {
    $this->__attributes = array();
    $this->__nodeData = array();
    $this->__value = '';
  }

  //-----------------------------------------------------------------------------------------------
  function writeTag(&$strs, $indent = 0, $stripWhitespace = false) {
    if (count($this->__attributes) == 0 && count($this->__nodeData) == 0 && $this->__value == '') {
      return;
    }

    $s = '';
    if (!$stripWhitespace) {
      $s .= substr(WHITESPACE, 1, $indent);
    } 

    $s .= '<' . $this->name;
    foreach ($this->__attributes as $key=>$value) {
      $s .= ' ' . $key . '="' . $value . '"'; 
    }
    
    if ($this->__value == '' && count($this->__nodeData) == 0) {
      $s .= ' />';
      $strs[] = $s;
    } else {  
      $s .= '>';

      if ($this->__value != '') {
        $s .= $this->__value . '</' . $this->name . '>';
        $strs[] = $s;
      } else {
        $strs[] = $s;

        foreach($this->__nodeData as $childNode) {
          $childNode->writeTag($strs, $indent + 2);
        }
        
        if (!$stripWhitespace) {
          $strs[] = substr(WHITESPACE, 1, $indent) . '</' . $this->name . '>';
        } else {  
          $strs[] = '</' . $this->name . '>';
        }
      }  
    }
  }

  //--------------------------------------------------------------------------------------------------
  function nodeCount() {
    return count($this->__nodeData);
  }  

  //--------------------------------------------------------------------------------------------------
  function addNode($aName) {
    $this->__nodeCounter++;
    $key = strval($this->__nodeCounter);
    $child = new XMLNode($aName, $this);
    $this->__nodeData[$key] = $child;
    $child->nodeKey = $key;
    return $child;
  }  

  //----------------------------------------------------------------------------------------------
  function addSimpleXMLNode(&$aNode) {
    $child = $this->addNode($aNode->getName());
    $sVal = trim(htmlspecialchars(strval($aNode[0])));

    if ($sVal != '') {
      $child->__value = $sVal;
    }
    
    foreach($aNode->attributes() as $key=>$val){
      $child->setStrAttr($key, $val);
    }
    
    foreach($aNode->children() as $node) {
      $child->addSimpleXMLNode($node);
    }
  }

  //--------------------------------------------------------------------------------------------------
  function nodeText() {
    $array = array();
    $this->writeTag($array, 0);
    $res = '';
    foreach($array as $str) {
      $res .= $str;
    }  
    return $res;
  }

  //--------------------------------------------------------------------------------------------------
  function getBoolAttr($aName) {
    return $this->__attributes[$aName] == '1' ? true : false;
  }

  //--------------------------------------------------------------------------------------------------
  function getStrAttr($aName) {
    return strval($this->__attributes[$aName]);
  }

  //--------------------------------------------------------------------------------------------------
  function getIntAttr($aName, $deft = 0) {
    return isset($this->__attributes[$aName]) ? intval($this->__attributes[$aName]) : $dflt;
  }

  //--------------------------------------------------------------------------------------------------
  function setBoolAttr($aName, $aValue) {
    unset($this->__attributes[$aName]);
    if ($aValue == true) {
      $this->__attributes[$aName] = 1;
    }
  }

  //--------------------------------------------------------------------------------------------------
  function setStrAttr($aName, $aValue) {
    unset($this->__attributes[$aName]);
    if ($aValue != '') {
      $this->__attributes[$aName] = strval($aValue);
    }
  }

  //--------------------------------------------------------------------------------------------------
  function setIntAttr($aName, $aValue) {
    unset($this->__attributes[$aName]);
    if (intval($aValue) != 0) {
      $this->__attributes[$aName] = intval($aValue);
    }
  }

  //--------------------------------------------------------------------------------------------------
  function writeDate($aName, $aDate) {
    if ($aDate != 0) {
      $child = $this->addNode($aName);
      $child->__value = date('Ymd', $aDate);
      return $child;
    } else {
      return null;
    }      
  }
  
  //--------------------------------------------------------------------------------------------------
  function writeTimestamp($aName, $aTimestamp) {
    if ($aTimestamp != 0) {
      $child = $this->addNode($aName);
      $child->__value = date('YmdHis', $aTimestamp);
      return $child;
    } else {
      return null;
    }      
  }

  //--------------------------------------------------------------------------------------------------
  function writeInt($aName, $aInt) {
    $iVal = intval($aInt);
    $child = null;

    if ($iVal != 0) {
      if ($aName != '') {
        $child = $this->addNode($aName);
      } else {
        $child = $this;
      }
      $child->__value = strval($iVal);      
    }

    return $child;
  }

  //--------------------------------------------------------------------------------------------------
  function writeStr($aName, $aStr) {
    $sVal = urlencode($aStr);
    $child = null;
    if ($sVal != '') {
      if ($aName != '') {
        $child = $this->addNode($aName);
      } else {
        $child = $this;
      }
      $child->__value = $sVal;
    }
    return $child;
  }    

  //--------------------------------------------------------------------------------------------------
  function writeUnchangedStr($aName, $aStr) {
    $sVal = $aStr;
    $child = null;
    if ($sVal != '') {
      if ($aName != '') {
        $child = $this->addNode($aName);
      } else {
        $child = $this;
      }
      $child->__value = $sVal;
    }
    return $child;
  }    

  //-----------------------------------------------------------------------------------------------
  function replaceValue($aName, $aStr) {
    $child = $this->findNode($aName);
    if ($aNode == null) {
      $this->writeStr($aName, $aStr);
    } else {  
      $child->__value = urlencode($aStr);  
    }
  }
  
  //--------------------------------------------------------------------------------------------------
  function writeFloat($aName, $aFloat, $exponent=2) {
    $child = null;
    $fVal = floatval($aFloat);
    if ($fVal != 0) {
      $val = floor($fVal * pow(10, $exponent));
      $child = $this->addNode($aName);
      $child->__value = strVal($val);
      
      if ($exponent != 2) {
        $child->setIntAttribute(TAG_EXPONENT, $exponent);
      }
    }
    return $child;
  }    

  //--------------------------------------------------------------------------------------------------
  function writeBool($aName, $aBool) {
    $child = null;
    
    if ($aBool == true) {
      $child = $this->addNode($aName);
      $child->__value = '1';
    }

    return $child;
  }

  //--------------------------------------------------------------------------------------------------
  function getNode($idx) {
    return $this->__nodeData[$idx];
  }  
  
  //--------------------------------------------------------------------------------------------------
  function findNode($aName) {
    $subNodes = $aName;
    if (substr($subNodes, 0, 1) == '/') {
      $subNodes = ltrim($subNodes, '/');
    }
    
    $p = strpos($subNodes, '/');
    if ($p != false) {
      $thisNode = substr($subNodes, 0, $p - 1);
      $subNodes = substr($subNodes, $p + 1, strlen($subNodes));
    } else {
      $thisNode = $subNodes;
    }  

    foreach($this->__nodeData as $node) {
      if ($thisNode == $node->name) {
        $subNodes = substr($subNodes, strlen($aName) + 1, strlen($subNodes));
        if ($subNodes == '') {
          return $node;
        } else {
          return $node->findNode($subNodes);
        }  
      }
    }
  }

  //--------------------------------------------------------------------------------------------------
  function removeNodeByName($aName) {
    $child = $this->findNode($aName);
    if ($child != null) {
      $this->removeNodeByKey($child->nodeKey);
    }
  }    

  //--------------------------------------------------------------------------------------------------
  function removeNodeByKey($aNodeKey) {
    unset($this->__nodeData[strval($aNodeKey)]);
  }

  //--------------------------------------------------------------------------------------------------}
  function renameNode($srcName, $tgtName) {
    if ($srcName == '') {
      $this->name = $tgtName;
    } else {  
      $child = $this->findNode($srcName);
      if ($child != null) {
        $child->name = $tgtName;
      }
    }
  }  

  //--------------------------------------------------------------------------------------------------
  function getFirst($aName) {
    $this->__lastFind = '';
    $this->__findName = $aName;
    return $this->getNext();
  }
  
  //--------------------------------------------------------------------------------------------------
  function getNext() {
    $foundLast = false;
    foreach($this->__nodeData as $nodeKey => $node) {
      if ($this->__findName == $node->name) {
        if ($this->__lastFind == '' || $foundLast) {
          $this->__lastFind = $nodeKey;
          return $node;
        }

        if ($nodeKey == $this->__lastFind) {
          $foundLast = true;
        }
      }
    }
    return null;
  }

  //--------------------------------------------------------------------------------------------------
  function readTimestamp($aName) {
    if ($aName == '') {
      $child = $this;
    } else  {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return 0;
    } else {  
      return xmlTimestampToTime($child->__value);
    }  
  }

  //--------------------------------------------------------------------------------------------------
  function readDate($aName) {
    if ($aName == '') {
      $child = $this;
    } else  {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return 0;
    } else {  
      return xmlDateToTime($child->__value);
    }  
  }

  //-------------------------------------------------------------------------------------------------
  function readStr($aName) {
    if ($aName == '') {
      $child = $this;
    } else  {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return '';
    } else {  
      return urldecode($child->__value);
    }  
  }

  //-------------------------------------------------------------------------------------------------
  function readBool($aName) {
    if ($aName == '') {
      $child = $this;
    } else  {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return false;
    } else {  
      return $child->__value == '1' ? true : false;
    }  
  }

  //-------------------------------------------------------------------------------------------------
  function readInt($aName) {
    if ($aName == '') {
      $child = $this;
    } else {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return 0;
    } else {  
      return intval($child->__value);
    }  
  }

  //-------------------------------------------------------------------------------------------------
  function readFloat($aName) {
    if ($aName == '') {
      $child = $this;
    } else  {  
      $child = $this->findNode($aName);
    }  
    if ($child == null) {
      return 0;
    } else {  
      return intval($child->__value) / 100;
    }  
  }

//END NODECLASS
}

/*=================================================================================================
TXMLObject
==================================================================================================*/

class XMLObject extends XMLNode {
var $__version;
var $__encoding;

  //--------------------------------------------------------------------------------------------------
  public function __construct() {
    $this->name = TAG_TOPNODE;
    $this->__version = '1.0';
    $this->__encoding = 'UTF-8';
    $this->__parent = null;
    $this->clearNode();
  }

  //--------------------------------------------------------------------------------------------------
  function writeToStrings(&$aStrs, $stripWhitespace = false) {
    $aStrs[] = '<?xml version="' . $this->__version . '" encoding="' . $this->__encoding . '"?>';
    $this->writeTag($aStrs, 0, $stripWhitespace);
  }

  //--------------------------------------------------------------------------------------------------
  function asXML($stripWhitespace = false) {
    $array = array();
    $this->writeToStrings($array, $stripWhitespace);
    $res = '';

    foreach($array as $str) {
      $res .= $str;

    }  
   return $res;
  }

  //--------------------------------------------------------------------------------------------------
  function writeToFile($aFilename) {
    $array = array();
    $this->writeToStrings($array);
    
    $handle = fopen($aFilename, "w");
    if ($handle ==  false) {
      return false;
    }  

    foreach($array as $str) {
      if (fwrite($handle, $str) == false) {
        return false;
      }  
    }  
    fclose($handle);
    return true;
  }
  
  //--------------------------------------------------------------------------------------------------
  function dumpHTML() {
    $array = array();
    $this->writeToStrings($array);
    echo('<br/><br/><strong>XML:</strong><br/><span style="font-family: courier">');
    
    foreach($array as $str) {
      $aStr = htmlspecialchars($str);
      $aStr = str_replace(' ', '&nbsp', $aStr);
      echo($aStr . '<br/>');
    }  
    echo('</span><br/><br/><strong>RAW:</strong><br/><span style="font-family: courier">');
    echo(htmlspecialchars($this->asXML()) . '</span>');
  }  

  //----------------------------------------------------------------------------------------------
  function parse($txt) {
    $xml = @simplexml_load_string($txt);
    if ($xml == null || $xml->getName() != 'admitOne') {
      return false;
    }  
    
    foreach($xml->attributes() as $key=>$val){
      $this->setStrAttr($key, $val);
    }
    
    foreach($xml->children() as $node) {
      $this->addSimpleXMLNode($node);
    }
  }  

//END CLASS
}
  
/*=================================================================================================
TXMLResponse
==================================================================================================*/

class XMLResponse extends XMLObject {
var $resultCode = 0;
var $resultText = '';

  public function __construct() {
    parent::__construct();
    $this->resultCode = ERR_CONNECTIONERROR;
    $this->resultText = 'Bad Connection';
  }
}

/*=================================================================================================
TXMLClientObject
==================================================================================================*/

class XMLClient extends XMLObject {

  public function __construct($messageID) {
    parent::__construct();
    $this->setIntAttr(TAG_REQUESTID, $messageID);
    $this->setStrAttr(TAG_TERMINAL, 'server');
  }

  //--------------------------------------------------------------------------------------------------
  function &send($address) {
		$xml = trim($this->asXML(true));
    $len = strlen($xml);
    if ($len == 0) {
      return null;
    }  
    $msgLen = 16 + 4 + $len;
    $binary = pack('NCCCCNNN', $msgLen, ord('R'), ord('S'), ord('P'), ord('L'), 9999, 0, $len) . $xml;

    $result = new XMLResponse();
    $connection = @fsockopen($address, 8000, $errno, $errstr, 5);
    if ($connection == false) {
      return $result;
    }
    
    @fwrite($connection, $binary);
    stream_set_timeout($connection, 30);
    $binary = @fread($connection, 20);
    
    if (strlen($binary) == 20) {
      $array = unpack('Nlen/C4ident/Nmsg/Nver/Nslen', $binary);
      $len = intval($array['slen']);
      if ($len != 0) {
        $str = fread($connection, $len);
        if (strlen($str) == $len) {
          $result->parse($str);   
          $result->resultCode = $result->getintAttr(TAG_RESULT);
          $result->resultText = $result->readStr(TAG_RESULTTEXT);
        }
      }  
    }  
      
		@fclose($connection);
    return $result;
  }  

//END CLASS
}

?>