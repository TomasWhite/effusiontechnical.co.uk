<?php
define('XMLID_NEWSLETTER', '507');
define('XMLID_RESERVESEATS', '508');
define('XMLID_CHECKSTATUS', '509');
define('XMLID_GETPERFORMANCEAVAILABILITY', '519');
define('XMLID_POSTORDER', '529');
define('XMLID_RELEASE', '530');
define('XMLID_GIFTCARDCONTROL', '516');
define('XMLID_GETSCHEME', '551');

define('TAG_DTD', '!DOCTYPE admitOne PRIVATE "-//CSL//DTD Admit One Portal Service//EN"');
define('TAG_TOPNODE', 'admitOne');
define('TAG_REQUESTID', 'requestId');
define('TAG_REQUESTNAME', 'requestName');
define('TAG_RESULT', 'result');
define('TAG_RESULTTEXT', 'resultText');

define('TAG_ACTION', 'action');
define('TAG_ACCOUNTCODE', 'accountCode');
define('TAG_TERMINAL', 'terminal');
define('TAG_TERMINALNAME', 'terminalName');
define('TAG_USERLOGIN', 'login');
define('TAG_ORDERITEMS', 'orderItems');
define('TAG_ORDERITEM', 'orderItem');
define('TAG_HANDLE', 'handle');
define('TAG_NAME', 'name');
define('TAG_SEATS', 'seats');
define('TAG_SEAT', 'seat');
define('TAG_SEATCODE', 'seatCode');
define('TAG_TICKETCODE', 'ticketCode');
define('TAG_PERFORMANCECODE', 'performanceCode');
define('TAG_PRICE', 'price');
define('TAG_QUANTITY', 'quantity');
define('TAG_ORDER', 'order');
define('TAG_TYPE', 'type');
define('TAG_SEATREQUEST', 'seatRequest');
define('TAG_SCHEMECODE', 'schemeCode');
define('TAG_CHARGECODE', 'chargeCode');
define('TAG_AUDIT', 'audit');
define('TAG_ALLOCATED', 'allocated');
define('TAG_GROUP', 'group');
define('TAG_EMAIL', 'email');

define('ERR_SUCCESS', 0);
define('ERR_CONNECTIONERROR', 1);
define('ERR_PERFORMANCEINVALID', 116);
define('ERR_PERFORMANCEEXPIRED', 165);
define('ERR_TICKETTYPEINVALID', 166);
define('ERR_SEATTYPEINVALID', 167);
define('ERR_NOBLOCK', 171);
define('ERR_INSUFFICIENTSEATS', 135);

/*
define('TAG_FORCE', 'force');
define('TAG_SEATAVAILABILITY', 'seatAvailability');
define('TAG_AVAILABILITYCODE', 'availabilityCode');
define('TAG_CODE', 'code');
define('TAG_SHORTNAME', 'shortName');
define('TAG_PRICETIER', 'priceTier');
define('TAG_COPIES', 'copies');
define('TAG_ACTION', 'action');
define('TAG_DEFAULT', 'default');
define('TAG_COLOR', 'color');
define('TAG_COORDINATES', 'coordinates');
define('TAG_FORMAT', 'format');
define('TAG_VALUE', 'value');
define('TAG_PASSWORD', 'password');
define('TAG_SCANDATA', 'scanData');
define('TAG_CUSTOMER', 'customer');
define('TAG_ORDERLIST', 'orders');
define('TAG_STATUS', 'status');
define('TAG_REFUNDED', 'refunded');
define('TAG_PAYMENTS', 'payments');
define('TAG_PAYMENT', 'payment');
define('TAG_INTERVALITEM', 'intervalItem');
define('TAG_INTERVALREF', 'intervalReference');

define('TAG_BOOKINGCHANNEL', 'bookingChannel');
define('TAG_SEATS', 'seats');
define('TAG_SEATTYPE', 'seatType');
define('TAG_SEATTYPES', 'seatTypes');

define('TAG_CARDDETAILS', 'cardDetails');
define('TAG_ADDRESS', 'address');
define('TAG_SHORTNOTE', 'shortNote');
define('TAG_TELEPHONE', 'telephone');
define('TAG_POSTCODE', 'postalCode');
define('TAG_PROCESSID', 'processID');
define('TAG_EXPIRYDATE', 'expiryDate');
define('TAG_STARTDATE', 'startDate');
define('TAG_ENDDATE', 'endDate');
define('TAG_ISSUE', 'issue');
define('TAG_AUTHCODE', 'authCode');
define('TAG_MOTO', 'moto');
define('TAG_STREET', 'street');
define('TAG_LOCALITY', 'locality');
define('TAG_TOWN', 'town');
define('TAG_COUNTY', 'county');
define('TAG_COUNTRY', 'country');
define('TAG_CUSTOMERNAME', 'customerName');
define('TAG_CARDNAME', 'cardName');
define('TAG_PROFESSION', 'profession');

define('TAG_ISSUEPOSITION', 'issuePosition');
define('TAG_ISSUELENGTH', 'issueLength');
define('TAG_VALIDITYPOS', 'validityPosition');
define('TAG_MERCHANT', 'merchantNumber');
define('TAG_TELEPHONECP', 'cpTelephone');
define('TAG_TELEPHONECNP', 'cnpTelephone');
define('TAG_PAYMENTACCOUNT', 'paymentAccount');

define('TAG_PLAN', 'plan');
define('TAG_PLANCODE', 'planCode');
define('TAG_SEATMAP', 'seatMap');
define('TAG_HOUSE', 'house');
define('TAG_PROCESS', 'process');
define('TAG_BROKEN', 'broken');
define('TAG_LOCKED', 'locked');
define('TAG_SOLD', 'sold');
define('TAG_RESERVED', 'reserved');
define('TAG_CAPACITY', 'capacity');
define('TAG_ORDERBY', 'orderBy');

define('TAG_RECORDLIMIT', 'recordLimit');
define('TAG_RENEWAL', 'renewal');

define('TAG_EVENTS', 'events');
define('TAG_EVENT', 'event');

define('TAG_PERFORMANCES', 'performances');
define('TAG_PERFORMANCE', 'performance');

define('TAG_SEATQUERY', 'seatQuery');
define('TAG_PERFORMANCEDATE', 'performanceDate');
define('TAG_EVENTCODE', 'eventCode');
define('TAG_HALLCODE', 'hallCode');
define('TAG_CUSTOMERCODE', 'customerCode');
define('TAG_ACCOUNTCODE', 'accountCode');
define('TAG_DISCOUNTCODE', 'discountCode');
define('TAG_PAYMENTCODE', 'paymentCode');
define('TAG_VOUCHERCODE', 'voucherCode');
define('TAG_PRODUCTCODE', 'productCode');
define('TAG_COMPONENTCODE', 'componentCode');
define('TAG_TAXCODE', 'taxCode');
define('TAG_CURRENCYCODE', 'currencyCode');
define('TAG_TICKETLAYOUTCODE', 'ticketLayoutCode');
define('TAG_VOUCHERREF', 'voucherReference');
define('TAG_POINTS', 'points');
*/
?>