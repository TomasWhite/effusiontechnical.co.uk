<?php
function siteFile($file) {
  return '.\\sites\\' . SITE_CIRCUITCODE . '\\' . $file;
}    

function quoteStr($text, $escaped = true)
{
  return '\''. $text . '\'';
}

function incDate($date, $inc = 1)
{
  return $date + ($inc * 24 * 60 * 60);  
}
  
function firstFriday($date)
{
  $res = $date;
  while (date("w", $res) != "5") {
    $res = incDate($res, -1); 
  }  
  return $res;
}

function currencyStr($val) {
  return SITE_CURRENCYSTRING . number_format($val / 100, 2, '.', ',');
}

function emailcurrencyStr($val) {
  return number_format($val / 100, 2, '.', ',');
}

function asc_shift($string, $amount) {
  $key = substr($string, 0, 1);
  if(strlen($string)==1) {
    return chr(ord($key) + $amount);
  } else {
    return chr(ord($key) + $amount) . asc_shift(substr($string, 1, strlen($string)-1), $amount);
  }
}

function make4($str) {
  return str_pad($str, 4, '$');
}  

function make2($str) {
  return str_pad($str, 2, '-');
}  

function make20($str) {
  return str_pad($str, 20, '*');
}  

function make10($str) {
  return str_pad($str, 10, '!');
}  

function make30($str) {
  return str_pad($str, 30, '^');
}  

function swapPairs($str) {
  $res = '';
  $arr = str_split($str, 2);
  foreach ($arr as $v) {
    $pair = str_split($v);
    $res .= dechex(ord($pair[1])) . dechex(ord($pair[0]));
  }  
    
  return $res;
}  

function makeURL($pageRef, $params = '', $secure = false)
{
  $https = (defined('DEVMODE') && DEVMODE == TRUE) ? false : $secure;
  if ($https) {
    $res .= 'https://';
  } else {
    $res .= 'http://';
  }
  $res .= HOST_NAME . '/index.php?s=' . SITE_CODE . '&p=' .$pageRef ;
  if ($params != '') {
    $res .=  '&' . $params;
  }  
  return $res;  
}

function pkcs5_pad ($text, $blocksize)
{
  $pad = $blocksize - (strlen($text) % $blocksize);
  return $text . str_repeat(chr($blocksize - $pad), $pad);
}

function pkcs5_unpad ($text)
{
$pad = ord($text{strlen($text)-1});
//if ($pad strlen($text)) return false;
if (!strspn($text, chr($pad), strlen($text) - $pad)) return false;
return substr($text, 0, -1 * $pad);
}

function hashStr($hashStr, $key)
{
  if ($key == '' || $hashStr == '') {
    return $hashStr;
  }

  $hashKey = substr($key . '987654321', 0, 8);
  $hashStr = pkcs5_pad($hashStr, 8);
  $crypttext = mcrypt_ecb(MCRYPT_DES, $hashKey, $hashStr, MCRYPT_ENCRYPT, chr(128) . chr(64) . chr(32) . chr(16) . chr(8) . chr(4) . chr(2) . chr(1));
  return base64_encode($crypttext);
}

function redirect($pageRef, $params = '', $secure = false) {
  session_write_close();
  header('Location: ' . makeURL($pageRef, $params, $secure));
  exit();
}

function showError($error) {
  redirect('error', 'e=' . urlencode($error), false);
}  

function sqlDate($dateVal)
{
  return quoteStr(date('Y-m-d', $dateVal));
}
  
function sqltimestamp($dateVal)
{
  return quoteStr(date('Y-m-d H:i:s', $dateVal));
}

function validEmailAddress($email) {
  // First, we check that there's one @ symbol, and that the lengths are right
  if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $email)) {
      // Email invalid because wrong number of characters in one section, or wrong number of @ symbols.
      return false;
  }
  // Split it into sections to make life easier
  $email_array = explode("@", $email);
  $local_array = explode(".", $email_array[0]);
  for ($i = 0; $i < sizeof($local_array); $i++) {
       if (!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) {
          return false;
      }
  }    
  if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) { // Check if domain is IP. If not, it should be valid domain name
      $domain_array = explode(".", $email_array[1]);
      if (sizeof($domain_array) < 2) {
              return false; // Not enough parts to domain
      }
      for ($i = 0; $i < sizeof($domain_array); $i++) {
          if (!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) {
              return false;
          }
      }
  }
  return true;
}

?>