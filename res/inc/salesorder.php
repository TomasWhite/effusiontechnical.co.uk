<?php

require (".\\inc\\portalXMl.php");
require_once(".\\inc\\xmlConstants.php");

define('itNone', 0);
define('itTicket', 1);
define('itCharge', 2);
define('itProduct', 3);
define('itVoucher', 4);
define('itDiscount', 5);
define('itVirtualProduct', 6);
define('itMembership', 7);
define('itEntitlement', 8);
define('itPerformance', 9);

/*=========================================================================
OLTC_SALESORDER
=========================================================================*/
class olct_salesOrder extends XMLClient {
var $errorText;
var $errorCode;
var $ticketQty = 0;
var $feeTotal = 0;
var $orderTotal = 0;
  
	//--------------------------------------------------------------------------------------------------
  public function __construct()
  {
    parent::__construct(XMLID_POSTORDER);
    if (isset($_SESSION['xmlorder'])) {
      $this->parse($_SESSION['xmlorder']);
    }  
  } 
  
  //--------------------------------------------------------------------------------------------------
  private function saveOrder() {
    $_SESSION['xmlorder'] = $this->asXML();
  }  
  
  //--------------------------------------------------------------------------------------------------
  public function clear() {
    $orderNode = $this->__orderNode();
    $orderItems =  $orderNode->findNode(TAG_ORDERITEMS);
    if ($orderItems != null && $orderItems->nodeCount() > 0) {
      foreach($orderItems->__nodeData as $pNode) {
        $this->releaseSeats($pNode);
      }
    }
       
    unset($_SESSION['xmlorder']);
    session_unregister('xmlorder');
    $this->clearNode();
    $this->saveOrder();
  }    

  //--------------------------------------------------------------------------------------------------
  private function __orderNode() {
    $child = $this->findNode(TAG_ORDER);
    if ($child == null) {
      $child = $this->addNode(TAG_ORDER);
      $child->writeStr(TAG_TYPE, 'sale');
      $child->writeStr(TAG_USERLOGIN, 'admin');
      $child->writeStr(TAG_TERMINALNAME, 'SERVER');
      $child->writeStr('bookingChannel', 'olct');
    }
    return $child;
  }    
      
  //------------------------------------------------------------------------------
  private function findOrderItem(&$baseItem, $itemType, $itemCode) {
    $orderItems = $baseItem->findNode(TAG_ORDERITEMS);
    if ($orderItems == null) {
      return null;
    }  

    $orderItem = $orderItems->getFirst(TAG_ORDERITEM);
    while ($orderItem != null) {
      switch ($itemType) {
        case itPerformance : 
             if ($orderItem->getStrAttr(TAG_PERFORMANCECODE) == $itemCode) {
               return $orderItem;
             }
             break;              
        case itTicket      : 
             if ($orderItem->getStrAttr(TAG_TICKETCODE) == $itemCode) {
               return $orderItem;
             }
             break;              
      }
    
      $orderItem = $orderItems->getNext;
    }  
  }
  
  //------------------------------------------------------------------------------
  function perfNodeFor($perfCode) {
    return $this->findOrderItem($this->__orderNode(), itPerformance, $perfCode);
  }  

  //--------------------------------------------------------------------------------------------------
  private function addOrderitem(&$baseItem, $itemType, $itemCode) {
    $orderItems = $baseItem->findNode(TAG_ORDERITEMS);
    if ($orderItems == null) {
      $orderItems = $baseItem->addNode(TAG_ORDERITEMS);
    }  

    $child = $orderItems->addNode(TAG_ORDERITEM);

    switch ($itemType) {
      case itPerformance : $child->setStrAttr(TAG_PERFORMANCECODE, $itemCode); 
                           break;
      case itTicket      : $child->setStrAttr(TAG_TICKETCODE, $itemCode); 
                           break;
      case itCharge      : $child->setStrAttr(TAG_CHARGECODE, $itemCode); 
                           break;
      case itMembership  : $child->setStrAttr(TAG_SCHEMECODE, $itemCode); 
                           break;
      default : die('Invalid itemType passed in addOrderItem');                       
    }  

    return $child;
  }

  //--------------------------------------------------------------------------------------------------
  public function summariseOrder() {
    $res = array();
    $this->orderTotal = 0;
    $this->ticketQty = 0;
    $this->feeTotal = 0;
    
    $orderItems = $this->__orderNode()->findNode(TAG_ORDERITEMS);
    if ($orderItems == null) {
      return $res;
    }
    
    foreach($orderItems->__nodeData as $pNode) {
      if ($pNode->getStrAttr(TAG_PERFORMANCECODE) != '') {
        $res[] = array('type'=>'P', 
                       'desc'=>$pNode->readStr(TAG_NAME) . ' in ' . $pNode->readStr('hall') . ' on ' . 
					       strftime('%A %d %B', $pNode->readTimestamp('time')) . ' @ ' . date('H:i', $pNode->readTimestamp('time')),
                       'itemval'=>0, 
                       'itemtot'=>0);
                       
        $tickets = $pNode->findNode(TAG_ORDERITEMS);
        if ($tickets == null) {
          return $res;
        }
        
        foreach ($tickets->__nodeData as $tNode) {
          $priceMul = intval($tNode->readInt('quantity') / $tNode->readInt('orderQty'));
          $price = ($tNode->readInt(TAG_PRICE) * $priceMul) + $tNode->readInt('orderValue');
  	      $linetot = $price * $tNode->readInt('orderQty');
          $this->ticketQty += $tNode->readInt(TAG_QUANTITY);
          $this->orderTotal += $linetot;
          $res[] = array('type'=>'T', 
                       'desc'=>$tNode->readStr('orderQty') . ' x ' . $tNode->readStr(TAG_NAME) . ' ' . LANG_ORDERTICKETS,
                       'itemval'=>$price,
                       'itemtot'=>$linetot);
        }  
      }

      if (SITE_FEEPERORDER == 1) {
        $this->feeTotal = SITE_FEEVALUE;
      } else {
        $this->feeTotal = SITE_FEEVALUE * $this->ticketQty;
        if ($this->feeTotal > SITE_FEECAP) {
          $this->feeTotal = SITE_FEECAP;
        }  
      }
      
      if ($this->feeTotal != 0) {
        $res[] = array('type'=>'B', 'itemtot'=>$this->feeTotal);  
        $this->orderTotal += $this->feeTotal;
      }
      
      $res[] = array('type'=>'Z', 'itemtot'=>$this->orderTotal);
    }

    return $res;
  }    

  //--------------------------------------------------------------------------------------------------
  private function releaseSeats(&$perfNode) {
    $handle = $perfNode->getIntAttr(TAG_HANDLE);
    if ($handle != 0)
    {
      $xml = new XMLClient(XMLID_RELEASE);
      $xml->writeInt(TAG_HANDLE, $handle);
      $result = $xml->send(SITE_PORTALADDRESS);
      $perfNode->setStrAttr(TAG_HANDLE, '0');
    }  
  }
  
  //--------------------------------------------------------------------------------------------------
  public function post(&$cardData){
    global $olct_session;

    $arr = $this->summariseOrder();
    if (count($arr) < 3) {
      return false;
    }  
    
    $orderNode = $this->__orderNode();
    
    $orderNode->removeNodeByName('session');
  
    $cardStr = make2($cardData['expMonth']) . make4($cardData['expYear']) . make2($cardData['startMonth']) . make4($cardData['startYear']) .
               make2($cardData['issue']) . make4($cardData['cvv']) . make20($cardData['cardNumber']) . make30($cardData['nameOnCard']) .
               make10($cardData['value']) . make10($this->feeTotal) . make20($cardData['telephone']) . make30($cardData['emailAddress']) .
               make10($cardData['altReference']) . make10($cardData['authCode']) . make10($cardData['processID']) .  
               make20($cardData['giftCardNumber']) . make10($cardData['giftCardValue']) . make2('xx');

    $cardStr = asc_shift($cardStr, 12);     
    $cardStr = swapPairs($cardStr);     
    $cardStr = base64_encode($cardStr);

    $orderNode->writeStr('session', $cardStr);
    
    //Post  loyalty
    if ($cardData['loyaltyCardNumber'] != '') {
      $pts = intval(($this->orderTotal * SITE_LOYALTYPTS) / 100);
      if ($pts > SITE_LOYALTYMAX) {
        $pts = SITE_LOYALTYMAX;
      }
      $orderNode->writeStr(TAG_ACCOUNTCODE, hashStr($cardData['loyaltyCardNumber'], SITE_CIRCUITCODE));
      $orderNode->writeStr('customerCode', $cardData['customerCode']);
      $orderNode->writeInt('points', $pts);
    }  
        
    $result = $this->send(SITE_PORTALADDRESS);
    if ($result != null && $result->resultCode == ERR_SUCCESS) {
      $_SESSION['email'] = $cardData['emailAddress'];
      $_SESSION['name'] = $cardData['nameOnCard'];
      $_SESSION['total'] = $this->orderTotal;
      $_SESSION['fee'] = $this->feeTotal;
      $_SESSION['audit'] = $result->readStr(TAG_AUDIT);
      $_SESSION['orderSummary'] = serialize($arr);
      $_SESSION['loyaltyPoints'] = $pts;
      $_SESSION['loyaltyBalance'] = $cardData['loyaltyBalance'];
      $_SESSION['loyaltyCardNumber'] = $cardData['loyaltyCardNumber'];
      $_SESSION['giftCardValue'] = $cardData['giftCardValue'];
      
      $sql = "insert into sup_order(SiteCode,Audit,Total,Fee,Email,Name,TicketQty)values(" .
             "'" . SITE_CODE . "'," . $result->readStr(TAG_AUDIT) . "," . $this->orderTotal . "," . $this->feeTotal . ",'" . $cardData['emailAddress'] . "',''," . $this->ticketQty . ")";
         
      $olct_session->dbQuery($sql);

      if ($_SESSION['referrer'] != '') {
        $olct_session->dbQuery("update sup_ReferrerVisits set orderCount = orderCount + 1, orderValue=orderValue+" . $_SESSION['total'] . 
                      ", ticketCount = ticketCount + " . $this->ticketQty . 
                      " where referCode='" . $_SESSION['referrer'] . "' and siteCode='" . SITE_CODE . "' and clickDate=" . sqlDate(time()));
      } 

      unset($_SESSION['xmlorder']);
      session_unregister('xmlorder');
      unset($_SESSION['cardData']);
      session_unregister('cardData');
      session_write_close();
      return true;      
    } else {
      return false;
    }  
  }
  
  //--------------------------------------------------------------------------------------------------
  public function removePerformance(&$perfNode) {
    $orderItems = $this->__orderNode()->findNode(TAG_ORDERITEMS);
    if ($orderItems == null || $orderItems->nodeCount() == 0) {
      return false;
    }
    $orderItems->removeNodeByKey($perfNode->nodeKey);
    $this->saveOrder();
    return true;
  }

  //--------------------------------------------------------------------------------------------------
  private function assignSeats(&$perfNode, &$response) {
    $perfNode->removeNodeByName(TAG_SEATS);
    $seatsNode = $perfNode->addNode(TAG_SEATS);

    $rSeats = $response->getFirst(TAG_SEATS);
    while ($rSeats != null) {
      $seatNode = $seatsNode->addNode(TAG_SEAT);
      $seatNode->setStrAttr(TAG_SEATCODE, $rSeats->getStrAttr(TAG_SEATCODE));
      
      $alloc = explode('/', $rSeats->readStr(TAG_ALLOCATED));
      $labels = '';
      $seatCount = 0;
      $firstSeatID = 0;
      
      foreach($alloc as $seatRef) {
        $seatData = explode(':', $seatRef);
        $seatCount++;
        if ($labels == '') {
          $firstSeatID = intval($seatData[0]);
        } 
        $labels .= $seatData[2] . $seatData[1] . ',';
        $lastSeatID = intval($seatData[0]);
      }  
      $seatNode->writeUnchangedStr('labels', rtrim($labels, ','));
      $seatNode->writeInt('seatID', $firstSeatID);
      $seatNode->writeInt('seatCount', $seatCount);
      $rSeats = $response->getNext();
    }
  }

  //------------------------------------------------------------------------------
  function checkAllReservations() {
    $orderItems = $this->__orderNode()->findNode(TAG_ORDERITEMS);
    if ($orderItems == null || $orderItems->nodeCount() == 0) {
      return false;
    }
   
    foreach ($orderItems->__nodeData as $pNode) {
      $handle = $pNode->getStrAttr(TAG_HANDLE);
      if ($handle != '') {
        $xml = new XMLClient(XMLID_CHECKSTATUS);
        $xml->writeStr(TAG_HANDLE, $handle);
        $result = $xml->send(SITE_PORTALADDRESS);
        if ($result->getIntAttr(TAG_RESULT) != ERR_SUCCESS) {
          $pNode->setStrAttr(TAG_HANDLE, '0');
          $orderItems->removeNodeByKey($pNode->nodeKey);
          $this->saveOrder();
          return false;
        }  
      }
    }  
    return true;
    $this->saveOrder();
  }
  
  //--------------------------------------------------------------------------------------------------
  function updateSoldOut($perfCode) {
    global $olct_session;

    $sql = "update sup_performance set soldOut = 3 where SiteCode='" . SITE_CODE . "' and performanceCode='" . $perfCode . "'";
    $olct_session->dbQuery($sql);
  }

  //--------------------------------------------------------------------------------------------------
  public function reservePerformanceSeats(&$perfNode) {
    $this->errorCode = ERR_PERFORMANCEINVALID;
    $tickets = $perfNode->findNode(TAG_ORDERITEMS);
    if ($tickets == null || $tickets->nodeCount() == 0) {
      return false;
    }  

    $this->releaseSeats($perfNode);

    $OK = false;
    $seats = array();
    $orderItem = $tickets->getFirst(TAG_ORDERITEM);
    while ($orderItem != null) {
      $seat = $orderItem->getStrAttr(TAG_SEATCODE);      
      $qty  = $orderItem->readInt(TAG_QUANTITY);
      if ($seat == '' || $qty == 0) {
        return false;
      }  
      $seats[$seat] = intval($seats[$seat]) + $qty;
      $orderItem = $tickets->getNext();
    }  

    if (count($seats) == 0) {
      return false;
    }  
    
    $xml = new XMLClient(XMLID_RESERVESEATS);
    $xml->writeStr(TAG_PERFORMANCECODE, $perfNode->getStrAttr(TAG_PERFORMANCECODE));
    $xml->writeStr('contigiousOnly', TRUE);
	
    foreach($seats as $seatCode=>$qty) {
      $seatNode = $xml->writeInt(TAG_SEATREQUEST, $qty);
      $seatNode->setStrAttr(TAG_SEATCODE, $seatCode);
    }  

    $response = $xml->send(SITE_PORTALADDRESS);
    if ($response != null) {
      switch ($response->resultCode) {
        case ERR_SUCCESS            : $OK = true;
                                      break; 
        case ERR_PERFORMANCEINVALID :
        case ERR_PERFORMANCEEXPIRED : $this->errorText = LANG_PERFOFFSALE;
                                      break;
        case ERR_CONNECTIONERROR    : $this->errorText = LANG_NOCONNECTION . ' - ' . SITE_PORTALADDRESS;
                                      break;
        case ERR_TICKETTYPEINVALID  :
        case ERR_SEATTYPEINVALID    : $this->errorText = LANG_TICKETTYPENOTAVAIL;
                                      break;
        case ERR_NOBLOCK            :
        case ERR_INSUFFICIENTSEATS  : $this->errorText = LANG_NOSEATS;
                                      $this->updateSoldOut($perfNode->getStrAttr(TAG_PERFORMANCECODE));
                                      break;
        default                     : $this->errorText = $xml->resultText;
      }
    }      

    $this->errorCode = $response->resultCode;
    if ($OK == true) {
      $perfNode->setIntAttr(TAG_HANDLE, $response->readInt(TAG_HANDLE));
      $this->assignSeats($perfNode, $response);
      $this->saveOrder();
    }

    return $OK;
  }  
  
  //--------------------------------------------------------------------------------------------------
  public function swapSeats(&$perfNode, $oldID, $newID, $seatCount, $seatCode) {
    $handle = $perfNode->getIntAttr(TAG_HANDLE);
    if ($handle == 0 && $newID != $oldID) {
      return false;
    }

    $xml = new XMLClient(XMLID_RESERVESEATS);
    $xml->writeStr(TAG_PERFORMANCECODE, $perfNode->getStrAttr(TAG_PERFORMANCECODE));
    $xml->writeInt(TAG_HANDLE, $handle);
    $str = strval($newID);
    for ($i=1; $i<$seatCount; $i++) {
      $str .= '/' . strval($newID + $i);
    }  

    $aNode = $xml->writeUnchangedStr('seatRequest', $str);
    $aNode->setStrAttr(TAG_SEATCODE, $seatCode);

    $response = $xml->send(SITE_PORTALADDRESS);
    if ($response == null || $response->resultCode != ERR_SUCCESS) {
      return false;
    }  
    $this->assignSeats($perfNode, $response);
    $this->saveOrder();
    return true;
  }  

  //--------------------------------------------------------------------------------------------------
  public function sellTicket(&$performance, &$ticket, $qty)
  {
    if ($performance == null || $qty == 0 || $ticket == null) {
      return null;
    }

    //Setup ther performance level Node
    $oNode = $this->__orderNode();
    $pItem = $this->findOrderItem($oNode, itPerformance, $performance->code);
    if ($pItem == null) {
      $pItem = $this->addOrderItem($oNode, itPerformance, $performance->code);
      $pItem->writeStr(TAG_NAME, $performance->event->title);
      $pItem->writeStr('hall', $performance->hall);
      $pItem->writeTimestamp('time', $performance->time);
    }

    //Add the ticket level node
    $tItem = $this->findOrderItem($pItem, itTicket, $ticket->code);
    if ($tItem != null) {
      $pItem->removeNodeByKey($tItem->nodeKey);
    }  

    $tItem = $this->addOrderItem($pItem, itTicket, $ticket->code);
    $tItem->setStrAttr(TAG_SEATCODE, $ticket->seatCode);
    $tItem->writeInt(TAG_QUANTITY, $qty * $ticket->qty);
    
    if ($ticket->qty > 1) {
      $tItem->writeInt(TAG_PRICE, $ticket->price / $ticket->qty);
    } else {
      $tItem->writeInt(TAG_PRICE, $ticket->price);
    }
    
    if ($ticket->chargeValue > 0) {
      $tItem->writeStr('chargeCode', $ticket->chargeCode);
      $tItem->writeInt('chargeValue', $ticket->chargeValue);
      $tItem->writeInt('orderValue', $ticket->chargeValue * $ticket->qty);
    }  
    $tItem->writeInt('orderQty', $qty);
    $tItem->writeStr(TAG_NAME, $ticket->name . ' ' . $ticket->seat);

    $this->releaseSeats($pItem);

    $this->saveOrder();
    //pItem.SellTicket(ATicket, Qty);
    return $pItem;
  }
  
}  
?>
