<?php
require_once ("..\\..\\inc\\cardFunctions.php");
require_once ("..\\..\\inc\\Functions.php");
  
function authoriseCard(&$cardData) {
    $cardStr = make2($cardData['expMonth']) . make4($cardData['expYear']) . make2($cardData['startMonth']) . make4($cardData['startyear']) .
               make2($cardData['issue']) . make4($cardData['cvv']) . make20($cardData['cardNumber']) . make30($cardData['nameOnCard']) .
               make10($cardData['value']);

    $cardStr = asc_shift($cardStr, 12);     
    $cardStr = swapPairs($cardStr);     
    $cardStr = base64_encode($cardStr);
    echo($cardStr);
    die;
}

$cardData = array();
  $cardData['cardType']     = '1';
  $cardData['cardNumber']   = '4444333322221111';
  $cardData['cvv']          = '123';
  $cardData['nameOnCard']   = 'Peter morton';
  $cardData['emailAddress'] = 'peter.morton@cslimited.co.uk';
  $cardData['expMonth']     = '01';
  $cardData['expYear']      = '2009';
  $cardData['value']        = '1720';

  AuthoriseCard($cardData);

?>     
