<?
//Initialise arrays
$parentElements = array();
$TSSChecks = array();
$currentElement = 0;
$currentTSSCheck = "";

$authorised = false;
$error = '';
$locationcode = SITE_LOCATIONCODE;

switch ($cardData['cardType']) {
  case CT_VISA      : $cardtype = 'VISA';  
                      break; 
  case CT_AMEX      : $cardtype = 'AMEX';  
                      break; 
  case CT_MASTERCARD: $cardtype = 'MC';  
                      break; 
  case CT_MAESTRO   : $cardtype = 'SWITCH';  
                      break; 
  case CT_LASER     : $cardtype = 'LASER';  
                      break; 
  case CT_DINERS    : $cardtype = 'DINERS';  
                      break; 
  default           : $error = 'Unrecognised card type for Realex';
                      break; 
}

if ($error == '') {
  $merchantid = REALEX_MID;
  $secret     = REALEX_SECRET;
  $account    = "";
  $ref        = SITE_CODE . date('His');
  $amount     = $cardData['value'];
  $currency   = REALEX_CURRENCYCODE;
  $cardnumber = $cardData['cardNumber'];
  $cardname   = $cardData['nameOnCard'];
  $expdate    = str_pad((int)$cardData['expMonth'], 2, '0', STR_PAD_LEFT) . substr($cardData['expYear'], 2, 2);
  $timestamp  = strftime("%Y%m%d%H%M%S");

  // This section of code creates the md5hash that is needed
  $tmp = "$timestamp.$merchantid.$ref.$amount.$currency.$cardnumber";
  $md5hash = md5($tmp);
  $tmp = "$md5hash.$secret";
  $md5hash = md5($tmp);

  // Create and initialise XML parser
  $xml_parser = xml_parser_create();
  xml_set_element_handler($xml_parser, "startElement", "endElement");
  xml_set_character_data_handler($xml_parser, "cDataHandler");

  //A number of variables are needed to generate the request xml that is send to Realex Payments.
  $xml = "<request type='auth' timestamp='$timestamp'>
	  <merchantid>$merchantid</merchantid>
    <account>$account</account>
    <orderid>$ref</orderid>
    <amount currency='$currency'>$amount</amount>
    <card> 
	    <number>$cardnumber</number>
	    <expdate>$expdate</expdate>
		  <type>$cardtype</type> 
      <chname>$cardname</chname> 
    </card> 
    <autosettle flag='1'/>
    <md5hash>$md5hash</md5hash>
    <tssinfo>
  		<address type=\"billing\">
		    <country>$locationcode</country>
	    </address>
    </tssinfo>
  </request>";
  
  // Send the request array to Realex Payments
  $ch = curl_init();    
  curl_setopt($ch, CURLOPT_URL, "https://epage.payandshop.com/epage-remote.cgi");
  curl_setopt($ch, CURLOPT_POST, 1); 
  curl_setopt($ch, CURLOPT_USERAGENT, "payandshop.com php version 0.9"); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // this line makes it work under https 
  $response = curl_exec ($ch);     
  curl_close ($ch); 

  if (!$response) {
    $error = 'Unable to send to realex';
  } else { 
    $resXML = simplexml_load_string($response);
    echo(htmlspecialchars($xml));
    echo('<br/><br/><br/><br/>');
  
    if ($resXML->result != '00') {
      $error = $resXML->message;
    } else {
      $authorised = true;    
      $cardData['altReference'] = $resXML->batchid; 
      $cardData['authCode']     = $resXML->authcode; 
      $cardData['processID']    = $resXML->orderid; 
    }
  }
  echo(htmlspecialchars($response));
  
  if (!$authorised) {
    $cardData['error'] = $error;
  }    
}
?>