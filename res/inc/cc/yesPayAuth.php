<?php
$authorised = false;
$ref    = SITE_CODE . date('His');
$amount = $cardData['value'];
$digest = sha1($ref . $amount . YESPAY_PASSWORD);

$postRequest = '';
$postRequest .= 'merchantID=' . YESPAY_MID . '&';
$postRequest .= 'MTR='        . $ref . '&';
$postRequest .= 'amount='     . $amount . '&';
$postRequest .= 'currency='   . YESPAY_CURRENCYCODE . '&';
$postRequest .= 'digest='     . $digest . '&';
$postRequest .= 'protocol='   . 'WEB' . '&';
$postRequest .= 'crdHldName=' . urlencode($cardData['nameOnCard']) . '&';
$postRequest .= 'cardNumber=' . $cardData['cardNumber'] . '&';

if ($cardData['cvv'] != '000') {
  $postRequest .= 'CVV=' . $cardData['cvv'] . '&';
}
  
$postRequest .= 'expiryDate=' . date('my', $cardData['expiryDate']) . '&';

if ($cardData['cardType'] == CT_MAESTRO) {
  $issueValid = ($cardData['issue'] > 0 && $cardData['issue'] < 99) ? true : false;
  if ($issueValid == true) {
    $postRequest .= 'issue=' . $cardData['issue'] . '&';
  } else {
    $postRequest .= 'startDate=' . date('my', $cardData['startDate']) . '&';
  }
}  
rtrim($postRequest, '&'); 

$url = "https://www.yes-pay.net/embossServlet/payment";
$useragent= "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)" ;

$ch = curl_init();

curl_setopt($ch, CURLOPT_USERAGENT, $useragent); //set our user agent
curl_setopt($ch, CURLOPT_URL, $url); //set the url we want to use
curl_setopt($ch, CURLOPT_POST, 1); 
curl_setopt($ch, CURLOPT_POSTFIELDS, $postRequest); //set data to post
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // this line makes it work under https 

$result = curl_exec($ch); //execute and get the results
curl_close($ch); 

$results = array();
parse_str($result, $results); 
$error = '';

//echo(htmlspecialchars($postRequest));
//echo('<br/>');
//echo(htmlspecialchars($result));
//die;

switch ($results['result']) {
  case '0'     : $authorised = true;
                 break;
  case '1'     : $error = 'Invalid Auth ID';
                 break;
  case '2'     : $error = 'Invalid Terminal Id';
                 break;
  case '3'     : $error = 'Invalid Merchant Id';
                 break;
  case '4'     : $error = 'Merchant Status Invalid';
                 break;
  case '5'     : $error = 'System Error';
                 break;
  case '6'     : $error = 'Transaction Declined';
                 break;
  case '7'     : $error = 'CSC / CVV is Mandatory';
                 break;
  case '8'     : $error = 'Authorisation in progress';
                 break;
  case '9'     : $error = 'Already Authorised.';
                 break;
  case '10'    : $error = 'AVS is Mandatory';
                 break;
  case '11'    : $error = 'Success URL not present';
                 break;
  case '12'    : $error = 'Failure URL not present';
                 break;
  case '13'    : $error = 'Invalid Digest';
                 break;
  case '14'    : $error = 'Merchant Authentication Failed';
                 break;
  case '15'    : $error = 'Invalid Subscription Reference';
                 break;
  case '16'    : $error = 'Invalid Merchant Login Id';
                 break;
  case '17'    : $error = 'Merchant Login Id already exists';
                 break;
  case '18'    : $error = 'Wallet Authentication Failed';
                 break;
  case '19'    : $error = 'Invalid Wallet Login Id';
                 break;
  case '20'    : $error = 'Wallet Login Id already exists';
                 break;
  case '21'    : $error = 'Invalid Card reference number';
                 break;
  case '22'    : $error = 'MTR is mandatory';
                 break;
  case '23'    : $error = 'Cardholder name is mandatory';
                 break;
  case '24'    : $error = 'Post code is mandatory';
                 break;
  case '25'    : $error = 'CVV Not Matched';
                 break;
  case '26'    : $error = 'Post code not matched';
                 break;
  case '27'    : $error = 'Address not matched';
                 break;
  case '34001' : $error = 'Invalid Card Number';
                 break;
  case '34002' : $error = 'Invalid Date format';
                 break;
  case '34007' : $error = 'Card not accepted';
                 break;
  case '34009' : $error = 'Transaction Barred';
                 break;
  case '34010' : $error = 'Invalid Issue Number';
                 break;
  case '34011' : $error = 'Invalid Start Date';
                 break;
  case '34012' : $error = 'Invalid Expiry Date';
                 break;
  case '34014' : $error = 'Expired Card';
                 break;
  case '34015' : $error = 'Prevalid Card YESpay';
                 break;
  case '34016' : $error = 'Currency not supported';
                 break;
  case '34017' : $error = 'Missing currency information';
                 break;
  case '34018' : $error = 'EPOS Server Offline';
                 break;
  case '34020' : $error = 'Refund amount exceeds original transaction amount.'; 
                 break;
  default      : $error = 'Unknown Error: ' . $results['result'];
                 break;
}

if (!$authorised) {
  $cardData['error'] = $error;
} else {
  $cardData['altReference'] = $results['PGTR']; 
  $cardData['authCode']     = $results['authCode']; 
  $cardData['processID']    = $results['MTR']; 
}  
?>
