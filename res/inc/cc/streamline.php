<?php
require_once ("..\\..\\inc\\cardFunctions.php");

define('SITE_AUTHPROCESS', 'esolutions');
define('STREAMLINE_MERCHANT', 'ODYSSEY');
define('STREAMLINE_PASSWORD', '5t0rmb3lf@st!');
define('STREAMLINE_CURRENCYCODE', 'GBP');
define('STREAMLINE_ORDERDESC', 'Storm Cinemas Tickets');
define('STREAMLINE_SERVER', 'https://ODYSSEY:5t0rmbelfast@secure-test.streamline-esolutions.com/jsp/merchant/xml/paymentService.jsp');
//define('STREAMLINE_SERVER', 'www.admit-one.co.uk/index.php');


function showError($msg){
  echo('ERROR>>' . $msg);
  die;
}
  
function authoriseCard(&$cardData) {
  $cardData['reference'] = "OLCT".time();

  $cardTag = '';
  switch ($cardData['cardType']) {       
    case CT_VISA       : $cardTag = 'VISA-SSL'; break;
    case CT_AMEX       : $cardTag = 'AMEX-SSL'; break;
    case CT_MASTERCARD : $cardTag = 'ECMC-SSL'; break;
    case CT_DINERS     : $cardTag = 'DINERS-SSL'; break;
    case CT_LASER      : $cardTag = 'LASER-SSL'; break;
    case CT_JCB        : $cardTag = 'JCB-SSL'; break;
    case CT_SOLO       : $cardTag = 'SOLO_GB-SSL'; break;
    case CT_MAESTRO    : $cardTag = 'SWITCH-SSL'; break;
  }  

  //$xml is the order string to send to Streamline
  $xml='
<?xml version="1.0"?>
<!DOCTYPE paymentService PUBLIC "-//streamline-esolutions//DTD Streamline PaymentService v1//EN" "http://dtd.streamline-esolutions.com/paymentService_v1.dtd">
<paymentService version="1.1" merchantCode="' . STREAMLINE_MERCHANT . '">
  <submit>
    <order orderCode="' . $cardData['reference'] . '">
      <description>' . STREAMLINE_ORDERDESC .  '(' . $cardData['reference'] . ')</description>
      <amount value="' . $cardData['value'] . '" currencyCode="' . STREAMLINE_CURRENCYCODE . '" exponent="2"/>
      <paymentDetails>
        <' . $cardTag . '>
          <cardNumber>' . $cardData['cardNumber'] . '</cardNumber>
          <expiryDate>
            <date month="' . $cardData['expMonth'] . '" year="' . $cardData['expYear'] . '"/>
          </expiryDate>
          <cardHolderName>"' . $cardData['nameOnCard'] . '</cardHolderName>
          <cvc>451</cvc>
        </' . $cardTag . '>
      </paymentDetails>
    </order>
  </submit>
</paymentService>';
   
  //make socket connection with Streamline
  //the curl library is used. make sure you have it installed propperly
  // more info: http://www.php.net/manual/en/ref.curl.php
  echo(STREAMLINE_SERVER . '<br/>' . htmlspecialchars($xml) . '</br>');
  $pwd = STREAMLINE_MERCHANT . ':5t0rmbelfast';// . STREAMLINE_PASSWORD;
  echo($pwd . '</br>');
  
  $ch = curl_init(STREAMLINE_SERVER);
  curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0"); 
  curl_setopt($ch, CURLOPT_PORT, 443);   
  curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);   
  curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);  
  curl_setopt($ch, curlOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, curlOPT_SSL_VERIFYHOST, 2);
  //curl_setopt($ch, CURLOPT_USERPWD, $pwd);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $xml); //$xml is the xml string
  curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
//  curl_setopt($ch, CURLOPT_HTTPAUTH, 1); 
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 0);

  $result = curl_exec($ch); // result will contain XML reply from Streamline curl_close ($ch);
  curl_close ($ch);
  echo('>>>>');
  print_r($result);
  die;
  if ( $result == false ) {
    showError('could not communicate with Streamline. Please check back in the future.');
    return false;
  }
  print_r('STREAMLINE RESULT>>>' . $result);
  die;
  //now we have the result from Streamline containing the xml answer. we need to parse this through the XML parser
  // initialize parser //using SAX parser

  $resultxml = simplexml_load_string($result);
  $resultNode = $resultXML->reply;
  if ($resultNode == null) {
    showError('invalid result from Streamline.');
    return false;
  }
  
  $error = $resultNode->error;
  if ($error != null) {
    $errorCode = intval(getAttribute($error));
    redirect('payment', 'e=The card details entered cannot be accepted by streamline.', true);
    return false;
  }
}

$cardData = array();
  $cardData['cardType']     = '1';
  $cardData['cardNumber']   = '4444333322221111';
  $cardData['cvv']          = '123';
  $cardData['nameOnCard']   = 'Peter morton';
  $cardData['emailAddress'] = 'peter.morton@cslimited.co.uk';
  $cardData['expMonth']     = '01';
  $cardData['expYear']      = '2009';
  $cardData['value']        = '1720';

  AuthoriseCard($cardData);

?>     
