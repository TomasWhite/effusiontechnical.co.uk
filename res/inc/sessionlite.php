<?php

class olct_session {
	var $_siteCode;
  var $_dbCursor = null;
	var $_dbResource = '';
	var $_dbErrorNum = 0;
	var $_dbErrorMsg = '';
	
  // Constructor-------------------------------------------------------------------------
	public function __construct($s)
  {
  	$this->_siteCode = $s;

    if (!($this->_dbResource = @mysql_connect(CONFIG_DBHOST, CONFIG_DBUSER, CONFIG_DBPASS, true ))) {
      die('Unable to create database session!');
    }
  
    mysql_query( "SET NAMES latin1", $this->_dbResource);
	  mysql_query( "SET CHARACTER SET latin1", $this->_dbResource);

  	if (!mysql_select_db( CONFIG_DBNAME, $this->_dbResource )) {
      die('Unable to connect to database!');
  	}
	}
  
  // Constructor-------------------------------------------------------------------------
	public function __destruct()
  {
  }
  
    /**
	* Execute the query
	* @return mixed A database resource if successful, FALSE if not.
	*/
	function dbQuery($sql) 
  {
		$this->_dbErrorNum = 0;
		$this->_dbErrorMsg = '';
		$this->_dbCursor = mysql_query($sql, $this->_dbResource);
		
    if (!$this->_dbCursor) {
			$this->_dbErrorNum = mysql_errno( $this->_dbResource );
			$this->_dbErrorMsg = mysql_error( $this->_dbResource )." SQL=$sql";
			die($this-> _dbErrorMsg);
			return false;
		}
		return $this->_dbCursor;
	}
  
  /**
	* This global function loads the first row of a query into an object
	* If an object is passed to this function, the returned row is bound to the existing elements of <var>object</var>.
	* If <var>object</var> has a value of null, then all of the returned query fields returned in the object.
	* @param string The SQL query
	* @param object The address of variable
	*/
	function dbLoadObject($sql, &$object ) {
		if ($object != null) {
			if (!($cur = $this->dbQuery($sql))) {
				return false;
			}
			
      if ($array = mysql_fetch_assoc( $cur )) {
				mysql_free_result( $cur );
				mosBindArrayToObject( $array, $object, null, null, false );
				return true;
			} else {
				return false;
			}
		} else {
			if ($cur = $this->dbQuery($sql)) {
				if ($object = mysql_fetch_object( $cur )) {
					mysql_free_result( $cur );
					return true;
				} else {
					$object = null;
					return false;
				}
      } else {
				return false;
			}
		}
	} 

  //----------------------------------------------------------
  function getScheduleDates()
  {
    $s = $_SESSION['siteCode'];

    $sql = "select distinct scheduleDate from sup_performance where siteCode = '" . $s . "' and " .
           "ScheduleDate >= current_date and OffSale = 0 order by ScheduleDate";
           
		if (!($cur = $this->dbQuery($sql))) {
			return null;
		}
    $array = array();
    $index = 0;
		while ($row = mysql_fetch_object( $cur )) {
  		$array[$index] = strtotime($row->scheduleDate);
      $index++;
    }  

		mysql_free_result( $cur );
		return $array;
  }  
  
  //---------------------------------
  private function readEvents($sql) {
    if (!($cur = $this->dbQuery($sql))) {
			return null;
		}
    $array = array();
    $event = null;
    
		while ($row = mysql_fetch_object( $cur )) {
  		if ($event == null || $row->eventCode != $event->eventCode) {
        $event = new olct_event($row);
        $array[] = $event;
      }

      $date = $event->addDate($row->scheduleDate);
      $performance = new olct_performance($row);
      $date->addPerformance($performance);
    }  

    mysql_free_result( $cur );
		return $array;
  } 
  
  //----------------------------------------------------------
  function getEventList($startDate, $endDate)
  {
    $sql = "select performanceCode, SoldOut, P.planCode, P.eventCode, hallName, scheduleDate, performanceTime, E.rating, E.genre, " .
           "E.releaseDate, E.runTime, E.ratingText, filmTitle, E.distrib, E.web, E.director, E.writer, E.producer, " .
           "E.castMembers, E.trailerlink, E.synopsis ".
           "from sup_performance P join sup_event E on E.EventCode = P.EventCode " .
           "where P.siteCode = '" . $this->_siteCode . "' and " .
           "ScheduleDate >= " . sqlDate($startDate) . " and ScheduleDate <= " . sqlDate($endDate) . " and " .
           "OffSale = 0 and performanceTime > " . sqltimestamp(time() + (60*SITE_SHOWCUTOFF)) . " order by FilmTitle, ScheduleDate, PerformanceTime";

   $array = $this->readEvents($sql);
    return $array;
  }

  //----------------------------------------------------------
  function getSingleEvent($eventCode)
  {
    $sql = "select performanceCode, SoldOut, P.planCode, P.eventCode, hallName, scheduleDate, performanceTime, E.rating, E.genre, " .
           "E.releaseDate, E.runTime, E.ratingText, filmTitle, E.distrib, E.web, E.director, E.writer, E.producer, " .
           "E.castMembers, E.trailerlink, E.synopsis ".
           "from sup_performance P join sup_event E on E.EventCode = P.EventCode " .
           "where P.siteCode = '" . $this->_siteCode . "' and " .
           "ScheduleDate >= current_date and P.eventCode = '" . $eventCode . "' and " .
           "OffSale = 0 and performanceTime > " . sqltimestamp(time() + (60*SITE_SHOWCUTOFF)) . " order by FilmTitle, ScheduleDate, PerformanceTime";

    $array = $this->readEvents($sql);
    if (count($array) == 0) {
      return null;
    } else {
      return $array[0];
    }  
  }  
  
  //----------------------------------------------------------
  function getNonLinkedEvent($eventCode)
  {
    $sql = "select E.eventCode, E.rating, E.genre, E.releaseDate, E.runTime, E.ratingText, filmTitle, ".
           "E.distrib, E.web, E.director, E.writer, E.producer, E.castMembers, E.trailerlink, E.synopsis " .
           "from sup_event E where E.eventCode = '" . $eventCode . "'";

    if (!($cur = $this->dbQuery($sql))) {
			return null;
		}
    $event = null;
    
		if ($row = mysql_fetch_object( $cur )) {
      $event = new olct_event($row);
    }
    mysql_free_result( $cur );
    
    return $event;
  } 
  
  //----------------------------------------------------------
  function getPerformanceEvent($performanceCode)
  {
    $sql = "select performanceCode, SoldOut, P.planCode, P.eventCode, hallName, scheduleDate, performanceTime, E.rating, E.genre, " .
           "E.releaseDate, E.runTime, E.ratingText, filmTitle, E.distrib, E.web, E.director, E.writer, E.producer, " .
           "E.castMembers, E.trailerlink, E.synopsis ".
           "from sup_performance P join sup_event E on E.EventCode = P.EventCode " .
           "where P.siteCode = '" . $this->_siteCode . "' and " .
           "ScheduleDate >= current_date and P.performanceCode = '" . $performanceCode . "' and " .
           "OffSale = 0 order by PerformanceTime";

    $perf = null;
    
    if (!($cur = $this->dbQuery($sql))) {
			return null;
		}
    $array = array();
    $event = null;
    
		while ($row = mysql_fetch_object( $cur )) {
      $perf = new olct_performanceEvent($row);
      break;
    }

    mysql_free_result( $cur );
		return $perf;
  }  
}


?>