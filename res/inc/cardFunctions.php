<?php
define (CT_NONE, '0');  
define (CT_VISA, '1');  
define (CT_AMEX, '2');  
define (CT_MASTERCARD, '3');  
define (CT_DINERS, '4');  
define (CT_LASER, '5');  
define (CT_JCB, '6');  
define (CT_SOLO, '7');  
define (CT_MAESTRO, '8');  
  
  //----------------------------------------------------------------------------------------------------
  function addOption($display, $value, $set) {
    $res = '<option value="'. $value . '"';
    if ($value == $set) {
      $res .= ' SELECTED';
    }  
    $res .= '>' . $display . '</option>';
    return $res;
  }  
    
  //----------------------------------------------------------------------------------------------------
  function monthSelect($name) {
    $v = $_POST[$name] == null ? '0' : strval($_POST[$name]);
    $res = '<select name="' . $name . '">';
    $res .= addOption('----', '0', $v);
    for ($i=1; $i<13; $i++) {
      $res .= addOption(date('M', mktime(0,0,0,$i,1,2000)), strval($i), $v);
    }
    return $res . '</select>';
  }

  //----------------------------------------------------------------------------------------------------
  function cardTypeSelect() {
    $v = $_POST['cardType'] == null ? '0' : strval($_POST['cardType']);
    $res = '<select STYLE="width: 170px" name="cardType" onChange="showStart(this.form.cardType)">';
    $res .= addOption('----', CT_NONE, $v);
    $res .= addOption('Visa', CT_VISA, $v);
    if (SITE_AMEXALLOWED == true) {
      $res .= addOption('Amercian Express', CT_AMEX, $v);
    }
    $res .= addOption('Mastercard', CT_MASTERCARD, $v);
    if (SITE_DINERSALLOWED == true) {
      $res .= addOption('Diners', CT_DINERS, $v);
    }  
    if (SITE_LASERALLOWED == true) {
      $res .= addOption('Laser', CT_LASER, $v);
    }  
    if (SITE_JCBALLOWED == true) {
      $res .= addOption('JCB', CT_JCB, $v);
    }  
    if (SITE_SOLOALLOWED == true) {
      $res .= addOption('Solo', CT_SOLO, $v);
    }  
    if (SITE_MAESTROALLOWED == true) {
      $res .= addOption('Maestro', CT_MAESTRO, $v);
    }  
    
    return $res . '</select>';
  }  

  //----------------------------------------------------------------------------------------------------
  function yearSelect($name, $desc) {
    $v = $_POST[$name] == null ? '0' : strval($_POST[$name]);
    $res = '<select name="' . $name . '">';
    $res .= addOption('----', '0', $v);
    $year = intval(date('Y', time()));
    if ($desc == true) {
      for ($i=0; $i < 10; $i++) {
        $res .= addOption(strval($year), strval($year), $v);
        $year--;
      }
    } else {  
      for ($i=0; $i < 10; $i++) {
        $res .= addOption(strval($year), strval($year), $v);
        $year++;
      }
    }  
    return $res . '</select>';
  }

  //----------------------------------------------------------------------------------------------------
  function checkIIN($cardNumber, $prefixes, $lengths) {
    //prefix first.  Prefixes contain comma list of IIN matches
    $array = split(',', $prefixes);
    $valid = false; 
    foreach($array as $prefix) {
      if (ereg('^' . $prefix, $cardNumber)) {
        $valid = true;
        break;
      }
    }
    if (!$valid) {
     return false; 
    }

    //Lengths Now. $lengthscontain comma list of PAN lengths
    $array = split(',', $lengths);
    foreach($array as $length) {
      if (strlen($cardNumber) == intval($length)) {
        return true;
      }
    }
 
    return false; 
  }

  //----------------------------------------------------------------------------------------------------
  function loadGiftBalance($cardNumber, &$cardDetails) {
    if (strlen($cardNumber) == 14) {
      $xml = new XMLClient(XMLID_GIFTCARDCONTROL);
      $xml->writeStr(TAG_ACTION, 'balance');
      $xml->writeStr(TAG_ACCOUNTCODE, hashStr($cardNumber, SITE_CIRCUITCODE));
      $result = $xml->send(SITE_PORTALADDRESS);
      if ($result != null && $result->resultCode == ERR_SUCCESS) {
        $node = $result->findNode('scheme');
        if ($node != null) {
          $cardDetails['giftCardBalance'] = $node->readInt('balance');
          if ($node->readInt('balance') == 0) {
            $cardDetails['postError'] = 'No balance available on the gift card';
            return false;
          }
          return true; 
        }  
      }
    }
    
    $cardDetails['postError'] = 'Invalid Gift card number';
    return false;    
  }
  
  //----------------------------------------------------------------------------------------------------
  function validateLoyaltyCard($cardNumber, &$cardDetails) {
    if (strlen($cardNumber) == 14) {
      $xml = new XMLClient(XMLID_GETSCHEME);
      $xml->writeStr(TAG_ACCOUNTCODE, hashStr($cardNumber, SITE_CIRCUITCODE));
      $result = $xml->send(SITE_PORTALADDRESS);
      if ($result != null && $result->resultCode == ERR_SUCCESS) {
        $node = $result->findNode('scheme');
        if ($node != null) {
          $bal = $node->readInt('points');

          $node = $result->findNode('customer');
          if ($node != null) {
            $cardDetails['customerCode'] = $node->getStrAttr('customerCode');
            $cardDetails['loyaltyBalance'] = $bal;
            return true; 
          }  
        }  
      }
    }
    
    $cardDetails['postError'] = 'Invalid Loyalty account';
    return false;    
  }

  //----------------------------------------------------------------------------------------------------
  function checkLUHN($creditCard) {
    if (strlen($creditCard) < 10) {
      return false;
    }
    
    $checksum = substr ($creditCard, -1);    // returns checksum 
    $restnum = substr($creditCard, 0, strlen($creditCard)-1);  // returns rest of credit card number 
    $calcvals = ""; 

    for ($i = strlen($restnum)+1; $i >= 0; $i=$i-2) {    
       $calcvals = $calcvals . strval(intval(substr($restnum,$i,1))*2); 
    } 

    for ($i = strlen($restnum) -2; $i >= 0; $i=$i-2) {    
      $calcvals = $calcvals . strval(intval(substr($restnum,$i,1))*1); 
    } 
    $calcvals = substr($calcvals,1); 
    $count = 0; 

    for ($i = 0; $i <= strlen($calcvals); $i++) { 
      $count = $count + intval(substr($calcvals,$i,1)); 
    } 

    if(intval(substr(strval((10*(intval(substr(strval($count),0,1))+1)-$count)),-1))==$checksum) { 
      return true; 
    } else { 
      return false; 
    } 
  } 

  //----------------------------------------------------------------------------------------------------
  function checkCreditCard(&$cardData) {
    // Check that the number is numeric and of the right sort of length.
    $cardNumber = $cardData['cardNumber'];
    
    if (!eregi('^[0-9]{13,19}$', $cardNumber) || !checkLUHN($cardNumber))  {
      $cardData['postError'] = LANG_INVALIDCARDNUMBER;
      return false; 
    }

    $valid = false;
    switch ($cardData['cardType']) {
      case CT_VISA       : $valid = checkIIN($cardNumber, '4', '13,16');
                           break;
      case CT_AMEX       : $valid = checkIIN($cardNumber, '34,37', '15');
                           break;
      case CT_MASTERCARD : $valid = checkIIN($cardNumber, '51,52,53,54,55', '16');
                           break;
      case CT_DINERS     : $valid = checkIIN($cardNumber, '30,36,38', '14');
                           break;
      case CT_SOLO       : // same as laser
      case CT_LASER      : $valid = checkIIN($cardNumber, '63,67', '16,18,19');
                           break;
      case CT_JCB        : $valid = checkIIN($cardNumber, '3,1800,2131', '15,16');
                           break;
      case CT_SWITCH     : $valid = checkIIN($cardNumber, '4903,4905,4911,4936,5641,633,675', '16,18,19');
                           break;
      case CT_MAESTRO    : $valid = checkIIN($cardNumber, '4444,5020,6', '16,18,19');
                           break;
    }

    if (!$valid) {
      $cardData['postError'] = LANG_INVALIDNUMBERFORTYPE;
      return false;
    }
    
    if ($cardData['cardType'] == CT_MAESTRO) {
      $issueValid = ($cardData['issue'] > 0 && $cardData['issue'] < 99) ? true : false;
      $startMonth = intval($cardData['startMonth']);
      $startYear  = intval($cardData['startYear']);
      if (checkDate($startMonth, 1, $startYear) === true) {
        $startDate = mktime(0, 0, 0, $startMonth, 1, $startYear);
        $cardData['startDate'] = $startDate;
      } else {
        $startDate = 0;
      }  
       
      $startValid = ($startDate == 0 || $startDate > time()) ? false : true;
      if ($startValid == false && $issueValid == false) {
        $cardData['postError'] = LANG_ISSUEREQUIRED;
        return false;
      }   
    }
    
    $expMonth = intval($cardData['expMonth']);
    $expYear  = intval($cardData['expYear']);
    
    $expiryDate = 0;
    for ($i=31; $i >= 28; $i--) {
      if (checkDate($expMonth, $i, $expYear) === true) {
        $expiryDate = mktime(0, 0, 0, $expMonth, $i, $expYear);
        break;
      }
    }
    if ($expiryDate == 0 || $expiryDate < time()) {
      $cardData['postError'] = LANG_INVALIDEXPIRY;
      return false;
    }
    $cardData['expiryDate'] = $expiryDate;
    
    if (strlen($cardData['cvv']) < 3 || strlen($cardData['cvv']) > 4 || !is_numeric($cardData['cvv'])) {
      $cardData['postError'] = LANG_INVALIDCVV;
      if (SITE_AMEXALLOWED == true) {
        $cardData['postError'] .= ' ' . LANG_CVVAMEX;
      }  
      return false;
    }

    return true;
  }

  function saveSessionCardData(&$cardData) {
    $res = '';
    foreach ($cardData as $key=>$value) {
      $res .= $key . '>' .$value . '|';
    }
    unset($_SESSION['cardData']);
    session_unregister('cardData');
    $_SESSION['cardData'] = $res;
    session_write_close();
  }
    
  function readSessionCardData() {
    $strs = explode('|', $_SESSION['cardData']);
    $array = array();
    foreach($strs as $value) {
      $vals = explode('>', $value);
      $array[$vals[0]] = $vals[1];
    }
    return $array;
  }
?>