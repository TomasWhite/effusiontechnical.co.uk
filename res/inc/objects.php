<?php
$lastSeat = null;
$workSeats = null;

/*=========================================================================
OLTC_EVENT
=========================================================================*/

class olct_event {
 var $title;
 var $eventCode;
 var $rating;   
 var $eventURL;  
 var $genre;
 var $releaseDate;
 var $runTime;
 var $ratingText;
 var $distributor;
 var $webSite;   
 var $director;  
 var $synopsis;  
 var $writer;    
 var $producer;  
 var $trailer;
 var $dates;
 var $lastDate = '';
 var $dateCount = 0;
     
	public function __construct($row)
  {
    $this->eventCode   = $row->eventCode;
    $this->title       = $row->filmTitle;
    $this->eventURL    = makeURL('details', 'eventCode=' . $row->eventCode);
    $this->genre       = $row->genre;
    $this->releaseDate = strtotime($row->releaseDate);
    $this->runTime     = $row->runTime;
    $this->ratingText  = $row->ratingText;
    $this->distributor = $row->distrib;
    $this->webSite     = $row->web;
    $this->director    = $row->director;
    $this->synopsis    = $row->synopsis;
    $this->writer      = $row->writer;
    $this->producer    = $row->producer;
    $this->trailer     = $row->trailerlink;
    
    $ratings = split(';', strtoupper($row->rating));
    $lastR  = '';
    foreach($ratings as $rating) {
      $codes = split(':', $rating);
      if (count($codes) == 1) {
        $lastR = $codes[0];
        break;
      }
      $lastR = $codes[1];
      if ($codes[0] == SITE_RATINGCODE) {
        break;
      }  
    }
    
    $this->rating = $lastR;

    $img = '/ratings/' . SITE_RATINGCODE . '/' . $lastR . 'sml.gif';
    if (!file_exists(CONFIG_PATH . $img)) {
      $this->smallRating = '/ratings/' . SITE_RATINGCODE . '/tbcsml.gif';
    } else {
      $this->smallRating = $img;
    }

    $img = '/ratings/' . SITE_RATINGCODE . '/' . $lastR . 'lrg.gif';
    if (!file_exists(CONFIG_PATH . $img)) {
      $this->largeRating = $this->smallRating;
    } else {
      $this->largeRating = $img;
    }

    $img = '/filmimages/small/' . $row->eventCode . '.jpg';
    if (!file_exists(CONFIG_PATH . $img)) {
      $this->smallImage = '/images/' . SITE_LOCATIONCODE . '/noimage.jpg';
    } else {
      $this->smallImage = $img;
    }

    $img = '/filmimages/large/' . $row->eventCode . '.jpg';
    if (!file_exists(CONFIG_PATH . $img)) {
      $this->largeImage = $this->smallImage;
    } else {
      $this->largeImage = $img;
    }
    
    $dates = array();
  } 
  
  function addDate($date)
  {
    if ($this->lastDate == '' || $this->lastDate != $date) {
      $this->lastDate = $date;
      $this->dates[$this->lastDate] = new olct_scheduleDate($date);
      $this->dateCount++;
    }  
    return $this->dates[$this->lastDate];
  }
}  

/*=========================================================================
OLTC_SCHEDULEDATE
=========================================================================*/
class olct_scheduleDate {
  var $date;
  var $performances;
  var $performanceCount;
  
	public function __construct($date)
  {
    $this->date = strtotime($date);
    $this->performances = array();
  } 
  
  function addPerformance($performance)
  {
    $this->performances[] = $performance;
    $this->performanceCount++;
  }
}  

/*=========================================================================
OLTC_TICKET
=========================================================================*/
class olct_ticket {
  var $name;
  var $seat;
  var $seatCode;
  var $code;
  var $qty;
  var $price;
  var $chargeCode;
  var $chargeValue;
  var $ticketKey;
  
	public function __construct($row)
  {
    $this->name = $row->printDescription == '' ? $row->description : $row->printDescription;
    $this->seat = $row->seat;
    $this->seatCode = $row->seatCode;
    $this->code = $row->ticketCode;
    $this->qty = $row->quantity == 0 ? 1 : $row->quantity;
    $this->price = $row->price;
    $this->ticketKey = $row->ticketCode . '|' . $row->seatCode;
    $this->chargeCode = $row->chargeCode;
    $this->chargeValue = $row->chargeValue;
  } 
  
  public function totalPrice() {
    return $this->price + ($this->chargeValue * $this->qty);
  }  
  
}  

/*=========================================================================
OLTC_PERFORMANCE
-----------------------------------------------*/

class olct_performance {
  var $code;
  var $hall;
  var $time;
  var $availability;
  var $bookingURL;
  var $planCode;
  var $soldOut;
  
	public function __construct($row)
  {
    $this->code = $row->performanceCode;
    $this->hall  = $row->hallName;
    $this->availability = 0;
    $this->time = strtotime($row->performanceTime);
    $this->planCode = strval($row->planCode);
    if ($this->planCode != '') {
      $this->allocated = true;
    } else {
      $this->allocated = false;
    }

    $this->soldOut = intval($row->SoldOut) > 0 ? true : false;
    
    if ($this->soldOut == true) {
      $this->bookingURL = "";
    } else {
      $this->bookingURL = makeURL('tickets', 'perfCode=' . $row->performanceCode);
    }  
  } 
}  

/*=========================================================================
OLTC_PRICECARD
=========================================================================*/
class olct_priceCard {
  var $perfCode;
  var $prices = array();
  
  //-----------------------------------------------------------------------
  function __construct($performanceCode) {  
    global $olct_session;
    
    if (defined('DBSITECODE')) { 
      $s = DBSITECODE;
    } else {
      $s = $_SESSION['siteCode'];
    }  
    
    $this->perfCode = $performanceCode;
    $sql = "select tt.ticketCode, tt.description, tt.printDescription, tt.quantity, price, tt.chargeCode, tt.chargeValue, " .
           "st.seatCode, st.Description as seat, st.dflt " .
           "from sup_performanceTicket pt " .
           "join sup_ticketType tt on tt.siteCode = pt.SiteCode and tt.ticketCode = pt.ticketCode " .
           "join sup_seatType st on st.siteCode = pt.SiteCode and st.seatCode = pt.seatCode " .
           "where pt.siteCode = '" . $s . "' and pt.PerformanceCode = '" . $performanceCode . "' " .
           "and OffSale = 0 order by tt.priority, tt.description";
    
    if (!($cur = $olct_session->dbQuery($sql))) {
      return;
		}
    
   	while ($row = mysql_fetch_object( $cur )) {
      $ticket = new olct_ticket($row);
      $this->prices[] = $ticket;
    }
    mysql_free_result( $cur );
  } 
            
  //-----------------------------------------------------------------------
  public function getTicket($ticketKey) {
    foreach ($this->prices as $ticket) {
      if ($ticket->ticketKey == $ticketKey) {
        return $ticket;
      }
    }
    return null;    
  }
  
  public function prices()
  {
    return $this->prices;
  }
  
  //-----------------------------------------------------------------------
  public function getSelectionForm($tableClass = "") {
    $res = $tableClass == '' ? '<table cellspacing="5">' : '<table class="' . $tableClass . '" cellspacing="5">';
    $res .= '<form name="tixSelect" id="tixSelect" action="' . makeURL('tickets') . '" method="get">';

    foreach ($this->prices as $ticket) {
      $res .= '<tr><td width="50"><select name="' . $ticket->ticketKey . '" id="' . $ticket->ticketKey . '">';
      $val = $_GET[$ticket->ticketKey];
      
      $res .= '<option value="0"';
      if ($val == '0' || $val == '') {
        $res .= ' selected';
      }
      $res .= '>' . LANG_NONE . '</Option>';
      
      for ($i = 1; $i <= SITE_MAXTICKETSELECTION; $i++) {
        $res .= '<option ';
        if ($val == strval($i)) {
          $res .= 'selected ';
        }  
        $res .= 'value="' . $i . '">' . $i . '</Option>';
      }  
      $res .= '</Select></td><td width="*">' . $ticket->name . ' ' . $ticket->seat . ' ' . 
	      LANG_TICKETSAT . ' ' . currencyStr($ticket->totalPrice()) . 
		  '</td</tr>';
    }
    $res .= '<tr><td colspan="2" align="right">';
    $res .= '<input type="hidden" name="perfCode" value="' . $this->perfCode . '">' .
            '<input type="hidden" name="action" value="process">'.
            '<input type="hidden" name="p" value="tickets">'.
            '<input type="hidden" name="s" value="' . SITE_CODE . '">'.
            '<input type="reset" value="' . LANG_RESET . '">'.
            '<input type="submit" value="' . LANG_SUBMIT . '">';
    $res .= '</td></tr></form></table>';  
    return $res;
  }
}

/*=========================================================================
OLTC_PERFORMANCEEVENT
=========================================================================*/
class olct_performanceevent extends olct_performance {
  var $event;
  var $__priceCard = null;
  
	public function __construct($row)
  {
    parent::__construct($row);
    $this->event = new olct_event($row);
  }

  function getPriceCard() {  
    if ($this->__priceCard == null) {
      $this->__priceCard = new olct_priceCard($this->code);
    }    
    return $this->__priceCard;
  } 
}  

/*=========================================================================
OLTC_SEATPLANSEAT
=========================================================================*/
class olct_seatplanseat {
  var $seatID;       
  var $seatTypeCode; 
  var $colID;        
  var $rowID;        
  var $rowLabel;     
  var $colLabel;     
  var $label;        
  var $available; 
  var $wheelChair;
  
  public function __construct($arr) {
    $this->seatID = $arr[0];       
    $this->seatTypeCode = $arr[1]; 
    $this->sectionID = $arr[2];
    $this->colID = $arr[3];
    $this->rowID = $arr[4];
    $this->rowLabel = $arr[5];     
    $this->colLabel = $arr[6];     
    $this->label = $arr[5] . SITE_LABELSEPERATOR . $arr[6];        
    $this->wheelChair = $arr[7];
  }  
}

/*=========================================================================
OLTC_SEATPLAN
=========================================================================*/

class olct_seatplan {
  var $seats = array();
  var $error = false;
  var $numCols = 0;
  var $numRows = 0;
  
  //-----------------------------------------------------------------------------------------------------------
	public function __construct($performanceCode, $planCode)
  {
    global $olct_session;

    if (defined('DBSITECODE')) { 
      $s = DBSITECODE;
    } else {
      $s = $_SESSION['siteCode'];
    }  

    $sql = "select planData from sup_seatPlan where siteCode = '" . $s . "' and " .
           "planCode = '" . $planCode . "'"; 

    if (!($cur = $olct_session->dbQuery($sql))) {
			return null;
		}

    $avail = new xmlClient(XMLID_GETPERFORMANCEAVAILABILITY);
    $avail->writeStr(TAG_PERFORMANCECODE, $performanceCode);
    $avail->writeStr('seatMap', 'Y');
    
    $response = $avail->send(SITE_PORTALADDRESS);
    if ($response == null || $response->resultCode != ERR_SUCCESS) {
      mysql_free_result( $cur );
      $this->error = true;
      return;
    }  
    
    $node = $response->findNode('plan');
    $map = str_split($node->readStr('seatMap'));
    $array = null;
		while ($row = mysql_fetch_object( $cur )) {
  		$array = explode('~', $row->planData);
      break;
    }

    mysql_free_result( $cur );
    
    foreach ($array as $seat) {
      $seatData = explode('|', $seat);
      $seat = new olct_seatplanseat($seatData);
      $seat->available = $map[$seat->seatID] == '0' ? true : false;
      $this->seats[$seat->seatID] = $seat;

      if ($seat->colID > $this->numCols) {
        $this->numCols = $seat->colID;
      }  

      if ($seat->rowID > $this->numRows) {
        $this->numRows = $seat->rowID;
      }  
    }
    
    $this->numRows++;
    $this->numCols++;
  }  

  //------------------------------------------------------------------------------------------------------------
  function seatAvail($id) {
    global $lastSeat, $workSeats;
    
    if ($id < 0 || $id >= count($this->seats)) {
      return false;
    }
    
    $seat = $this->seats[$id];
    
    //No Availability, Different Rows or we hit an aisle
    if (!array_key_exists($seat->seatID, $workSeats)) {
      if (!$seat->available) {
        return false;
      }
    }  
    $res = ($seat->rowID <> $lastSeat->rowID || abs($seat->colID - $lastSeat->colID) > 1);
    $lastSeat = $seat;
    return !$res;
  }

  //------------------------------------------------------------------------------------------------------------
  // return > 0 if all seats at this index [and to the right up to numSeats] are available (and on same row)
  function seatsAvailable($seatID, $isMySeat) {
    global $lastSeat, $workSeats; 
    
    $seatArray = array();
    $lastSeat = $this->seats[$seatID];
    $mySeatsCount = count($workSeats);
    
    if ($lastSeat->available || $isMySeat) {
      array_push($seatArray, $lastSeat);
    } else {
      return -1;
    }

    for ($i=1; $i<$mySeatsCount; $i++) {
      if ($this->seatAvail($lastSeat->seatID + 1)) {
        array_push($seatArray, $lastSeat);
      } else {
        break;
      }
    }    
    
    //Ok..  Nada going forward...So lets look back from the point where we hit the problem
    $remainingSeats = $mySeatsCount - count($seatArray);
    if ($remainingSeats > 0) {
      $lastSeat = $seatArray[0];
      for ($i=1; $i<=$remainingSeats; $i++) {
        if ($this->seatAvail($lastSeat->seatID - 1)) {
          array_unshift($seatArray, $lastSeat);
        } else {
          break;
        }
      }      
    }

    if (count($seatArray) < $mySeatsCount) {
      return -1;
    }  
    
    if (count($seatArray) > 1) {
      //Check Singleton left
      $lastSeat = $seatArray[0];
      if ($this->seatAvail($lastSeat->seatID - 1)) {
        if (!$this->seatAvail($lastSeat->seatID - 1)) {
          array_pop($seatArray);
          array_unshift($seatArray, $this->seats[$seatArray[0]->seatID - 1]);
        }  
      }

      //Check Singleton Right
      $lastSeat = $seatArray[count($seatArray) - 1];
      if ($this->seatAvail($lastSeat->seatID + 1)) {
        if (!$this->seatAvail($lastSeat->seatID + 1)) {
          array_shift($seatArray);
          array_push($seatArray, $this->seats[$seatArray[count($seatArray) - 1]->seatID + 1]);
        }  
      }
    }
    return $seatArray[0]->seatID;
  } 
  
  //-----------------------------------------------------------------------------------------------------------
  function getPlanString(&$mySeats, &$returnLabels) {
    global $lastSeat, $workSeats; 

    $workSeats = $mySeats;
    $rowLabels = array();
    $data = array();
    $denyRows = array();
    if (defined('PLAN_DENYROWS')) {
      $denyRows = str_split(PLAN_DENYROWS);
    }
    
    for ($i=0; $i < $this->numRows; $i++) {
      $rowLabels[$i] = '';
      $strs = array();
      for ($c=0; $c < $this->numCols; $c++) {
        $strs[$c] = "[-1,'','',-1]";
      }
      $data[$i] = $strs;       
    }

    foreach ($this->seats as $seat) {
      $rowLabels[$seat->rowID] = $seat->rowLabel;
      $available = 'X';

      $selectID = -1;
      $mySeat = array_key_exists($seat->seatID, $mySeats); 

      if (in_array($seat->rowLabel, $denyRows)) {
        $available = 'X';
      } elseif ($seat->wheelChair == 1) {
        $available = 'W';
      } elseif ($seat->available || $mySeat) {
        $avSeat = $this->seatsAvailable($seat->seatID, $mySeat);
        if ($avSeat > -1) {
          $selectID = $avSeat;
          $available = $mySeat == true ? 'M' : 'O';
        }
      }
      $str = "[" . $seat->seatID . ",'" . $seat->colLabel . "','" . $available . "'," . $selectID . "]";
      $data[$seat->rowID][$seat->colID] = $str;
    }
    
    $res = '[';
    $firstRow = true;
    foreach ($rowLabels as $row) {
      if (!$firstRow) {
        $res .= ',';
      }  
      $firstRow = false;
      $res .= "'" . $row . "'";
    }
    $res .= ']';
    $returnLabels = $res;

    $res = '[';
    $firstRow = true;
    foreach($data as $row) {
      if (!$firstRow) {
        $res .= ',';
      }  
      $firstRow = false;
      $res .= '[';
      $firstCol = true;
      foreach ($row as $col) {
        if (!$firstCol) {
          $res .= ',';
        }  
        $firstCol = false;
        $res .= $col;
      }
      $res .= ']';
    }      
    $res .= ']';
    
    return $res;
  }    
}  

