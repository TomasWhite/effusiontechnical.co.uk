<?php
require (".\\inc\\configuration.php");
require (".\\inc\\sessions.php");
require (".\\inc\\portalXMl.php");

//----------------------------------------------------------
// OLTC SEARCH
//----------------------------------------------------------

class olct_search extends olct_session {
var $_result = null;
	
  // Constructor-------------------------------------------------------------------------
	public function __construct()
   {
    session_start();
	$this->_result = new XMLResponse();
  	$this->setError(0);
	
	if (!($this->_dbResource = @mysql_connect(CONFIG_DBHOST, CONFIG_DBUSER, CONFIG_DBPASS, true ))) {
      $this->setError(ERR_NODATABASELIBRARY);
	  return;
    }
  
  	if (!mysql_select_db( CONFIG_DBNAME, $this->_dbResource )) {
      $this->setError(ERR_NODATABASELIBRARY);
	  return;
  	}

    $siteCode = $_GET['s'];

    if ($siteCode == null) {
      $siteCode = $_SESSION['siteCode'];
      if ($siteCode == '') {
        $this->setError(ERR_TERMINALINVALID);
	    return;
      }  
    } else {
      $_SESSION['siteCode'] = $siteCode;
    }      
    
    $sql = "Select S.siteCode, S.circuitCode, siteName from SUP_Site S where S.SiteCode = '" . $_SESSION['siteCode'] . "'";
    if ($this->dbLoadObject($sql, $siteDetails)) {
      $this->_result->writeStr('siteCode', $siteCode);
    } else {
      unset($_SESSION['siteCode']);
      $this->setError(ERR_TERMINALINVALID);
    }  
  }

  //----------------------------------------------------------
  function setError($errCode) {
    $this->_result->errorCode = $errCode;
  }
  
  //----------------------------------------------------------
  function performSearch() {
    $srch = $_GET['title'];

	if ($srch == null) {
      $this->setError(ERR_INVALIDACTION);
      return;
	}
	
	$matchStr = '';
	$match = explode(" ", $srch);
	if (count($match) > 0)
	{ 
	  foreach($match as $str) {
	    if (strlen($str) > 3) {
		  $matchStr .= '+' . $str . ' ';
		}  
	  }
	}  
	
    $sql = "select eventCode, filmTitle, releaseDate from sup_event where " .
           "soundex(filmtitle) = soundex('" . $srch . "') or ".
           "filmtitle like '%" . $srch . "%' ";
		   
	if ($matchStr != '')
      $sql .= " or MATCH (filmTitle) AGAINST ('" . $matchStr . "' IN BOOLEAN MODE) ";	
		   
	$sql .= "order by releasedate desc";
    
	$node = $this->_result->addNode('events');	   
    
	if ($cur = $this->dbQuery($sql)) {
	  while ($row = mysql_fetch_object( $cur )) {
        $pNode = $node->addNode('event');
		$pNode->setStrAttr('eventCode', $row->eventCode);
	    $pNode->writeStr('name', $row->filmTitle);	
		$pNode->writeStr('releaseDate', str_replace('-', '', $row->releaseDate));
      }
    }
    mysql_free_result( $cur );
  }

  //----------------------------------------------------------
  function getMovie() {
    $srch = $_GET['eventCode'];

	if ($srch == null) {
      $this->setError(ERR_INVALIDACTION);
      return;
	}
	
    $sql = "select * from sup_event where eventCode=" . $srch;

	if ($cur = $this->dbQuery($sql)) {
      $row = mysql_fetch_object($cur);
	  
      $pNode = $this->_result->addNode('event');
      $pNode->setStrAttr('eventCode', $row->eventCode);
	  $pNode->writeStr('name', $row->filmTitle);	
	  $pNode->writeStr('genre', $row->Genre);	
	  $pNode->writeStr('distributor', $row->Distrib);
	  $pNode->writeStr('releaseDate', str_replace('-', '', $row->releaseDate));
      $pNode->writeStr('runTime', $row->RunTime);
      $pNode->writeStr('Director', $row->Director);
      $pNode->writeStr('rating', $row->Rating);
      $pNode->writeStr('writer', $row->Writer);
      $pNode->writeStr('producer', $row->Producer);
      $pNode->writeStr('ratingAdvice', $row->RatingText);
      $pNode->writeStr('webSite', $row->Web);
      $pNode->writeStr('trailer', $row->TrailerLink);
      $pNode->writeStr('synopsis', $row->Synopsis);
	}
    mysql_free_result( $cur );
  }

  //----------------------------------------------------------
  function dumpResults() {
	$array = array();
    $this->_result->writeToStrings($array);
    
    foreach($array as $str) {
      echo($str . chr(13));
    }  
  }  
}

//----------------------------------------------------------
// MAIN
//----------------------------------------------------------

$search = new olct_search;

if ($_GET['action'] == 'search') {
  $search->performSearch();
} elseif ($_GET['action'] == 'get') {
  $search->getMovie();
} else {
  $search->setError(ERR_INVALIDACTION);
}

$search->dumpResults();
?>
