<?php
  require ("./inc/objects.php");

  $eventCode = $_GET['eventCode'];
  $event = $olct_session->getSingleEvent($eventCode);
  if ($event == null) {
    redirect(makeURL('error', 'err=' . urlencode(LANG_NOEVENTFORCODE)));
  }
  
?>
<NOSCRIPT>Your browser requires Javascript to be enabled to book tickets</NOSCRIPT> 
<?
  
include siteFile('siteheader.php');
include siteFile('siteEventDetails.php');
include siteFile('sitefooter.php');
?>
