<?php
require_once (".\\inc\\configuration.php");
require_once (".\\inc\\sessions.php");
require_once (".\\inc\\functions.php");
require (".\\inc\\portalXMl.php");
require_once(".\\inc\\xmlConstants.php");

if (!defined('__INDEX__')) {
  showError(LANG_NONAVIGATE);
}  

$newsletters = array();
for ($i = 1; $i <= 10; $i++) {
  $c = 'NEWSLETTER_' . $i;
  if (defined($c)) { 
    $s = explode('|', constant($c));
    $newsletters[] = array('ID' => $s[0], 'NAME' => $s[1]);
  } else {
    break;
  }  
}

function registerNewsletter($code, $name, $email) {
  $xml = new XMLClient(XMLID_NEWSLETTER);
  $xml->writeStr(TAG_ACTION, 'register');
  $xml->writeStr(TAG_GROUP, $code);
  $xml->writeStr(TAG_NAME, $name);
  $xml->writeStr(TAG_EMAIL, $email);
  $response = $xml->send(SITE_NEWSLETTERADDRESS);
  if ($response == null || $response->resultCode != ERR_SUCCESS) {
    return false;
  }  
  return true;
}

function unRegisterNewsletter($code, $name, $email) {
  $xml = new XMLClient(XMLID_NEWSLETTER);
  $xml->writeStr(TAG_ACTION, 'unRegister');
  $xml->writeStr(TAG_GROUP, $code);
  $xml->writeStr(TAG_NAME, $name);
  $xml->writeStr(TAG_EMAIL, $email);
  $response = $xml->send(SITE_NEWSLETTERADDRESS);
  if ($response == null || $response->resultCode != ERR_SUCCESS) {
    return false;
  }  
  return true;
}
 
function postField($name, $size, $private = false) {
  $res = '<input type="text" name="' . $name . '" id="' . $name . '" size="' . $size . '"';
  if ($_POST[$name] != null) {
    $res .= ' value="' . $_POST[$name] . '"';
  }
  
  if ($private) {
    $res .= ' autocomplete="off"';
  }
  
  return $res . '>';
}

$postError = $_GET['e'];
$registered  = '';
$unRegistered = '';
$alreadyRegistered = '';

if ($_POST['action'] == 'process') {
  $completed = false;
  $postError = '';
  
  $name  = $_POST['name'];
  $email = $_POST['email'];
  $cemail = $_POST['cemail'];

  if ($name == '') {
    $postError = 'Name is a required field';
  } elseif (!validEmailAddress($email)) {
    $postError = 'Your email address is invalid';
  } elseif ($email != $cemail) {
    $postError = 'Your email address is invalid';
  }  

  if ($postError == '') {
    foreach ($newsletters as $newsletter) {
      if (isset($_POST[$newsletter['ID']]) == 'Y') {
        if (registerNewsletter($newsletter['ID'], $name, $email) == true) {
          $registered .= $newsletter['NAME'] . ',';
        } else {
          $alreadyRegistered .= $newsletter['NAME'] . ',';
        }
      } else {
        unRegisterNewsletter($newsletter['ID'], $name, $email);
        $unRegistered .= $newsletter['NAME'] . ',';
      }
    }
    
    $registered = trim($registered, ',');
    $unRegistered = trim($unRegistered, ',');
    $alreadyRegistered = trim($alreadyRegistered, ',');
    $completed = true;
  }
}  

include siteFile('siteheader.php');
if ($completed == false) {
  include siteFile('siteNewsRegister.php');
} else {  
  include siteFile('siteNewsCompleted.php');
}
include siteFile('sitefooter.php');
?>
