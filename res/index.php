<?php
define('__INDEX__', true);

require_once ("./inc/configuration.php");
require_once ("./inc/sessions.php");
require_once ("./inc/functions.php");
                   
$olct_session = new olct_session;

include siteFile('siteerror.php');

setlocale(LC_TIME, SITE_LOCATIONCODE);
include (CONFIG_PATH . '/language/' . SITE_LANGUAGECODE . '.php');

if (!isset($_SESSION['siteCode'])) {
  die ('divert to the general on-line cine tickets page');
}

$pageName = strval($_GET['p']);
$error = strval($_GET['e']);

if ($pageName != 'process' && isset($_SESSION['cardData'])) {
  unset($_SESSION['cardData']);
  session_unregister('cardData');
}

if ($pageName == 'process') {
  if (!isset($_SESSION['cardData'])) {
    $pageName = 'error';
    $error = LANG_NONAVIGATE;
  } else {
    include '.\process.php';
    exit;
  }
}
?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title><?=SITE_METATITLE?></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<meta content='text/html; charset=utf-8' http-equiv='Content-Type'/>
<meta name="keywords" content="<?=CONFIG_METAKEYS . ',' . SITE_METAKEYS?>" />
<meta name="description" content="<?=SITE_METADESC?>" />
<meta name="author" content="<?=CONFIG_METAAUTHOR?>" />

<?
  if (function_exists('htmlHead')) {
    htmlHead();
  } else {
    ?>
<link rel="stylesheet" href="/sites/<?=SITE_CIRCUITCODE?>/stylesheet.css" type="text/css" media="all" />
    <?
  }     
?>

</head>
<?

switch ($pageName) {
  case 'details' : include './eventdetails.php';
                   break;
  case 'success' : include './success.php';
                   break;
  case 'error'   : siteError($error);
                   break;
  case 'seats'   : include './seats.php';
                   break;
  case 'tickets' : include './selectTickets.php';
                   break;
  case 'payment' : include './payment.php';
                   break;
  case 'newsreg' : include './newsRegister.php';
                   break;
  default        : include './eventtimes.php';
                   break;
}
?>

</html>