<?php
require_once (".\\inc\\configuration.php");
require_once (".\\inc\\sessions.php");
require_once (".\\inc\\functions.php");

if (!defined('__INDEX__')) {
  showError(LANG_NONAVIGATE);
}  
 
require_once(".\\inc\\objects.php");
require_once(".\\inc\\salesOrder.php");
require_once(".\\inc\\cardFunctions.php");

function postField($name, $size, $private = false) {
  $res = '<input type="text" name="' . $name . '" id="' . $name . '" size="' . $size . '"';
  if ($_POST[$name] != null) {
    $res .= ' value="' . $_POST[$name] . '"';
  }
  
  if ($private) {
    $res .= ' autocomplete="off"';
  }
  
  return $res . '>';
}

function addPaymentSection($w)
{
  $v = $_POST['cardType'] == null ? '0' : strval($_POST['cardType']);
  $display = $v == CT_MAESTRO ? "table-row" : "none";

  $r = chr(13) . chr(13) . '<script language="javascript">' . chr(13) .
       'function showStart(dd) {' . chr(13) .
       '  var style = document.getElementById("sd").style;'  . chr(13) .
       '  if(navigator.appName.indexOf("Microsoft") > -1) { ' . chr(13) .
       '    var canSee = "block"; ' . chr(13) .
       '  } else {' . chr(13) .
       '    var canSee = "table-row"; ' . chr(13) .
       '  } ' . chr(13) .
       '  if (dd.value == "' . CT_MAESTRO . '") {' . chr(13) .
       '    style.display = canSee;'  . chr(13) .
       '  } else {'  . chr(13) .
       '    style.display = "none";'  . chr(13) .
       '  } ' . chr(13) .
       '}'  . chr(13) .
       '</script>'  . chr(13) . chr(13) ;
  
  $r .= '<form name="paymentDetails" action="' . makeURL('payment', '', true) . '" method="post">';
  $r .= '<table border="0" cellpadding="0" cellspacing="3" width="100%">';
  $r .= '<tr><td width="'. $w . '">' . LANG_CARDTYPE . ':</td><td>' . cardTypeSelect() . '</td></tr>';
  $r .= '<tr><td width="'. $w . '">' . LANG_CARDNUMBER . ':</td><td>' . postField('cardNumber', 35, true) . '</td></tr>';
  $r .= '<tr><td width="'. $w . '">' . LANG_EXPIRYDATE . ':</td><td>' . monthSelect('expMonth') . '&nbsp;&nbsp;' . yearSelect('expYear', false) . '</td></tr>';
  
  $r .= '<tr name="sd" id="sd" style="display: ' . $display . '"><td width="'. $w . '">' . LANG_STARTDATE . ':</td><td>' . monthSelect('startMonth') . '&nbsp;&nbsp;' . yearSelect('startYear', true) .  
        '&nbsp;&nbsp;' . LANG_ORISSUE . ':&nbsp;' . postField('issue', 2) . '</td></tr>';

  $r .= '<tr><td width="'. $w . '">' . LANG_CVVCODE . ':</td><td>' . postField('cvv', 5, true) . '</td></tr>';
  $r .= '<tr><td width="'. $w . '">' . LANG_CARDNAME . ':</td><td>' . postField('nameoncard', 35) . '</td></tr>';
  $r .= '<tr><td width="'. $w . '">' . LANG_EMAIL . ':</td><td>' . postField('email', 35) . '</td></tr>';
  
  if (function_exists('paymentNote')) {
    $r .= paymentNote($w);
  }  
  
  $r .= '<tr><td colspan="2"><input type="hidden" name="action" value="process"></td></tr>';
  $r .= '<tr><td colspan="2" align="right"><input type="submit" value="' . LANG_PAYNOW . '" /></td></tr>';
  $r .= '</table></form>';
  $r .= '<br/><img src="/images/rapidssl_ssl_certificate.gif" border="0">';
  return $r;
}    


//MAIN--------------------
$performance = $olct_session->getPerformanceEvent($_SESSION[TAG_PERFORMANCECODE]);
if ($performance == null) 
{
  showError(LANG_PERFEXPIRED);
}
$event = $performance->event;

$order = new olct_salesOrder();

if ($order->checkAllReservations() == false) {
  $order->clear();
  showError(LANG_TIMEOUT);
}

$orderSummary = $order->summariseOrder();
$postError = $_GET['e'];

if ($_POST['action'] == 'process') {
  $cardData = array();

  $cardData['cardType']     = intval($_POST['cardType']);
  $cardData['cardNumber']   = str_replace(' ', '', $_POST['cardNumber']);
  $cardData['cvv']          = trim(strval($_POST['cvv']));
  $cardData['nameOnCard']   = trim(strval($_POST['nameoncard']));
  $cardData['emailAddress'] = trim($_POST['email']);
  $cardData['note']         = trim(strval($_POST['note']));
  $cardData['expMonth']     = intval($_POST['expMonth']);
  $cardData['expYear']      = intval($_POST['expYear']);
  $cardData['startMonth']   = intval($_POST['startMonth']);
  $cardData['startYear']    = intval($_POST['startYear']);
  $cardData['issue']        = intval($_POST['issue']);
  $cardData['postError']    = '';

  $giftOK = true;
  $loyaltyOK = true;
  
  //ProcessLoyalty Cards
  $loyaltyNumber = trim($_POST['loyaltyCardNumber']);
  $cardData['loyaltyCardNumber'] = '';
  if (SITE_LOYALTY && $loyaltyNumber != '') {
    $loyaltyOK = validateLoyaltyCard($loyaltyNumber, $cardData);
    if ($loyaltyOK == true) {
      $cardData['loyaltyCardNumber'] = $loyaltyNumber;
    }  
  }

  //Process Gift Cards
  $giftCardNumber = trim($_POST['giftCardNumber']);
  $cardData['giftCardNumber'] = '';
  $cardData['giftCardBalance'] = 0;
  if ($loyaltyOK && SITE_GIFTCARDS && $giftCardNumber != '') {
    $giftOK = loadGiftBalance($giftCardNumber, $cardData);
    if ($giftOK == true) {
      $cardData['giftCardNumber'] = $giftCardNumber;
    }  
  }
  
  //Check the card, and process if all ok
  if ($loyaltyOK && $giftOK && checkCreditCard($cardData)) {
    $bal = $order->orderTotal;
    if ($cardData['giftCardBalance'] >= $bal) {
      $cardData['giftCardValue'] = $order->orderTotal;
    } else {
      $cardData['giftCardValue'] = $cardData['giftCardBalance'];
    }

    $cardData['value'] = $order->orderTotal - $cardData['giftCardValue'];

    if ($cardData['nameOnCard'] == '') {
      $cardData['postError'] = LANG_NAMEREQUIRED;
    } elseif (!validEmailAddress($cardData['emailAddress'])) {
      $cardData['postError'] = LANG_INVALIDEMAIL;
    } elseif ($cardData['note'] == '' && SITE_ORDERNOTEREQUIRED == true) {
      $cardData['postError'] = SITE_ORDERNOTEERROR;
    }  
    
    if ($cardData['postError'] == '') {
      $cardData['authStatus'] = 'init';
      saveSessionCardData($cardData);      
      redirect('process', '', true);
    };
  }
  $postError = $cardData['postError'];
}  

  include siteFile('siteheader.php');
  include siteFile('sitePayment.php');
  include siteFile('sitefooter.php');
?>
