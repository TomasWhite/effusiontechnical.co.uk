<?php
define('__INDEX__', true);

require_once (".\\inc\\configuration.php");
require_once (".\\inc\\sessions.php");
require_once (".\\inc\\functions.php");
require_once('.\\inc\\phpmailer\\class.phpmailer.php' );

    $mail = new mosPHPMailer(); 
    $mail->Host     = CONFIG_EMAILHOST;
    $mail->Mailer   = "smtp";    
    $mail->SMTPAuth = true; // turn on SMTP authentication
    $mail->Username = CONFIG_EMAILUSER; // SMTP username
    $mail->Password = CONFIG_EMAILPASS; // SMTP password
    $mail->From     = 'noreply@showtimecinemas.ie';
    $mail->FromName = 'Show TIme';
    $mail->Subject  = 'Online Booking Confirmation';
    $mail->Body     = 'Test mail from PHP Mailer';
    $mail->AddAddress('annie.morton@dgrka.com', 'Annie');
    if($mail->Send()) {
      $mailsuccess = true;
    } else {
      $mailsuccess = false;
    }  
?>