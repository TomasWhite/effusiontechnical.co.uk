<?php
require_once (".\\inc\\configuration.php");
require_once (".\\inc\\sessions.php");
require_once (".\\inc\\functions.php");
require (".\\inc\\objects.php");
require (".\\inc\\salesOrder.php");

if (!defined('__INDEX__')) {
  showError(LANG_NONAVIGATE);
}  

$performanceCode = $_GET['perfCode'];
if ($performanceCode == null) {
  $performanceCode = $_SESSION[TAG_PERFORMANCECODE];
}

$performance = $olct_session->getPerformanceEvent($performanceCode);

if ($performance == null) {
  showError(LANG_PERFOFFSALE);
}
$_SESSION[TAG_PERFORMANCECODE] = $performance->code;
$event = $performance->event;


$order = new olct_salesOrder();
if ($order->checkAllReservations() == false) {
  $order->clear();
  showError(LANG_TIMEOUT);
}

$perfNode = $order->perfNodefor($performance->code);
if ($perfNode == null) {
  $order->clear();
  showError(LANG_ORDERERROR);
}

$seatNode = $perfNode->findNode(TAG_SEATS);
if ($seatNode == null) {
  $order->clear();
  showError(LANG_SEATERROR);
}

$seatNode = $seatNode->getFirst(TAG_SEAT);
if ($seatNode == null) {
  $order->clear();
  showError(LANG_SEATITEMERROR);
}

$seatCode = $seatNode->getStrAttr(TAG_SEATCODE);
$seatCount = $seatNode->readInt('seatCount');

$numRows = 0;
$numCols = 0;
$error = '';

$mySeat = $seatNode->readInt('seatID');

if (isset($_GET['si'])) {
  $newSeat = $_GET['si'];
  if (!$order->swapSeats($perfNode, $mySeat, $newSeat, $seatCount, $seatCode)) {
    $error = LANG_SEATSTAKEN;
  } else {
    $seatNode = $perfNode->findNode(TAG_SEATS);
    $seatNode = $seatNode->getFirst(TAG_SEAT);
    $mySeat = $seatNode->readInt('seatID');
  }  
}

$mySeats = array();
for ($i=0; $i<$seatCount; $i++) {
  $mySeats[$mySeat + $i] = TRUE;
}

$plan = new olct_seatplan($performance->code, $performance->planCode);
if ($plan->error == true) {
  showError(LANG_INVALIDPLAN);
}
$data = $plan->getPlanString($mySeats, $rowLabels);

?>
<style>
.rl{width:16px;height:16px;padding:1px;font-size:10px;font-weight:bold;text-align:center;}
.as{width:16px;height:16px;padding:1px;background:#BE352D;border:1px solid black;font-size:8px;text-align:center; cursor:pointer}
.uw{width:16px;height:18px;padding:1px;background-image:url('/images/wheelchair.gif');border:1px solid black;font-size:8px;text-align:center;}
.us{width:16px;height:16px;padding:1px;background:#BEBEBE;border:1px solid black;font-size:8px;text-align:center;}
.my{width:16px;height:16px;padding:1px;background:#FFF717;border:1px solid black;font-size:8px;text-align:center;}
.ov{width:16px;height:16px;padding:1px;background:#FFB2B1;border:1px solid black;font-size:8px;text-align:center; cursor:pointer}
</style>

<script language="javascript">
var RequestSeats=<?=$seatCount?>;
var rowLabels = <?=$rowLabels?>;
var columnData = <?=$data?>;

function sover(sid)
{
  for (i=0;i<RequestSeats;i++)
  {
    seatID = sid + i;
    Seat = document.all['s' + seatID];
    if (Seat == undefined)
      Seat = document.all['m' + seatID];

    Seat.className = 'ov';
  }
}

function sout(sid)
{
  for (i=0;i<RequestSeats;i++)
  {
    seatID = sid + i;
    Seat = document.all['s' + seatID];
    if (Seat == undefined)
    {
      Seat = document.all['m' + seatID];
      Seat.className = 'my';
    }
    else
      Seat.className = 'as';
  }
}

function selectSeats(sid)
{
  document.location = 'http://<?=HOST_NAME?>?s=<?=SITE_CODE?>&p=seats&si=' + sid;
}

function writePlan()
{
  var txt;
  var rowLabel;
  var seatLabel;

  txt = '<table cellspacing=\"1\" cellpadding=\"0\" border=\"0\" style=\"border-spacing: 5px\">';
  first = true;

  for (rows=0; rows<rowLabels.length; rows++)
  {
    rowLabel = rowLabels[rows];
    txt = txt + '<tr><td class="rl">' + rowLabel + '</td>';
    for (cols=0; cols<columnData[rows].length; cols++)
    {
      if (columnData[rows][cols][0] == -1)
      {
        txt = txt + '<td>&nbsp;</td>';
      }
      else
      {
        colLabel = columnData[rows][cols][1];
        cstatus = columnData[rows][cols][2];
        selectSeat = columnData[rows][cols][3];
        if (cstatus == 'X')
          txt = txt + '<td class="us" name=\"' + seatLabel + '\">' + colLabel  + '</td>';
        else if (cstatus == 'W')
          txt = txt + '<td class="uw" name=\"' + seatLabel + '\">&nbsp;</td>';
        else if (cstatus == 'M')
        {
          txt = txt + '<td class="my" style="cursor:pointer" onclick="selectSeats(' + selectSeat + ')" onmouseover="sover(' + selectSeat + ')" onmouseout="sout(' + selectSeat + ')" id=\"m' + columnData[rows][cols][0] + '\">' + colLabel  + '</td>';
        }
        else
          txt = txt + '<td class="as" onclick="selectSeats(' + selectSeat + ')" onmouseover="sover(' + selectSeat + ')" onmouseout="sout(' + selectSeat + ')" id=\"s' + columnData[rows][cols][0] + '\">' + colLabel  + '</td>';
      }
    }

    txt = txt + '<td class="rl">' + rowLabels[rows] + '</td></tr>';
  }
  txt = txt + '</table>';
  document.write(txt);
}
</script>
<?

  include siteFile('siteheader.php');
  include siteFile('siteSeats.php');
  include siteFile('sitefooter.php');
?>
