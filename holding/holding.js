// JavaScript Document
var imageList=new Array()
var altList=new Array()
var currentImage = 0;
var imageCount = 0;
var fadeTime = 1000;

var ChainCollector = function(base) {
    var CLASS = arguments.callee;

    this.then = this.and = this;
    var queue = [], baseObject = base || {};

    this.____ = function(method, args) {
        queue.push({func: method, args: args});
    };

    this.fire = function(base) {
        var object = base || baseObject, method, property;
        for (var i = 0, n = queue.length; i < n; i++) {
            method = queue[i];
            if (object instanceof CLASS) {
                object.____(method.func, method.args);
                continue;
            }
            property = object[method.func];
            object = (typeof property == 'function')
                    ? property.apply(object, method.args)
                    : property;
        }
        return object;
    };
};

ChainCollector.addMethods = function(object) {
    var methods = [], property, i, n, name;
    var self = this.prototype;

    var reservedNames = [], blank = new this();
    for (property in blank) reservedNames.push(property);
    var re = new RegExp('^(?:' + reservedNames.join('|') + ')$');

    for (property in object) {
        if (Number(property) != property)
            methods.push(property);
    }
    if (object instanceof Array) {
        for (i = 0, n = object.length; i < n; i++) {
            if (typeof object[i] == 'string')
                methods.push(object[i]);
        }
    }
    for (i = 0, n = methods.length ; i < n; i++)
        (function(name) {
            if (re.test(name)) return;
            self[name] = function() {
                this.____(name, arguments);
                return this;
            };
        })(methods[i]);

    if (object.prototype)
        this.addMethods(object.prototype);
};

jQuery.fn.wait = function(time) {
  var collector = new ChainCollector(), self = this;
  // Deal with scoping issues...
  var fire = function() { collector.fire(self); };
  setTimeout(fire, Number(time) * 1000);
  return collector;
};

// Then extend ChainCollector with all jQuery's methods
ChainCollector.addMethods(jQuery);

$(document).ready(function () {
	imageRatio = 1280/853;

	$('#image img#img0').attr('src', "/holding/images/Effusion_holding_page.jpg");
	
	 if ($.browser.msie) {
       v = $.browser.version * 1;
       if(v >=6 && v < 7 ) {
	   	
		//viewportwidth = document.getElementsByTagName('body')[0].clientWidth;
       	//viewportheight = document.getElementsByTagName('body')[0].clientHeight;

		viewportwidth = document.getElementsByTagName('html')[0].clientWidth;
       	viewportheight = document.getElementsByTagName('html')[0].clientHeight;
		
		$('body, #image, #box-outer').addClass('special');
		
		
		//min-width: 700px;
		//min-height: 600px;	
	
		if( (viewportwidth*1) > 700)  {
			$('body, #image, #box-outer').addClass('special-2');
			//alert('with need fixing '+(viewportwidth*1)+'|'+(viewportheight*1) );
			$('body, #image, #box-outer').width(viewportwidth);
			$('body, #image, #box-outer').height(viewportheight);
			doSomething();
		} else if( (viewportheight*1) > 600)  {
			$('body, #image, #box-outer').addClass('special-2');
			//alert('with need fixing '+(viewportwidth*1)+'|'+(viewportheight*1) );
			$('body, #image, #box-outer').width(viewportwidth);
			$('body, #image, #box-outer').height(viewportheight);
			doSomething();
		}

		
	   }
	 }
	 
	   $(window).bind('resize', function() {
		  if (resizeTimer) clearTimeout(resizeTimer);
		  resizeTimer = setTimeout(doSomething, 100);
      });


	doSomething();
	
});

 var resizeTimer = null;
 var imageRatio = null;

$(window).load(function () {
						 
	$('#image img#img0').attr('src', "/holding/images/Effusion_holding_page.jpg");
	$('#image img#img0').wait(3).then.fadeIn(10, function(){
		runSlides();																		   
	});

	
});

      function doSomething() {
	  		windowHeight = $(window).height();    
	  		windowWidth = $(window).width();    
			
			if( (windowHeight * imageRatio) > windowWidth ){
				//alert('height ' + windowWidth + ' | ' + windowHeight);
				$('body').addClass('size-by-height');	
			} else {
				//alert('width ' + windowWidth + ' | ' + windowHeight);
				$('body').removeClass('size-by-height');	
			}
      };



function runSlides(){
	newimagesrt = '?2';
	//alert('runSlides() 1');
	imageList[0]="/holding/images/Effusion_holding_page.jpg"+newimagesrt; 
	//imageList[1]="/holding/images/Effusion_holding_page5.jpg"+newimagesrt; 
	//imageList[2]="/holding/images/Effusion_holding_page6.jpg"+newimagesrt;
	//imageList[3]="/holding/images/Effusion_holding_page7.jpg"+newimagesrt;
	//imageList[4]="/holding/images/Effusion_holding_page11.jpg"+newimagesrt;
	//imageList[3]="/holding/images/Effusion_holding_page8.jpg";
	//imageList[4]="/holding/images/Effusion_holding_page9.jpg";
	//imageList[5]="/holding/images/Effusion_holding_page10.jpg";
	//imageList[6]="/holding/images/Effusion_holding_page11.jpg";
	//imageList[7]="/holding/images/Effusion_holding_page12.jpg";


	altList[0]="Sugar Loaf Walk"; 
	altList[1]="Millennium Bridge"; 
	altList[2]="Bikes";
	altList[3]="Two doors, Red and Blue";
	altList[4]="alt 4";
	altList[5]="alt 5";
	altList[6]="alt 6";
	altList[7]="alt 7";

	currentImage = 0;
	imageCount = imageList.length;	
	
	startImage = "/holding/images/Effusion_holding_page.jpg";
	$('#image img#img0').attr('src', startImage);

	if ( jQuery.preLoadImages(startImage) ){
		//$('#image img#img0').fadeIn(fadeTime ,function(){
			imageList.shift();
			altList.shift();
			fadeImage(imageList, currentImage, altList);
		//} );
	}	
}

(function($) {
  var cache = [];
  // Arguments are image paths relative to the current page.
  $.preLoadImages = function() {
    var args_len = arguments.length;
    for (var i = args_len; i--;) {
      var cacheImage = document.createElement('img');
      cacheImage.src = arguments[i];
      cache.push(cacheImage);
    }
	return true;
  }
})(jQuery)



function fadeImage(arrayImages, imageNumber, arrayAlt){
	if(currentImage != imageNumber){
		alert('WHY'+currentImage+'|'+imageNumber);
		//return false;	
	}
	var anImage = '';
	if (arrayImages.length > 0 ) {
		//alert(image);
		
		anImage = arrayImages[0];
		altText = arrayAlt[0];
		arrayImages.shift();
		arrayAlt.shift();
		
		if(anImage){
			if ( jQuery.preLoadImages(anImage) ){

				$('#image #imgbox0').after('<div class="imgbox image-next" id="imgbox'+(imageNumber+1)+'" ><img src="'+anImage+'" onclick="" alt="'+altText+'" class="image-next" id="img'+(imageNumber+1)+'"></div>');

					$('#image img#img'+imageNumber).wait(1).then.animate({opacity: 0},fadeTime, function(){
						//alert('imageNumber faded '+imageNumber); 
						$('#image #imgbox'+imageNumber).addClass('current-hidden');																															
						$('#image #imgbox'+imageNumber).removeClass('image-current');

						$('#image img#img'+imageNumber).addClass('current-hidden');																															
						$('#image img#img'+imageNumber).removeClass('image-current');

						$('#image #imgbox'+(imageNumber+1)).addClass('image-current');																															
						$('#image #imgbox'+(imageNumber+1)).removeClass('image-next');																															
								
						$('#image img#img'+(imageNumber+1)).addClass('image-current');																															
						$('#image img#img'+(imageNumber+1)).removeClass('image-next');																															
								
						//$('#image img').removeClass('current-hidden');	
						$('#image img.current-hidden').animate({opacity: 1},fadeTime, function(){
							$('#image .imgbox.current-hidden').removeClass('current-hidden');
							$('#image #imgbox'+imageNumber).addClass('image-used');	

							$('#image img.current-hidden').removeClass('current-hidden');
							$('#image img#img'+imageNumber).addClass('image-used');	


							nextArray = arrayImages;
							nextAlt = arrayAlt;
							currentImage = imageNumber + 1;
							fadeImage(nextArray, imageNumber + 1, nextAlt);								   
						});																															
						
					});
				//});
			}
		}
	} else {
		//alert('done');	
		continuefadeing();
	}
	return true
}

function continuefadeing(){
	if ( currentImage >= imageCount ){
		currentImage = 0;
	}
	imageNumber = currentImage;
	nextImageNumber = imageNumber + 1;
	if ( nextImageNumber >= imageCount ){
		nextImageNumber = 0;
	}
	//alert(currentImage+'|'+nextImageNumber)
	if(nextImageNumber == currentImage){
		return false;	
	}
	$('#image #imgbox'+nextImageNumber).removeClass('image-used');																															
	$('#image #imgbox'+nextImageNumber).addClass('image-next');																															

	$('#image img#img'+nextImageNumber).removeClass('image-used');																															
	$('#image img#img'+nextImageNumber).addClass('image-next');																															
	
	$('#image img#img'+imageNumber).animate({opacity: 0},fadeTime, function(){
		$('#image #imgbox'+imageNumber).addClass('current-hidden');																															
		$('#image #imgbox'+imageNumber).removeClass('image-current');

		$('#image img#img'+imageNumber).addClass('current-hidden');																															
		$('#image img#img'+imageNumber).removeClass('image-current');

		$('#image #imgbox'+nextImageNumber).addClass('image-current');																															
		$('#image #imgbox'+nextImageNumber).removeClass('image-next');																															

		$('#image img#img'+nextImageNumber).addClass('image-current');																															
		$('#image img#img'+nextImageNumber).removeClass('image-next');																															
								
		$('#image img.current-hidden').animate({opacity: 1},fadeTime, function(){
			$('#image .current-hidden').removeClass('current-hidden');
			$('#image #imgbox'+imageNumber).addClass('image-used');	
			$('#image img#img'+imageNumber).addClass('image-used');	
			currentImage = imageNumber + 1;
			continuefadeing();								   
		});																															
	});
}

