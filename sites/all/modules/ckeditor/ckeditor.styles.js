/*
$Id: ckeditor.styles.js,v 1.1.2.2 2009/12/16 11:32:04 wwalc Exp $
Copyright (c) 2003-2009, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

/*
 * This file is used/requested by the 'Styles' button.
 * 'Styles' button is not enabled by default in DrupalFull and DrupalFiltered toolbars.
 */

CKEDITOR.addStylesSet( 'drupal',
[
	{ name : 'Sifr header', element : 'p', attributes : { class : 'univers14' } },
	{ name : 'Paragraph', element : 'p', attributes : { class : 'p-incontent' } },
	{ name : 'Heading A', element : 'h3', attributes : { class : 'h3-incontent' } },
	{ name : 'Heading B', element : 'h4', attributes : { class : 'h4-incontent' } },
	{ name : 'Heading C', element : 'h5', attributes : { class : 'h5-incontent' } },
	{ name : 'Highlight', element : 'p', attributes : { class : 'highlight' } },
	{ name : 'Pull Quote', element : 'p', attributes : { class : 'pullquote' } },

  { name : 'Body Intro', element : 'p', attributes : { class : 'bodyintro' } },

  { name : 'Body Title', element : 'span', attributes : { class : 'bodytitle' } },

	{ name : 'Image 90px', element : 'img', attributes : { class : 'imgsmall' } },
	{ name : 'Image 200px', element : 'img', attributes : { class : 'imgmedium' } },
	{ name : 'Image 630px', element : 'img', attributes : { class : 'imglarge' } },

	{ name : 'Image Caption', element : 'span', attributes : { class : 'imgcaption' } },
	{ name : 'Image Centre', element : 'span', attributes : { class : 'imgcentre' } },
	{ name : 'Image Left', element : 'span', attributes : { class : 'imgleft' } },
	{ name : 'Image Right', element : 'span', attributes : { class : 'imgright' } }


]);
