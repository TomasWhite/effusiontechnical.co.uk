<?php
// $Id: template.php,v 1.44.2.11 2009/11/05 13:11:38 johnalbin Exp $

/**
 * @file
 * Contains theme override functions and preprocess functions for the Zen theme.
 *
 * IMPORTANT WARNING: DO NOT MODIFY THIS FILE.
 *
 * The base Zen theme is designed to be easily extended by its sub-themes. You
 * shouldn't modify this or any of the CSS or PHP files in the root zen/ folder.
 * See the online documentation for more information:
 *   http://drupal.org/node/193318
 */

// Auto-rebuild the theme registry during theme development.
if (theme_get_setting('zen_rebuild_registry')) {
  drupal_rebuild_theme_registry();
}

/*
 * Add stylesheets only needed when Zen is the active theme. Don't do something
 * this dumb in your sub-theme; see how wireframes.css is handled instead.
 */
if ($GLOBALS['theme'] == 'zen') { // If we're in the main theme
  if (theme_get_setting('zen_layout') == 'border-politics-fixed') {
    drupal_add_css(_zen_path() . '/layout-fixed.css', 'theme', 'all');
  }
  else {
    drupal_add_css(_zen_path() . '/layout-liquid.css', 'theme', 'all');
  }
}

/**
 * Implements HOOK_theme().
 */
function zen_theme(&$existing, $type, $theme, $path) {
  include_once './' . _zen_path() . '/template.theme-registry.inc';
  return _zen_theme($existing, $type, $theme, $path);
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return
 *   A string containing the breadcrumb output.
 */
function zen_breadcrumb($breadcrumb) {
  // Determine if we are to display the breadcrumb.
  $show_breadcrumb = theme_get_setting('zen_breadcrumb');
  if ($show_breadcrumb == 'yes' || $show_breadcrumb == 'admin' && arg(0) == 'admin') {

    // Optionally get rid of the homepage link.
    $show_breadcrumb_home = theme_get_setting('zen_breadcrumb_home');
    if (!$show_breadcrumb_home) {
      array_shift($breadcrumb);
    }

    // Return the breadcrumb with separators.
    if (!empty($breadcrumb)) {
      $breadcrumb_separator = theme_get_setting('zen_breadcrumb_separator');
      $trailing_separator = $title = '';
      if (theme_get_setting('zen_breadcrumb_title')) {
        if ($title = drupal_get_title()) {
          $trailing_separator = $breadcrumb_separator;
        }
      }
      elseif (theme_get_setting('zen_breadcrumb_trailing')) {
        $trailing_separator = $breadcrumb_separator;
      }
      return '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . "$trailing_separator$title</div>";
    }
  }
  // Otherwise, return an empty string.
  return '';
}



function zen_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' ' . $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
	$testlink = trim(strip_tags($link));
	$testlink = strtolower($testlink);
	$testlink = zen_id_safe($testlink);
	
	
  return '<li id="menu-'.$testlink.'" class="whu ' . $class . '">' . $link . $menu . "</li>\n";
}



/**
 * Implements theme_menu_item_link()
 */
function zen_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }

  // If an item is a LOCAL TASK, render it as a tab
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">' . check_plain($link['title']) . '</span>';
    $link['localized_options']['html'] = TRUE;
  }
	
	//print '<h1>'.$link['type'].'</h1>';
	//$link['localized_options']['attributes']['type'] = $link['type'];
	
	if( ($link['type'] == 4) ){
		global $user;
		//if($user->uid){
		//	print_r( $link );
		//}
		$testlink = trim(strip_tags($link['title']));
		$testlink = strtolower($testlink);
		$testlink = zen_id_safe($testlink);
		$link['localized_options']['attributes']['id'] = 'm-l-'.$testlink;
		return '<span id="m-box-'.$testlink.'" class="menu-box active-'.$link['in_active_trail'].' haschildren-'.$link['has_children'].'">'.l($link['title'], $link['href'], $link['localized_options']).'</span>';
	} else {
		return l($link['title'], $link['href'], $link['localized_options']);	
	}
	

  
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function zen_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= '<ul class="tabs primary clear-block">' . $primary . '</ul>';
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clear-block">' . $secondary . '</ul>';
  }

  return $output;
}


/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function zen_preprocess_page(&$vars, $hook) {

  // exclude backend pages to avoid core js not working anymore
  // you could also just use a backend theme to avoid this

	if (arg(0) != 'admin' || !(arg(1) == 'add' && arg(2) == 'edit') || arg(0) != 'panels' || arg(0) != 'ctools') {
    $scripts = drupal_add_js();

    $new_jquery = array('sites/default/themes/zen/jquery-1.7.2.min.js' => $scripts['core']['misc/jquery.js']);
    $scripts['core'] = array_merge($new_jquery, $scripts['core']);
    unset($scripts['core']['misc/jquery.js']);

    $vars['scripts'] = drupal_get_js('header', $scripts);
  }


  // Add conditional stylesheets.
  if (!module_exists('conditional_styles')) {
    $vars['styles'] .= $vars['conditional_styles'] = variable_get('conditional_styles_' . $GLOBALS['theme'], '');
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $classes = explode(' ', $vars['body_classes']);
  // Remove the mostly useless page-ARG0 class.
  if ($index = array_search(preg_replace('![^abcdefghijklmnopqrstuvwxyz0-9-_]+!s', '', 'page-'. drupal_strtolower(arg(0))), $classes)) {
    unset($classes[$index]);
  }
  if (!$vars['is_front']) {
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    $classes[] = zen_id_safe('page-' . $path);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        $section = 'node-add';
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        $section = 'node-' . arg(2);
      }
    }
    $classes[] = zen_id_safe('section-' . $section);
  }
  if (theme_get_setting('zen_wireframes')) {
    $classes[] = 'with-wireframes'; // Optionally add the wireframes style.
  }
  $vars['body_classes_array'] = $classes;
  $vars['body_classes'] = implode(' ', $classes); // Concatenate with spaces.

  /**
   * Slove 30 CSS files limit in Internet Explorer
   */
  $preprocess_css = variable_get('preprocess_css', 0);
  if (!$preprocess_css) {
    $styles = '';
    foreach ($vars['css'] as $media => $types) {
      $import = '';
      $counter = 0;
      foreach ($types as $files) {
        foreach ($files as $css => $preprocess) {
          $import .= '@import "'. base_path() . $css .'";'."\n";
          $counter++;
          if ($counter == 15) {
            $styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
            $import = '';
            $counter = 0;
          }
        }
      }
      if ($import) {
        $styles .= "\n".'<style type="text/css" media="'. $media .'">'."\n". $import .'</style>';
      }
    }
    if ($styles) {
      $vars['styles'] = $styles;
    }
  }

}

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
function zen_preprocess_maintenance_page(&$vars, $hook) {
  // Add conditional stylesheets.
  if (!module_exists('conditional_styles')) {
    $vars['styles'] .= $vars['conditional_styles'] = variable_get('conditional_styles_' . $GLOBALS['theme'], '');
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $vars['body_classes_array'] = explode(' ', $vars['body_classes']);
}

/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */
function zen_preprocess_node(&$vars, $hook) {
  // Special classes for nodes
  $classes = array('node');
  if ($vars['sticky']) {
    $classes[] = 'sticky';
  }
  if (!$vars['status']) {
    $classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['uid'] && $vars['uid'] == $GLOBALS['user']->uid) {
    $classes[] = 'node-mine'; // Node is authored by current user.
  }
  if ($vars['teaser']) {
    $classes[] = 'node-teaser'; // Node is displayed as teaser.
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $classes[] = zen_id_safe('node-type-' . $vars['type']);
  $vars['classes'] = implode(' ', $classes); // Concatenate with spaces
}

/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
function zen_preprocess_comment(&$vars, $hook) {
  include_once './' . _zen_path() . '/template.comment.inc';
  _zen_preprocess_comment($vars, $hook);
}

/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
function zen_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  // Special classes for blocks.
  $classes = array('block');
  $classes[] = 'block-' . $block->module;
  $classes[] = 'region-' . $vars['block_zebra'];
  $classes[] = $vars['zebra'];
  $classes[] = 'region-count-' . $vars['block_id'];
  $classes[] = 'count-' . $vars['id'];

  $vars['edit_links_array'] = array();
  $vars['edit_links'] = '';
  if (theme_get_setting('zen_block_editing') && user_access('administer blocks')) {
    include_once './' . _zen_path() . '/template.block-editing.inc';
    zen_preprocess_block_editing($vars, $hook);
    $classes[] = 'with-block-editing';
  }

  // Render block classes.
  $vars['classes'] = implode(' ', $classes);
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'id'.
 * - Replaces any character except alphanumeric characters with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function zen_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
 * Returns the path to the Zen theme.
 *
 * drupal_get_filename() is broken; see #341140. When that is fixed in Drupal 6,
 * replace _zen_path() with drupal_get_path('theme', 'zen').
 */
function _zen_path() {
  static $path = FALSE;
  if (!$path) {
    $matches = drupal_system_listing('zen\.info$', 'themes', 'name', 0);
    if (!empty($matches['zen']->filename)) {
      $path = dirname($matches['zen']->filename);
    }
  }
  return $path;
}

function neat_trim($str, $n, $delim='...') {
    $len = strlen($str);
    if ($len > $n) {
        preg_match('/(.{' . $n . '}.*?)\b/', $str, $matches);
        return rtrim($matches[1]) . $delim;
    }
    else {
        return $str;
    }
}

function is_single_image($node){
	
	
	if(($node->type == 'six_column_page' || $node->type == 'column_page' || $node->type == 'page' || $node->type == 'long_content' || $node->type == 'parent_page' || $node->type == 'vertical_page') && arg(2) != 'edit'){
			return true;
	} else {
			return false;
	}
	
}

function make_right_box(){
		$strReturn = "";
		$strReturn .= "<div id='right_perm_col'>";

		$strReturn .= "<div id='right_perm_col_services'>";

		$strReturn .= '<h3 class="title rhs_title"><a href="/node/724">services</a></h3>';
			
		$view = get_view('promote_options', 'block_3'); // font left
		$view->execute();
		
		$strReturn .= '<ul id="services_direct">';
		foreach($view->result as $r) {
			$n = node_load($r->nid);
			$strReturn .= '<li><a href="/node/'.$r->nid.'">'.$n->title.'&nbsp;<span class="menu_arrow">&raquo;</span></a></li>';
		}
		$strReturn .= '</ul>';
		
		$strReturn .= '<h4 id="more_services"><a href="/node/724">All services&nbsp;<span class="menu_arrow">&raquo;</span></a></h4>';
		
		$strReturn .= "</div>"; // id='right_perm_col_services'
		
		$strReturn .= '<h3 class="title rhs_title">more</h3>';

		$view = get_view('promote_options', 'block_4'); // font left
		$view->execute();
		$strReturn .= '<ul id="more">';
		foreach($view->result as $r) {
			$n = node_load($r->nid);
			$strReturn .= '<li><a href="/node/'.$r->nid.'">'.$n->title.'&nbsp;<span class="menu_arrow">&raquo;</span></a></li>';
		}
		$strReturn .= '</ul>';


		//print '<h3>Menu</h3>';
		//print theme('links', menu_navigation_links('menu-new-navagation'),array('id' => 'nav_main'));
		$sub_menu = menu_navigation_links('secondary-links');
		
		$strReturn .= "<ul id='sub_nav'>"; // ;
		foreach($sub_menu as $key => $value){
			//print_r($value);
			$strReturn .= "<li><a href='/".$value['href']."'>".$value['title']."&nbsp;<span class='menu_arrow'>&raquo;</span></a></li>";
/*			$sub_menu[$key]['attributes']['title'] = $value['attributes']['title'].'&nbsp;<span class="menu_arrow">&raquo;</span>';
			$sub_menu[$key]['title'] = $value['title'].'';*/
		}
		$strReturn .= "</ul>"; // id='sub_nav';
		
		//print_r($sub_menu);
		
		//$strReturn .= theme('links', $sub_menu,array('id' => 'nav_sub'));
		//$strReturn .= theme('menu', menu_navigation_links('secondary-links'),array('id' => 'nav_sub'));
		
		$strReturn .= '<span id="close">&copy; 2002-2012 Effusion LLP</span>';
		$strReturn .= "</div>"; // id='right_perm_col';

		print $strReturn;
}

function get_view($view_id = 'promote_options', $display_id = 'default'){
	//print "<div><h1>".$view_id."".$display_id."</h1>";		
						
	$view = views_get_view('promote_options');
						//$view->set_arguments(array($node->nid));
	$view->set_display($display_id);
						//$view->execute_display('block-2');
						//$view->pre_execute();
	return $view;					
	
	/*$view->execute();
	foreach($view->result as $r) {
		$n = node_load($r->nid);
		print '<h2>'.$n->title.'</h2>';
	}
	print "</div>";		*/
	
}

function makeGrid($node){
	global $user;
	/*if($user->uid){
		print '<pre>';
		print_r($node);
		print '</pre>';
	}*/
	if( isset($node->field_grid_title[0]['value']) ){
		$grid_title = $node->field_grid_title[0]['value'];
		$grid_title = "<h2 class='title title_grid'>".$grid_title.'</h2>';	
	}
	
	$node->field_image_grid[0]['data']['alt'];
  $node->field_image_grid[0]['data']['title'];
  $node->field_image_grid[0]['filepath']; 

	$grid_body = "";
	foreach($node->field_image_grid as $key => $value){
		$img_alt = "";
		if(isset($value['data']['alt'])){
			$img_alt = $value['data']['alt'];
		} 
		$img_title = "";
		if(isset($value['data']['title'])){
			$img_title = $value['data']['title'];
		}
		$img_path = "";
		if(isset($value['filepath'])){
			$img_path = $value['filepath'];
		}
		
		if($img_path){
			$thumb = theme('imagecache', '170', $img_path, $img_title);
			
			if($img_alt){
				$pos = strpos($img_alt, 'http://' );
				if($pos !== false){
					$thumb = '<a href="'.$img_alt.'" target="_blank">'.$thumb.'</a>';
				} else {
					$thumb = '<a href="/'.$img_alt.'" >'.$thumb.'</a>';
				}
			}

			$grid_body .= '<div class="grid_item grid_item_'.$key.' grid_item_2_'.($key%2).' grid_item_3_'.($key%3).' ">'.$thumb.'</div>';

		}
	}
	
	$return = "";
	if($grid_body){
		$return = '<div id="grid_box">';
		$return .= $grid_title;
		$return .= $grid_body;
		$return .= '</div>';
	}

	return $return;
}
