<div class="projectdetails">
<div class="col1 inlineleft width34percent">
<h2 class="univers14 bottommargin"><?php print $node->field_titlecol1[0]['value'] ?></h2>
<div class="padright"><?php print check_markup($node->content['body']['#value'], 3, false) ?></div>
</div>
<div class="col1 inlineleft width33percent">
<h2 class="univers14 bottommargin"><?php print $node->field_titlecol2[0]['value'] ?></h2>
<div class="padright"><?php print $node->field_col2[0]['value'] ?></div>
</div>
<div class="col1 inlineleft width33percent">
<h2 class="univers14 bottommargin"><?php print $node->field_titlecol3[0]['value'] ?></h2>
<div class="padright"><?php print $node->field_col3[0]['value'] ?></div>
</div>
<div class="clearboth"></div>
</div>