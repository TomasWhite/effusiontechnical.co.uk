<?php
      $view = views_get_view('childs');
      $view->set_arguments(array($node->nid));
      $view->execute_display('default');
?>
<div class="projectdetails">
  <div class="projectdescription inlineleft width50percent">
    <h2 class="univers14 cattitle h2-alwaysopen"><?php print $node->title ?></h2>
    <div style="padding-right: 50px"><div class="bottomspacer"></div><?php print strip_tags($node->body) ?></div>
    <p><a href="#" onclick="history.back(); return false;" class="bkbtn"><img src="/sites/default/themes/zen/images/backarrow.jpg" alt="" /><img src="/sites/default/themes/zen/images/backbtn.jpg" alt="" /></a></p>
  </div>
  <div id="childs" class="inlineleft width50percent">
    <h2 class="univers14 cattitle h2-alwaysopen"><?php print $node->field_childs_title[0]['value'] ?></h2>
    <div class="bottomspacer"></div>
    <?php
      foreach($view->result as $r) {
        $thumb = node_load($r->nid);

        if($thumb->field_images[0]['filepath'] != '')
          $p = $thumb->field_images[0]['filepath'];
        else
          $p = 'sites/default/files/imagefield_default_images/default.jpg';

        print '<div class="thumbcontainer inlineleft"><a href="/node/'.$thumb->nid.'" class="thumblink">';

        if($thumb->field_thumb[0]['filepath'] != '')
          print '<img class="imagecache imagecache-slidethumb dummy-imagecache" src="/'.$thumb->field_thumb[0]['filepath'].'" alt="'.$thumb->title.'" title="'.$thumb->title.'" />';
        else
          print theme('imagecache', 'slidethumb', $p, $thumb->title, $thumb->title);
        print '</a></div>';
      }
    ?>
  </div>
  <div class="clearboth"></div>
</div>