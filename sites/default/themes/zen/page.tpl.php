<?php
/*

 if ($title == 'Page not found'){
	header('Location: /');
	exit;
 }
 
 global $user;
 if($user->uid){
 } else {
	 if( arg(0) == 'user'){
		 
	 } else {
		header('Location: /holdingtech.php');
		exit;		 
	 }
 }
 
 */
?>

<?php
global $user;

function getmaxsize ($imgarray) {
  $max = 0;
  foreach($imgarray as $key => $img) {
    if($img["filepath"] != '')
      $max = $key;
  }
  return $max;
}

//if($node->type == 'six_column_page' || $node->type == 'column_page' || $node->type == 'page' || $node->type == 'long_content' || $node->type == 'parent_page') {
	
if(	is_single_image($node) ){
	
  $n = $node;
  if($n->field_images[0]["filepath"] != '') {
    $images[] = '<img class="slidebg stretch slide-nid-'.$r->nid.'" src="/'.$n->field_images[getmaxsize($n->field_images)]["filepath"].'" alt="'.$n->title.'" />';

    $hrefs[] = '{\'src\': "/'.$n->field_images[getmaxsize($n->field_images)]["filepath"].'", \'class\': "stretch slide-nid-'.$n->nid.'"}';
  }

  $jscripts[] = 'changeRes('.$n->nid.', "print");';
} else {
  $view = views_get_view('childs');
  $view->set_arguments(array($node->nid));
  $view->execute_display('default');

  $images = array();
  $jscripts = array();
  $hrefs = array();

  foreach($view->result as $r) {
    $n = node_load($r->nid);
    //krumo($n);
    if($n->field_images[0]["filepath"] != '') {
      $images[] = '<img class="slidebg stretch slide-nid-'.$r->nid.'" src="/'.$n->field_images[getmaxsize($n->field_images)]["filepath"].'" alt="'.$n->title.'" />';
	  
	  //WHU stefan I have changed the argument to strings ???.
      $hrefs[] = '{\'src\': "/'.$n->field_images[getmaxsize($n->field_images)]["filepath"].'", \'class\': "stretch slide-nid-'.$n->nid.'"}';
    }
	//strtolower($node->field_type[0]['value'])
	
    $jscripts[] = 'changeRes('.$n->nid.', "'.$node->field_type[0]['value'].'");';
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <title><?php print $title ?></title>
    <link rel="shortcut icon" href="/sites/default/files/zen_favicon_1.ico" type="image/vnd.microsoft.icon">
    <?php print $styles; ?>
    <style type="text/css" media="all">
      @import "/sites/default/themes/zen/reset.css";
      @import "/sites/default/themes/zen/reset-vms.css";
    </style>
    <style type="text/css" media="all">
      @import "/sites/default/themes/zen/menu.css";
    </style>
    <!--[if IE]>
<link type="text/css" rel="stylesheet" media="all" href="/sites/default/themes/zen/ie.css?x" />
<![endif]-->
    <!--[if IE 6]>
<link type="text/css" rel="stylesheet" media="all" href="/sites/default/themes/zen/ie6.css?x" />
<![endif]-->
<!--[if IE 7]>
<link type="text/css" rel="stylesheet" media="all" href="/sites/default/themes/zen/ie7.css?x" />
<![endif]-->

    <style type="text/css" media="all">
      @import "/sites/default/themes/zen/imageLoader.css";
    </style>

    <link rel="stylesheet" type="text/css" href="/sites/default/themes/zen/MyFontsWebfontsKit.css">
    <!-- <link href="http://fast.fonts.com/cssapi/47b82278-f2b4-4c62-8b31-85bb8fab3afe.css" rel="stylesheet" type="text/css" /> -->
    <link href="/sites/default/themes/zen/fontscom.css" rel="stylesheet" type="text/css" />

    <?php print $scripts; ?>
    <script type="text/javascript" src="/sites/default/themes/zen/js-general.js"></script>
    <script type="text/javascript" src="/sites/default/themes/zen/jquery.cycle.all.min.js"></script>
    <script type="text/javascript" src="/sites/default/themes/zen/jquery.imageLoader.1.2.js"></script>
    <script type="text/javascript" src="/sites/default/themes/zen/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="/sites/all/modules/ckeditor/ckeditor/ckeditor.js"></script>
		
		<script type="text/javascript" src="/sites/default/themes/zen/jquery.mousewheel.js"></script>
    <script type="text/javascript" src="/sites/default/themes/zen/jScrollPane.js"></script>
    <style type="text/css" >
			@import "/sites/default/themes/zen/jScrollPane.css";
		</style>
    
    <?php if(($node->type == 'project') && arg(2) != 'edit'){ ?>
    <script type="text/javascript">
		
			function onBeforePre(curr, next, opts, fwd) {
				$('.activeSlide img').animate({
					opacity: 0.6
				});
				if(beforewasexecuted > 0) {
					//txt = $(next).data('description');

					if($('#slideshow img:visible').next('img').length ){
						if($('#slideshow img:visible').next('img').attr('inverted') == 1){
							$('body').addClass('inverted');
						} else {
							if($('body').hasClass('inverted'))
								$('body').removeClass('inverted')
						}
					}
					
/*				if(txt == null)
						txt = '';
						$('.projectdescriptioninner').html(txt);*/
				}
				
				showslidetext_from_next(next);	
				
				beforewasexecuted = 1;
      }

      function onAfterPre(curr, next, opts, fwd) {
				$('.activeSlide img').animate({
					opacity: 1
				});
				if($('#slideshow img:visible').attr('inverted') == 1){
					$('body').addClass('inverted');
				} else {
					if($('body').hasClass('inverted'))
						$('body').removeClass('inverted')
				}

				
      }

			function showslidetext_from_next(next){
				
				slide_id = $(next).attr('nid');
				
				showslidetext(slide_id);
			}
			function showslidetext(slide_id){
				//$('#open_mainContent_wrapper').addClass('show_'+slide_id);
				$('.slide_text_box').hide();
				if($('#silde_nid_'+slide_id+' .slide_title').text() != ""){
					$('#silde_nid_'+slide_id).show();
				} else {
					$('#silde_nid_'+slide_id+' .slide_title').text('loading text...');
					$('#silde_nid_'+slide_id).show();
				}
				
				//.slide_title
			}
		
      var beforewasexecuted;
	    beforewasexecuted = 0;
      $(document).ready(function () {
				
					if( $("#slideshow img").length ){
						if($('#slideshow img').length == 1) {
							$('#slideshow img').show();
							return;
						}
					  $('#slideshow').cycle({
            	fx: 'fade',
							speed: '700',
							timeout:  0,
							next:   '#next2', 
							prev:   '#prev2', 
              pager:  '#nav',
              before:   onBeforePre,
              after:   onAfterPre,
              pagerAnchorBuilder: function(idx, slide) {
                // return selector string for existing anchor
                return '#nav .thumbcontainer:eq(' + idx + ') a';
              }
            });
							
							if( current_resolution == 1){
              <?php 
								print implode("\n", $jscripts);  
							?>
							}
					} else {
					
        	$("#slideshow").imageLoader({
        		images: [<?php if(is_array($hrefs)) print implode(',', $hrefs); ?>]
				  }, function(){
            if($('#slideshow img').length == 1) {
							$('#slideshow img').show();
							return;
						}
					  $('#slideshow').cycle({
            	fx: 'fade',
							speed: '700',
							timeout:  0,
							next:   '#next2', 
							prev:   '#prev2', 
              pager:  '#nav',
              before:   onBeforePre,
              after:   onAfterPre,
              pagerAnchorBuilder: function(idx, slide) {
                // return selector string for existing anchor
                return '#nav .thumbcontainer:eq(' + idx + ') a';
              }
            });
             
              <?php print implode("\n", $jscripts);  ?>
            });
					} //if( $("#slideshow img").length ){
         });
    </script>
    <?php 
		
		//} elseif(($node->type == 'six_column_page' || $node->type == 'column_page' || $node->type == 'page' || $node->type == 'long_content' || $node->type == 'parent_page') && arg(2) != 'edit'){ 
		} elseif( is_single_image($node) ){ 
		
		
		?>
    
    <script type="text/javascript">
      var beforewasexecuted;
	    beforewasexecuted = 0;
      $(document).ready(function () {

				if( $("#slideshow img").length ){
				
				} else {

        $("#slideshow").imageLoader({
            images: [<?php if(is_array($hrefs)) print implode(',', $hrefs); ?>]
			
          }, function(){
            
      //if($('#slideshow img').length == 1) {
				//NOTE LENGTH IS ALWAYS 1 for the above node types
        $('#slideshow img').show();
        return;
      //}
			
			/*
			   $('#slideshow').cycle({
                  fx: 'fade',
                  speed: '700',
	              timeout:  0,
						  next:   '#next2', 
						  prev:   '#prev2', 
                  pager:  '#nav',
                  before:   onBefore,
                  after:   onAfter,
                  pagerAnchorBuilder: function(idx, slide) {
                    // return selector string for existing anchor
                    return '#nav .thumbcontainer:eq(' + idx + ') a';
                  }
              });


              function onBefore(curr, next, opts, fwd) {
                $('.activeSlide img').animate({
                  opacity: 0.6
                });
                if(beforewasexecuted > 0) {
                  txt = $(next).data('description');

                  if($(next).data('inverted') == '1')
                    $('body').addClass('inverted');
                  else
                    if($('body').hasClass('inverted'))
                      $('body').removeClass('inverted')

                  if(txt == null)
                    txt = '';
                  $('.projectdescriptioninner').html(txt);

                }
                beforewasexecuted = 1;
              }

              function onAfter(curr, next, opts, fwd) {
                $('.activeSlide img').animate({
                  opacity: 1
                });
              }
              <?php print implode("\n", $jscripts);  ?>
							*/
            });
				
				} // if( $("#slideshow img").length ){
				
          });
    </script>
    <?php }  ?>

<!--<script type="text/javascript" src="pngfix.js"></script>-->
<script type="text/javascript" >
  $(document).ready(function () {
    //$('#header').supersleight();
  });
	
	<?php
		$image_info = array();
		$slide_images = get_images_php($node->nid, $image_info, $image_text);
	
	
		$resoution = imagebg_session_resoution_get();
		$resoution_height = imagebg_session_resoution_height_get();
		$ratio = $resoution / $resoution_height;
		$slide_text = '';
		if( isset($image_info[0]['width']) ){
			// not based on first image
			$ratio_image = $image_info[0]['width']/$image_info[0]['height'];

			foreach($image_text as $key => $value){
				 $a_title = $value['title'];
				 $a_teaser = $value['teaser'];
				 $a_body = $value['body'];
				 $a_nid = $value['nid'];
				 
				 $slide_text .= '<div id="silde_nid_'.$a_nid.'" class="slide_text_box silde_nid_'.$a_nid.' slide_key_'.$key.'">';
				 
				 $slide_text .= '<div class="slide_title">'.$a_title.'</div>';
				 $slide_text .= '<div class="slide_teaser">'.$a_teaser.'</div>';
				 $slide_text .= '<div class="slide_body">'.$a_body.'</div>';
				 
				 $slide_text .= '</div>';
			}
			
		} else {
			$ratio_image = 1.33; // average screen???
		}
		
		
		if( $ratio > $ratio_image ){
			$image_class = " image_width_set ratio_".$ratio." ratio_image_".$ratio_image;
		} else {
			$image_class = " image_height_set ratio_".$ratio." ratio_image_".$ratio_image;
		}
	?>
	
	var current_resolution = <?php print $resoution; ?>;
	var current_resolution_height = <?php print $resoution_height ?>;
	var current_resolution_ratio = <?php print $ratio ?>;

	

</script>
    <style type="text/css">
			.univers14,
			.univers14 a{
				font-family: "Univers LT W01 65 Bold", Arial, Helvetica, sans-serif !important;
				font-weight: normal !important;
				font-size: 14px !important;
			}
			.univers14 a:hover{
				color: #666666 !important;
			}
			h2.univers14{
				height: 19px;
			}
			
			<?php // if( $user->uid ){	?>		
			.vertcontent{
				background-image: url(/sites/default/themes/zen/images/bgcontent_col_920.png);
				background-position: 0 0;
				background-repeat: repeat-y;
			}
			.vertcontent.has_grid{
				background-image: url(/sites/default/themes/zen/images/bgcontent_col_920.gif);
			}
			
			.nid-8 .vertcontent,
			.nid-723 .vertcontent{
				background-image: url(/sites/default/themes/zen/images/bgcontent_col_home_620.png);
			}
			.node-type-page .vertleft, 
			.node-type-page .vertright, 
			.node-type-vertical-page .vertleft, 
			.node-type-vertical-page .vertright {
				background-image: none;
			}
			
			<?php //}	?>
		</style>
    
    
  </head>
  <?php
    $sec = '';
    if($node->field_type[0]['value'])
      $sec = ' '.strtolower($node->field_type[0]['value']);
    $node->field_inverted[0]['value'] ? $sec .= ' inverted' : '';
    $sec .= ' nid-'.$node->nid;
  ?>
  <body class="<?php print $body_classes.$sec.$image_class; ?>">
  <div id='height-box'>
    <?php if ($node->type == 'project') { ?>
		<a id="prev2" href="#">Prev</a>
		<a id="next2" href="#">Next</a>
    <?php } ?>
    <div id="header">
      <h1 id="logocontainer"><a id="logo" href="/" title="Home">Effusion technical</a></h1>
      <div id="mainmenu_new">
        <?php 
					print menu_tree('menu-new-navagation');
				?>
      </div>
    </div>

    <div id="mainContent">
      

      <div id="mainContentInner">
        <?php print $messages; ?>
        <?php print $tabs; ?>
        <?php print $content; ?>
	     
        <?php 
					/*if ($is_front){
						//run_view('promote_options', 'default');
						//run_view('promote_options', 'block_1');
						$view = get_view('promote_options', 'block_2'); // font left
						$view->execute();
						foreach($view->result as $r) {
							$n = node_load($r->nid);
							print '<h2><a href="/node/'.$r->nid.'">'.$n->title.'</a></h2>';
						}
						//make_right_box();
						//run_view('promote_options', 'block_3');
						//run_view('promote_options', 'block_4');
						
					}*/
				?>
      </div>
      </div>
    <div id="footer">

    </div>
    <?php 
		//if(($node->type == 'project' || $node->type == 'six_column_page' || $node->type == 'column_page' || $node->type == 'page' || $node->type == 'long_content' || $node->type == 'parent_page') && arg(2) != 'edit'): 
		if( is_single_image($node) || ( ($node->type == 'project') && (arg(2) != 'edit') ) ):
		?>

    <!--div id="slideshow">
    </div-->
    	<?php 
			//global $user;
			//if($user->uid == 1){
				print '<div id="slideshow">';
				print $slide_images;
				print '</div>';
				if($user->uid == 1){
					if( $node->type == 'project' ){
						print '<div id="open_mainContent_wrapper" class="start_hidded">';
						print '<a id="open_mainContent" title="Stop the slideshow and open the navagation">Stop slide show</a>';
						print '<div class="clearfix"></div>';
					
						//Potential slide controls
						/*print '<div id="slide_controls">';
							print '<a id="slide_stop" title="Stop the slideshow and open the navagation">Stop</a>';
							print '<a id="slide_pause" title="Pause/Play  the slideshow ">Pause</a>';
							print '<a id="slide_slow" title="Play/Pause the slideshow slowly">Slowly</a>';
							print '<a id="slide_play" title="Play/Pause the slideshow" class="active">Play</a>';
							print '<a id="slide_fast" title="Play/Pause the slideshow fast">Fast</a>';
						print '</div>';
						print '<div class="clearfix"></div>';*/

						print $slide_text;

						print '<div class="clearfix"></div>';
						print '</div>';
					}
				}
			//} else {
			//	print '<div id="slideshow">';
				//print get_images_php($node->nid);
			//	print '</div>';
			//}
			?>
    <?php endif; ?>
    <?php print $closure; ?>
    </div>

  <script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-2696878-2']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

  </script>


 </body>
</html>

<script type="text/javascript">
  $(document).ready(function () {
        <?php
          if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.') !== false) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.') === false)) {
        ?>
           $('#mainContent').css('position', 'absolute');
           $('#mainContent').css('bottom', '45px');

        <?php
          }
        ?>
  });
</script>