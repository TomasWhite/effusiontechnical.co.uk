<head>


  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Web</title>

  <style type="text/css" media="print">
    @import "/sites/default/themes/zen/print.css";
  </style>
  <style type="text/css" media="all">
    @import "/sites/default/themes/zen/reset.css";
  </style>
  <!--[if IE]>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/default/themes/zen/ie.css?x"/>
  <![endif]-->
  <!--[if IE 6]>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/default/themes/zen/ie6.css?x"/>
  <![endif]-->

  <style type="text/css" media="all">
    @import "/sites/default/themes/zen/imageLoader.css";
  </style>
  <script type="text/javascript" src="/sites/default/modules/jquery_update/replace/jquery.min.js?J"></script>
  <script type="text/javascript" src="/misc/drupal.js?J"></script>
  <script type="text/javascript" defer="defer" src="/sites/all/modules/acquia/admin_menu/admin_menu.js?J"></script>
  <script type="text/javascript" src="/sites/all/modules/panels/js/panels.js?J"></script>
  <script type="text/javascript" src="/sites/default/modules/render/plugins/sifr/sifr.js?J"></script>
  <script type="text/javascript" src="/sites/default/files/render/sifr3-rules.js?J"></script>
  <script type="text/javascript">
    <!--//--><![CDATA[//><!--
    jQuery.extend(Drupal.settings, { "basePath": "/", "admin_menu": { "margin_top": 1, "position_fixed": 1, "tweak_tabs": 1 }, "CTools": { "pageId": "page-8d4cb64c1153a30620270cd95d5f45da" } });
    //--><!]]>
  </script>
  <script type="text/javascript" src="/sites/default/themes/zen/js-general.js"></script>
  <script type="text/javascript" src="/sites/default/themes/zen/jquery.cycle.all.min.js"></script>
  <script type="text/javascript" src="/sites/default/themes/zen/jquery.imageLoader.1.2.js"></script>
  <script type="text/javascript" src="/sites/default/themes/zen/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="/sites/all/modules/ckeditor/ckeditor/ckeditor.js"></script>

  <style type="text/css" charset="utf-8">/* See license.txt for terms of usage */
  .firebugCanvas {
    position: fixed;
    top: 0;
    left: 0;
    display: none;
    border: 0 none;
    margin: 0;
    padding: 0;
    outline: 0;
  }

  .firebugCanvas:before, .firebugCanvas:after {
    content: "";
  }

  .firebugHighlight {
    z-index: 2147483646;
    position: fixed;
    background-color: #3875d7;
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0 none;
  }

  .firebugHighlight:before, .firebugHighlight:after {
    content: "";
  }

  .firebugLayoutBoxParent {
    z-index: 2147483646;
    position: fixed;
    background-color: transparent;
    border-top: 0 none;
    border-right: 1px dashed #E00 !important;
    border-bottom: 1px dashed #E00 !important;
    border-left: 0 none;
    margin: 0;
    padding: 0;
    outline: 0;
  }

  .firebugRuler {
    position: absolute;
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0 none;
  }

  .firebugRuler:before, .firebugRuler:after {
    content: "";
  }

  .overflowRulerX > .firebugRulerV {
    left: 0;
  }

  .overflowRulerY > .firebugRulerH {
    top: 0;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  .firebugLayoutBox {
    margin: 0;
    padding: 0;
    border: 0 none;
    outline: 0;
  }

  .firebugLayoutBox:before, .firebugLayoutBox:after {
    content: "";
  }

  .firebugLayoutBoxOffset {
    z-index: 2147483646;
    position: fixed;
    opacity: 0.8;
  }

  .firebugLayoutBoxMargin {
    background-color: #EDFF64;
  }

  .firebugLayoutBoxBorder {
    background-color: #666666;
  }

  .firebugLayoutBoxPadding {
    background-color: SlateBlue;
  }

  .firebugLayoutBoxContent {
    background-color: SkyBlue;
  }

  .firebugLayoutLine {
    z-index: 2147483646;
    background-color: #000000;
    opacity: 0.4;
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0 none;
  }

  .firebugLayoutLine:before, .firebugLayoutLine:after {
    content: "";
  }

  .firebugLayoutLineLeft, .firebugLayoutLineRight {
    position: fixed;
    width: 1px;
    height: 100%;
  }

  .firebugLayoutLineTop, .firebugLayoutLineBottom {
    position: fixed;
    width: 100%;
    height: 1px;
  }

  .firebugLayoutLineTop {
    margin-top: -1px;
    border-top: 1px solid #999999;
  }

  .firebugLayoutLineRight {
    border-right: 1px solid #999999;
  }

  .firebugLayoutLineBottom {
    border-bottom: 1px solid #999999;
  }

  .firebugLayoutLineLeft {
    margin-left: -1px;
    border-left: 1px solid #999999;
  }

  .fbProxyElement {
    position: absolute;
    background-color: transparent;
    z-index: 2147483646;
    margin: 0;
    padding: 0;
    outline: 0;
    border: 0 none;
  }
  </style>
</head>
<body
    onpageshow='event.persisted &amp;&amp; (function(){var allInstances = CKEDITOR.instances, editor, doc;for ( var i in allInstances ){	editor = allInstances[ i ];	doc = editor.document;	if ( doc )	{		doc.$.designMode = "off";		doc.$.designMode = "on";	}}})();'
    class="not-front logged-in node-type-page one-sidebar sidebar-left page-web section-web nid-5 admin-menu">
<div id="height-box">
  <div id="header">
    <h1 id="logocontainer"><a id="logo" href="/" title="Home">Effusion</a></h1>

    <div id="mainmenu">
      <ul class="links">
        <li class="menu-3630 active-trail first active"><a href="/web" title="Web" class="active">Web</a></li>
        <li class="menu-3631"><a href="/print" title="print / id">Print</a></li>
        <li class="menu-3963"><a href="/identity" title="Identity">Identity</a></li>
        <li class="menu-3632"><a href="/miscellaneous" title="miscellaneous">Miscellaneous</a></li>
        <li class="menu-3615"><a href="/content/6-column-test" title="About">About</a></li>
        <li class="menu-3633 last"><a href="/content/contact" title="contact">Contact</a></li>
      </ul>
    </div>
  </div>
  <div style="bottom: auto; top: 100px;" id="mainContent">
    <div class="projecttabs">
      <ul class="tabs">
        <!-- 2 - client/sector, 4 - cost, 3 - project, 5 - when -->
        <li id="tabwhen"><a class="" href="/web/when">When we did it</a></li>
        <li id="tabcost"><a class="" href="/web/cost">Cost</a></li>
        <li id="tabtype"><a class="" href="/web/type">Type of project</a></li>
        <li id="tabsector"><a class="active" href="/web/sector">Sector</a></li>
      </ul>
      <div class="clearboth"></div>
    </div>
    <div id="mainContentInner">
      <ul class="tabs primary clear-block">


      </ul>
      <div class="projectdetails">
        <h2 style="" class="cattitle univers14 sIFR-replaced">
          <object style="" data="/sites/default/files/render/univers.swf" name="sIFR_replacement_0" id="sIFR_replacement_0"
                  type="application/x-shockwave-flash" class="sIFR-flash" width="880" height="23">
            <param
                value="id=sIFR_replacement_0&amp;content=Public%2520sector&amp;width=880&amp;renderheight=23&amp;link=&amp;target=&amp;size=14&amp;css=.sIFR-root%257Bdisplay%253Ablock%253Bfont-weight%253Anormal%253Bfont-style%253Anormal%253Bcolor%253A%2523000000%253Bbackground-color%253A%253Bmargin-left%253A0%253Bmargin-right%253A0%253Btext-align%253Aleft%253Btext-indent%253A0%253Btext-decoration%253Anone%253Bletter-spacing%253A0%253Bleading%253A0%253B%257Da%257Btext-decoration%253Anone%253B%257Da%253Alink%257Bcolor%253A%2523000000%253B%257Da%253Ahover%257Bcolor%253A%2523666666%253Btext-decoration%253Anone%253B%257D&amp;cursor=arrow&amp;tunewidth=0&amp;tuneheight=0&amp;offsetleft=&amp;offsettop=&amp;fitexactly=false&amp;preventwrap=false&amp;forcesingleline=false&amp;antialiastype=&amp;thickness=&amp;sharpness=&amp;kerning=false&amp;gridfittype=pixel&amp;flashfilters=&amp;opacity=100&amp;blendmode=&amp;selectable=true&amp;fixhover=true&amp;events=false&amp;delayrun=false&amp;version=436"
                name="flashvars">
            <param value="transparent" name="wmode">
            <param value="transparent" name="bgcolor">
            <param value="always" name="allowScriptAccess">
            <param value="best" name="quality">
          </object>
          <span id="sIFR_replacement_0_alternate" class="sIFR-alternate">Public sector</span></h2>
        <div style="display: block;" class="slidepanel panel-0 opened">
          <div class="catcontainer inlineleft">
            <div id="leftprojcol" class="inlineleft leftprojcol"></div>
            <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/31"
                                                                                              title="Artist Studio Finder: Web">Artist
              Studio Finder: Web</a></p>

              <p class="projecttitle"><a href="/node/37" title="Sintropher: Web">Sintropher: Web</a></p>

              <p class="projecttitle"><a href="/node/307" title="Core Cities: Web">Core Cities: Web</a></p></div>
            <div class="catthumbs inlineleft">
              <div class="thumbcontainer inlineleft"><a href="/node/31" class="thumblink"><img style="opacity: 0.6;"
                                                                                               src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0133_artiststudiofinder1.jpg"
                                                                                               alt="Artist Studio Finder: Web"
                                                                                               title="Artist Studio Finder: Web"
                                                                                               class="imagecache imagecache-slidethumb"
                                                                                               width="67" height="81">

                <div class="nid">31</div>
              </a></div>
              <div class="thumbcontainer inlineleft"><a href="/node/37" class="thumblink"><img style="opacity: 0.6;"
                                                                                               src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0109_sintropher3.jpg"
                                                                                               alt="Sintropher: Web" title="Sintropher: Web"
                                                                                               class="imagecache imagecache-slidethumb"
                                                                                               width="67" height="81">

                <div class="nid">37</div>
              </a></div>
              <div class="thumbcontainer inlineleft"><a href="/node/307" class="thumblink"><img style="opacity: 0.6;"
                                                                                                src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0015_corecities5.jpg"
                                                                                                alt="Core Cities: Web"
                                                                                                title="Core Cities: Web"
                                                                                                class="imagecache imagecache-slidethumb"
                                                                                                width="67" height="81">

                <div class="nid">307</div>
              </a></div>
              <div class="clearboth cleanup-thumb"></div>
            </div>
            <div class="clearboth cleanup-thumb-cat"></div>
          </div>
          <h2 style="" class="cattitle univers14 negativemargin sIFR-replaced">
            <object style="" data="/sites/default/files/render/univers.swf" name="sIFR_replacement_1" id="sIFR_replacement_1"
                    type="application/x-shockwave-flash" class="sIFR-flash" width="880" height="23">
              <param
                  value="id=sIFR_replacement_1&amp;content=Start-ups%2520and%2520Private%2520sector&amp;width=880&amp;renderheight=23&amp;link=&amp;target=&amp;size=14&amp;css=.sIFR-root%257Bdisplay%253Ablock%253Bfont-weight%253Anormal%253Bfont-style%253Anormal%253Bcolor%253A%2523000000%253Bbackground-color%253A%253Bmargin-left%253A0%253Bmargin-right%253A0%253Btext-align%253Aleft%253Btext-indent%253A0%253Btext-decoration%253Anone%253Bletter-spacing%253A0%253Bleading%253A0%253B%257Da%257Btext-decoration%253Anone%253B%257Da%253Alink%257Bcolor%253A%2523000000%253B%257Da%253Ahover%257Bcolor%253A%2523666666%253Btext-decoration%253Anone%253B%257D&amp;cursor=arrow&amp;tunewidth=0&amp;tuneheight=0&amp;offsetleft=&amp;offsettop=&amp;fitexactly=false&amp;preventwrap=false&amp;forcesingleline=false&amp;antialiastype=&amp;thickness=&amp;sharpness=&amp;kerning=false&amp;gridfittype=pixel&amp;flashfilters=&amp;opacity=100&amp;blendmode=&amp;selectable=true&amp;fixhover=true&amp;events=false&amp;delayrun=false&amp;version=436"
                  name="flashvars">
              <param value="transparent" name="wmode">
              <param value="transparent" name="bgcolor">
              <param value="always" name="allowScriptAccess">
              <param value="best" name="quality">
            </object>
            <span id="sIFR_replacement_1_alternate" class="sIFR-alternate">Start-ups and Private sector</span></h2>
          <div style="display: block;" class="slidepanel panel-1 closed">
            <div class="catcontainer inlineleft">
              <div id="leftprojcol" class="inlineleft leftprojcol"></div>
              <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/30" title="Vartma: Web">Vartma:
                Web</a></p>

                <p class="projecttitle"><a href="/node/39" title="Promoveo: Web">Promoveo: Web</a></p>

                <p class="projecttitle"><a href="/node/45" title="Natolli: Web">Natolli: Web</a></p></div>
              <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/47" title="Nixsi: Web">Nixsi:
                Web</a></p>

                <p class="projecttitle"><a href="/node/49" title="Ducat: Web">Ducat: Web</a></p>

                <p class="projecttitle"><a href="/node/301" title="South London Clinic: Web">South London Clinic: Web</a></p></div>
              <div class="catthumbs inlineleft">
                <div class="thumbcontainer inlineleft"><a href="/node/30" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0145_vartma2_0.jpg"
                                                                                                 alt="Vartma: Web" title="Vartma: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">30</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/39" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0093_promoveo2.jpg"
                                                                                                 alt="Promoveo: Web" title="Promoveo: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">39</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/45" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0054_natolli5.jpg"
                                                                                                 alt="Natolli: Web" title="Natolli: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">45</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/47" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0045_nixsi3.jpg"
                                                                                                 alt="Nixsi: Web" title="Nixsi: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">47</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/49" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0062_duact3.jpg"
                                                                                                 alt="Ducat: Web" title="Ducat: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">49</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/301" class="thumblink"><img style="opacity: 0.6;"
                                                                                                  src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0011_southlondonclinic1.jpg"
                                                                                                  alt="South London Clinic: Web"
                                                                                                  title="South London Clinic: Web"
                                                                                                  class="imagecache imagecache-slidethumb"
                                                                                                  width="67" height="81">

                  <div class="nid">301</div>
                </a></div>
                <div class="clearboth cleanup-thumb"></div>
              </div>
              <div class="clearboth cleanup-thumb-cat"></div>
            </div>
            <h2 style="" class="cattitle univers14 negativemargin sIFR-replaced">
              <object style="" data="/sites/default/files/render/univers.swf" name="sIFR_replacement_2" id="sIFR_replacement_2"
                      type="application/x-shockwave-flash" class="sIFR-flash" width="880" height="23">
                <param
                    value="id=sIFR_replacement_2&amp;content=Non%2520Profit%2520/%2520Third%2520sector&amp;width=880&amp;renderheight=23&amp;link=&amp;target=&amp;size=14&amp;css=.sIFR-root%257Bdisplay%253Ablock%253Bfont-weight%253Anormal%253Bfont-style%253Anormal%253Bcolor%253A%2523000000%253Bbackground-color%253A%253Bmargin-left%253A0%253Bmargin-right%253A0%253Btext-align%253Aleft%253Btext-indent%253A0%253Btext-decoration%253Anone%253Bletter-spacing%253A0%253Bleading%253A0%253B%257Da%257Btext-decoration%253Anone%253B%257Da%253Alink%257Bcolor%253A%2523000000%253B%257Da%253Ahover%257Bcolor%253A%2523666666%253Btext-decoration%253Anone%253B%257D&amp;cursor=arrow&amp;tunewidth=0&amp;tuneheight=0&amp;offsetleft=&amp;offsettop=&amp;fitexactly=false&amp;preventwrap=false&amp;forcesingleline=false&amp;antialiastype=&amp;thickness=&amp;sharpness=&amp;kerning=false&amp;gridfittype=pixel&amp;flashfilters=&amp;opacity=100&amp;blendmode=&amp;selectable=true&amp;fixhover=true&amp;events=false&amp;delayrun=false&amp;version=436"
                    name="flashvars">
                <param value="transparent" name="wmode">
                <param value="transparent" name="bgcolor">
                <param value="always" name="allowScriptAccess">
                <param value="best" name="quality">
              </object>
              <span id="sIFR_replacement_2_alternate" class="sIFR-alternate">Non Profit / Third sector</span></h2>
            <div style="display: block;" class="slidepanel panel-2 closed">
              <div class="catcontainer inlineleft">
                <div id="leftprojcol" class="inlineleft leftprojcol"></div>
                <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/319" title="Fastlaners: Web">Fastlaners:
                  Web</a></p>

                  <p class="projecttitle"><a href="/node/275" title="Young Foundation: Web">Young Foundation: Web</a></p>

                  <p class="projecttitle"><a href="/node/26" title="IVAR: Web">IVAR: Web</a></p></div>
                <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/27"
                                                                                                  title="Operation Black Vote: Web">Operation
                  Black Vote: Web</a></p>

                  <p class="projecttitle"><a href="/node/28" title="Who Runs my City?: Web">Who Runs my City?: Web</a></p>

                  <p class="projecttitle"><a href="/node/29" title="Social Innovator: Web">Social Innovator: Web</a></p></div>
                <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/32"
                                                                                                  title="London's Artist Quarter: Web">London's
                  Artist Quarter: Web</a></p>

                  <p class="projecttitle"><a href="/node/33" title="Book of Travels: Web">Book of Travels: Web</a></p>

                  <p class="projecttitle"><a href="/node/34" title="Studio Schools Trust: Web">Studio Schools Trust: Web</a></p></div>
                <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/35"
                                                                                                  title="3 Choirs Festival: Web">3 Choirs
                  Festival: Web</a></p>

                  <p class="projecttitle"><a href="/node/43" title="Maslaha: Web">Maslaha: Web</a></p>

                  <p class="projecttitle"><a href="/node/46" title="Night Workers' Blog: Web">Night Workers' Blog: Web</a></p></div>
                <div id="rightprojcol" class="inlineleft rightprojcol"><p class="projecttitle"><a href="/node/50"
                                                                                                  title="The Eckhart Society: Web">The
                  Eckhart Society: Web</a></p>

                  <p class="projecttitle"><a href="/node/51" title="St Lawrence, Ludlow: Web">St Lawrence, Ludlow: Web</a></p>

                  <p class="projecttitle"><a href="/node/52" title="Innovate: Web">Innovate: Web</a></p></div>
                <div class="clearboth leftprojcol-rightprojcol"></div>
              </div>
              <div class="catthumbs inlineleft">
                <div class="thumbcontainer inlineleft"><a href="/node/319" class="thumblink"><img style="opacity: 0.6;"
                                                                                                  src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0029_fastlaners4.jpg"
                                                                                                  alt="Fastlaners: Web"
                                                                                                  title="Fastlaners: Web"
                                                                                                  class="imagecache imagecache-slidethumb"
                                                                                                  width="67" height="81">

                  <div class="nid">319</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/275" class="thumblink"><img style="opacity: 0.6;"
                                                                                                  src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0166_yf6.jpg"
                                                                                                  alt="Young Foundation: Web"
                                                                                                  title="Young Foundation: Web"
                                                                                                  class="imagecache imagecache-slidethumb"
                                                                                                  width="67" height="81">

                  <div class="nid">275</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/26" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0071_ivar6.jpg"
                                                                                                 alt="IVAR: Web" title="IVAR: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">26</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/27" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0164_obv2.jpg"
                                                                                                 alt="Operation Black Vote: Web"
                                                                                                 title="Operation Black Vote: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">27</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/28" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0159_whoruns2.jpg"
                                                                                                 alt="Who Runs my City?: Web"
                                                                                                 title="Who Runs my City?: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">28</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/29" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0152_si1.jpg"
                                                                                                 alt="Social Innovator: Web"
                                                                                                 title="Social Innovator: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">29</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/32" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0102_laq1.jpg"
                                                                                                 alt="London's Artist Quarter: Web"
                                                                                                 title="London's Artist Quarter: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">32</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/33" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0128_tob1.jpg"
                                                                                                 alt="Book of Travels: Web"
                                                                                                 title="Book of Travels: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">33</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/34" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0116_sst1.jpg"
                                                                                                 alt="Studio Schools Trust: Web"
                                                                                                 title="Studio Schools Trust: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">34</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/35" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0117_threechoirs4.jpg"
                                                                                                 alt="3 Choirs Festival: Web"
                                                                                                 title="3 Choirs Festival: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">35</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/43" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0080_maslaha1.jpg"
                                                                                                 alt="Maslaha: Web" title="Maslaha: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">43</div>
                </a></div>
                <div class="thumbcontainer inlineleft"><a href="/node/46" class="thumblink"><img style="opacity: 0.6;"
                                                                                                 src="http://effusion.co.uk/sites/default/files/imagecache/slidethumb/thumbnail_web_0053_nwb1.jpg"
                                                                                                 alt="Night Workers' Blog: Web"
                                                                                                 title="Night Workers' Blog: Web"
                                                                                                 class="imagecache imagecache-slidethumb"
                                                                                                 width="67" height="81">

                  <div class="nid">46</div>
                </a></div>
                <div class="clearboth cleanup-thumb"></div>
              </div>
              <div class="clearboth cleanup-thumb-cat"></div>
            </div>
            <a href="#"><img id="archivebtn" src="/sites/default/themes/zen/images/archive.jpg" alt="Archive" title="Archive"></a>
          </div>
        </div>
      </div>
      <div id="footer">

      </div>
    </div>
  </div>
</body>