<?php 
$grid = makeGrid($node); 
$extr_css = "has_grid_no";
if($grid){
	$extr_css = "has_grid";
}
?>

<div class="vertcontent <?php print $extr_css; ?>">
  <div class="vertleft inlineleft">
    <h1 class="title"><?php print $node->title; ?></h1>
    <?php print $node->body; ?>
    
    <?php print $grid; ?>
    
    <?php print display_vblock($node->nid, 'special', true); ?>
  </div>
  <div class="vertright inlineleft">
    <?php
      display_vblock($node->nid);
			make_right_box();
    ?>
  </div>
  <div class="clearboth"></div>
</div>