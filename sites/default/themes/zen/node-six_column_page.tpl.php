<div class="projectdetails">
<div class="col1 inlineleft width34percent">
<?php
if($node->field_linkcol1[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol1[0]['value'].'">'. $node->field_titlecol1[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol1[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print check_markup($node->content['body']['#value'], 3, false) ?></div>
</div>
<div class="col1 inlineleft width33percent">
<?php
if($node->field_linkcol2[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol2[0]['value'].'">'. $node->field_titlecol2[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol2[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print $node->field_col2[0]['value'] ?></div>
</div>
<div class="col1 inlineleft width33percent">
<?php
if($node->field_linkcol3[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol3[0]['value'].'">'. $node->field_titlecol3[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol3[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print $node->field_col3[0]['value'] ?></div>
</div>

<div class="clearboth"></div>

<div class="col1 inlineleft width34percent">
<?php
if($node->field_linkcol4[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol4[0]['value'].'">'. $node->field_titlecol4[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol4[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print $node->field_col4[0]['value'] ?></div>
</div>
<div class="col1 inlineleft width33percent">
  <?php
if($node->field_linkcol5[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol5[0]['value'].'">'. $node->field_titlecol5[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol5[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print $node->field_col5[0]['value'] ?></div>
</div>
<div class="col1 inlineleft width33percent">
  <?php
if($node->field_linkcol6[0]['value'] != '')
  $coltitle = '<a href="'.$node->field_linkcol6[0]['value'].'">'. $node->field_titlecol6[0]['value'] .'</a>';
else
  $coltitle = $node->field_titlecol6[0]['value'];
?>
<h2 class="univers14 bottommargin"><?php print $coltitle; ?></h2>
<div class="padright"><?php print $node->field_col6[0]['value'] ?></div>
</div>

<div class="clearboth"></div>
</div>