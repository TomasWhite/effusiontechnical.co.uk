<h2 class="univers14"><?php print $node->title ?></h2>
<div class="projectdetails">
<div class="col1 inlineleft width50percent">
<div class="padright">
<?php
if($node->field_visual[0]['filepath']) {
?>
<img style="float: left; margin-right: 10px" src="/<?php print $node->field_visual[0]['filepath'] ?>" alt="<?php print $node->title ?>" title="<?php print $node->title ?>"/>
<?php
}
?>
<?php print check_markup($node->content['body']['#value'], 3, false) ?></div>
<a href="#" onclick="history.back(); return false;" class="bkbtn">Back</a>

</div>
<div class="col1 inlineleft width50percent">
<div class="padright"><?php print $node->field_col2[0]['value'] ?></div>
</div>
<div class="clearboth"></div>
</div>