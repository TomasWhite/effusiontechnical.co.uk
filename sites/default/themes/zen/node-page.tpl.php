<div class="vertcontent">
  <div class="vertleft inlineleft">
    <h1 class="title"><?php print $node->title ?></h1>
    
    <?php 
		print $node->body;
    
    if ($is_front){
						//run_view('promote_options', 'default');
						//run_view('promote_options', 'block_1');
						$view = get_view('promote_options', 'block_2'); // font left
						$view->execute();

						foreach($view->result as $key => $r) {
                          $extraclass = '';
                          $thumb = '';
						  $n = node_load($r->nid);
							//print '<h2><a href="/node/'.$r->nid.'">'.$n->title.'</a></h2>';

                          if($key < 2) {
														//print '<pre>';
														//print_r($node->field_thumb);
														//print '</pre>';
														//print '<pre>';
														//print_r($node->field_images);
														//print '</pre>';
														
                            $extraclass = 'thumb';
                            $img = theme('imagecache', '170x97', $n->field_thumb[0]['filepath']);
														//print '<pre>';
														//print_r($img);
														//print '</pre>';
                            $thumb = <<<html
                                <div class="boxthumb"><a href="/node/$n->nid">$img</a></div>
html;
                          }
                          $desc = truncate_utf8(strip_tags($n->field_teaser[0]['value']), 150, true, true);
                          print <<<html
                            <div class="box box-$key inlineleft $extraclass">
                                <h2 class="boxtitle clearfix"><a href="/node/$n->nid"><span>$n->title</span></a></h2>
                                <div class="clearboth"></div>
                                $thumb
                                <div class="boxdesc"><a href="/node/$n->nid">$desc &raquo;</a></div>
                            </div>
html;
                          if(($key + 1) % 2 == 0)
                            print '<div class="clearboth"></div>';

                          if($key == 3)
                            break;

						}
						//make_right_box();
						//run_view('promote_options', 'block_3');
						//run_view('promote_options', 'block_4');

/*						print "Front|";		
						
						$view = views_get_view('promote_options');
						//$view->set_arguments(array($node->nid));
						print $view->set_display('block-2');
						//$view->execute_display('block-2');
						print "|";		
						//$view->pre_execute();
				    $view->execute();
						foreach($view->result as $r) {
							$n = node_load($r->nid);
							print '<h2>'.$n->title.'</h2>';
						}*/
					}
     ?>
  </div>
  <div class="vertright inlineleft">
    <?php
      display_vblock($node->nid);
			make_right_box();
    ?>
  </div>
  <div class="clearboth"></div>
</div>