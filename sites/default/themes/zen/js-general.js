/* This is used sitewide (on the whole website) */

 /*function myEaseInSine(x, t, b, c, d) {
		return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
	}

	function myEaseOutSine(x, t, b, c, d) {
		return c * Math.sin(t/d * (Math.PI/2)) + b;
	}*/


var test_value = 0;

function changeRes(nid, type) {
  csrc = $('#slidebg').attr('src');
	
	if(current_resolution != 0){
		test_value = current_resolution;
	} else {
		test_value = $(window).width();
	}
	
  //alert(type);
  if (type == 'Web' || type == 'Identity'){
	  //alert('Web:'+screen.width );
	  if(test_value>=2000) {
		$.getJSON('/effusion/getimage/' + nid  + '/3', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
		  $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-1500 width-web');
		});
	  }
	  else if(test_value>=1280) {
		$.getJSON('/effusion/getimage/' + nid  + '/2', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-1280 width-web');
		});
	  }
	  else if(test_value>=1024) {
		$.getJSON('/effusion/getimage/' + nid  + '/1', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-1024 width-web');
		});
	  } else {
		$.getJSON('/effusion/getimage/' + nid + '/0', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-else width-else-web width-web');
		});
	  }
	  
  } else {
	  if(test_value>=1800) {
		$.getJSON('/effusion/getimage/' + nid  + '/2', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).attr('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-1800 width-not-web');
		});
	  }
	  else if(test_value>=1200) {
		$.getJSON('/effusion/getimage/' + nid  + '/1', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-1200 width-not-web');
		});
	  } else {
		$.getJSON('/effusion/getimage/' + nid + '/0', function(data) {
		  $('.slide-nid-'+nid).attr('src','/' + data.slide);
		  $('.slide-nid-'+nid).data('description', data.desc);
      $('.slide-nid-'+nid).data('inverted', data.inverted);
      $('.slide-nid-'+nid).removeClass('stretch');
		  $('body').addClass('width-else width-else-not-web width-not-web');
		});
	  }
  }
  doResize();
}

function changeSlide(e) {
  snid = $(e).find('.nid').text();
  changeRes(snid, 'Err');
}

$(document).ready(function () {


  if($('.vertleft ol').length) {
    x = 1;
    $('.vertleft ol li').each(function () {
      $(this).prepend('<div class="olnumber">' + x + '</div>');
      $(this).children('.olnumber').height($(this).innerHeight());
      x++;
    });
  }



   $('a.cattitle').each(function () {
    h = $(this);

    panel = $(this).next();

    if(!panel.hasClass('alwaysopen')) {
      if(panel.hasClass('opened')) {
        h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_up_black.gif) no-repeat 0px 0px');
      } else {
        h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_down_black.gif) no-repeat 0px 0px !important');
      }
    }
   });



  $('.catcontainer .projecttitle a').hover(
    function() {
      $('.thumbcontainer img').eq($('.catcontainer .projecttitle a').index($(this))).mouseover();
    },
    function() {
      $('.thumbcontainer img').eq($('.catcontainer .projecttitle a').index($(this))).mouseout();
    }
  );

  var hovered;	  
  hovered = false;
  $('#next2, #prev2').hover(
    function () {
      //alert($(this).parent().hasClass('activeSlide'));
      if(!$(this).parent().parent().hasClass('activeSlide')){
        $(this).stop().animate({
          opacity: 0.8
        }, 1000);
      }
    },
    function () {
      if(!$(this).parent().parent().hasClass('activeSlide')){
        $(this).stop().animate({
          opacity: 0.0
        }, 1000);
      } 
    }
  );


  $('.thumbcontainer img').animate({
    opacity: 0.8
  });
  $('.activeSlide img').animate({
    opacity: 1
  });

  $('.thumbcontainer img').hover(
    function () {
      //alert($(this).parent().hasClass('activeSlide'));
      if(!$(this).parent().parent().hasClass('activeSlide')){
        $(this).stop().animate({
          opacity: 1
        }, 100);
      }
    },
    function () {
      if(!$(this).parent().parent().hasClass('activeSlide')){
        $(this).stop().animate({
          opacity: 0.8
        }, 100);
      } 
    }
  );

  positionContent();

	$('#search .form-text').attr('value','Search...');
	$('#search .form-text').focus(function() {
		if($(this).attr('value') == 'Search...')
			$(this).attr('value', '');
	});
	$('#search .form-text').blur(function() {
		if($(this).attr('value') == '')
			$(this).attr('value','Search...');
	});

  $('.closed').each(function() {
    panel = $(this);
    h = panel.prev();
    panel.slideUp(500, 'easeOutExpo');
    h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_down_black.gif) no-repeat 0px 0px');
  });


  $('h2.cattitle').click(function () {
    $(this).next('a').click();
  });

  $('a.cattitle').click(
    function() {
      h = $(this);
      panel = $(this).next();
      if(!panel.hasClass('alwaysopen')) {
        $('.opened').each(function() {
          if(!$(this).hasClass('alwaysopen')) {
            $(this).removeClass('opened');
            $(this).addClass('closed');
            $(this).slideUp(500, 'easeOutExpo');
            h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_down_black.gif) no-repeat 0px 0px');
          }
        });
        $('.a-opened').each(function() {
          if(!$(this).hasClass('alwaysopen')) {
            $(this).removeClass('a-opened');
            $(this).addClass('a-closed');
          }
        });



      
        if(panel.is(':visible')) {
          panel.removeClass('opened');
          panel.addClass('closed');
          panel.slideUp(500, 'easeOutExpo');
          h.removeClass('a-opened');
          h.addClass('a-closed');
          h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_down_black.gif) no-repeat 0px 0px');
        } else {
          panel.removeClass('closed');
          panel.addClass('opened');
          panel.slideDown(500, 'easeOutExpo');
          h.removeClass('a-closed');
          h.addClass('a-opened');
          h.css('background', 'url(/sites/default/themes/zen/images/expanding_arrows/button_up_black.gif) no-repeat 0px 0px');
        }
      }

    });



	// caption
	var imgTitle;
	$('.imgcaption img').each(function(){
		imgTitle = $(this).attr('title');
		if (imgTitle) {

		} else {
			imgTitle = $(this).attr('alt');
		}
		//alert(imgTitle);

		//if (imgTitle){
			$(this).after('<span class="imgcaptiontext">'+imgTitle+'</span>');
		//}
	});

  doResize();

});

function doResize() {
	
	
	if( $('.node-type-page #slideshow, .node-type-parent-page #slideshow, .node-type-long-content #slideshow').length ){
		if( $('#slideshow img').length ){
			
			if( $('body').hasClass('logged-in') ){
					//alert( 'img ' + $('#slideshow img').height() + ' window ' + $(window).height());
			}
			
			/*
			if( $('#slideshow img').height() > $(window).height() ){
				$('#slideshow').addClass('height_100');
				$('#slideshow').removeClass('width_100');
				
				$('#slideshow').css('height', '100%'); 
				$('#slideshow').css('width', 'auto');
				$('#slideshow img').css('height', '100%');
				$('#slideshow img').css('width', 'auto');
			} else {
				$('#slideshow').addClass('width_100');
				$('#slideshow').removeClass('height_100');
	
				$('#slideshow').css('height', 'auto'); 
				$('#slideshow').css('width', '100%');
				$('#slideshow img').css('height', 'auto');
				$('#slideshow img').css('width', '100%');
			}
			*/
		} else {
			$('#slideshow').addClass('no_image');
		}
	}
	
}

function positionContent() {
  if($('.section-node-edit').length == 0 && $('.section-admin').length == 0) {
    if($(window).height() < $('#mainContent').height() + 150) {
      $('#mainContent').css('bottom', 'auto');
      $('#mainContent').css('top', '100px');
      $('#mainContent').css('position', 'static');
    }
    else {
      $('#mainContent').css('bottom', '25px');
      $('#mainContent').css('top', 'auto');
      //$('#mainContent').css('position', 'fixed');
			if($(window).width() > 1000){
	      $('#mainContent').css('position', 'fixed');
			} else {
				$('#mainContent').css('position', 'absolute');
			}
    }
  }
}

var window_resise; // = setTimeout("doTimedResize()",100);
var new_resolution = 0;
var new_resolution_height = 0;


function doTimedResize(){
	var size_change = false;
	
//	if( $('body').hasClass('logged-in') ){
		//var current_resolution = <?php print imagebg_session_resoution_get() ?>;
		//var current_resolution_height = <?php print imagebg_session_resoution_height_get() ?>;
		
		new_resolution = $(window).width();
		//screen.width
		new_resolution_height = $(window).height();
		var randomNum = Math.random()
		//if(new_resolution != current_resolution){
		if( test_in_interval(new_resolution, current_resolution, 25) ){
			
		} else {
			
			size_change = true;
			current_resolution = new_resolution;
			
			$.getJSON('/imagebg/setresolution/' + new_resolution +'?'+randomNum , function(data) {
			 
		  	//data.slide
			});
		}

		//if(new_resolution_height != current_resolution_height){
		if( test_in_interval(new_resolution_height, current_resolution_height, 25) ){
			
		} else {

			size_change = true;
			current_resolution_height = new_resolution_height;
			$.getJSON('/imagebg/setresolutionheight/' + new_resolution_height +'?'+randomNum , function(data) {
			 
		  	//data.slide
				
			});
		}
		if(size_change){
			//alert('timed changed '+new_resolution+' '+new_resolution_height+' '+current_resolution+' '+current_resolution_height+' ');
		} 
		test_image_height();
			
//	}
}

function test_image_height(){
	var corrected = false;
	if( $('.web #slideshow, .print #slideshow').length ){
		if( $('.web #slideshow').length ){

  		/*$('.web #slideshow').css('max-width', '100%');*/
			/*$('.web #slideshow img').css('max-width', '100%');*/
				$('.web #slideshow').css('height', '100%'); 
				$('.web #slideshow').css('width', 'auto');
				$('.web #slideshow img').css('height', '100%');
				$('.web #slideshow img').css('width', 'auto');
		}
		if( $('.print #slideshow').length ){
	  	/*$('.print #slideshow').css('height', '100%');
  		$('.print #slideshow').css('width', 'auto');
  		$('.print #slideshow img').css('height', '100%');
  		$('.print #slideshow img').css('width', 'auto');*/
		}
	} else {
		//alert($(window).width() + ' ' + $('#slideshow').width() );

		//if( (corrected == false) && ($(window).height() >= $('#slideshow img').attr("naturalHeight")) ){
		if( (corrected == false) && ($(window).height() > $('#slideshow img').height() ) ){
			corrected = true;
			$('#slideshow').css('height', '100%'); 
			$('#slideshow').css('width', 'auto');
			$('#slideshow img').css('height', '100%');
			$('#slideshow img').css('width', 'auto');
			//alert('height 100%');
		}
		
		//alert( $('#slideshow img').attr("naturalWidth") );					
		//if( (corrected == false) && ( $(window).width() >= $('#slideshow img').attr("naturalWidth") ) ){
		if( (corrected == false) && ( $(window).width() > $('#slideshow img').width() ) ){
			corrected = true;
			$('#slideshow').css('height', 'auto'); 
			$('#slideshow').css('width', '100%');
			$('#slideshow img').css('height', 'auto');
			$('#slideshow img').css('width', '100%');
			//alert('width 100%');
		}
		
		if( (corrected == false) && ($(window).height() == $('#slideshow').height() ) ){
			corrected = true;
			//alert('height already 100%');
		}
		
		/*if( (corrected == false) && ( $(window).width() == $('#slideshow').width() ) ){
			corrected = true;
		}
		if( (corrected == false) && ( $(window).height() == $('#slideshow').height() ) ){
			corrected = true;
		}*/
		
		if (corrected == false) {
			corrected = true;
			$('#slideshow').css('height', 'auto'); 
			$('#slideshow').css('width', '100%');
			$('#slideshow img').css('height', 'auto');
			$('#slideshow img').css('width', '100%');
		}
	}
}

function test_in_interval(x, y, interval){
	
	if(x == y){
		return true;
		//alert( 'equal ' + x + ' ' +y+ ' ' +interval );
	}
	if(x > y){
		//alert( 'X ' + x + ' ' +y+ ' ' +interval );
		if( (x - y) < interval ){
			return true;
		} else {
			return false;
		}
		
	}
	if(x < y){
		//alert( 'Y ' + x + ' ' +y+ ' ' +interval );

		if( (y - x) < interval ){
			return true;
		} else {
			return false;
		}
	}
	return false;
}

$(document).ready(function (){
	//testVerticalPage();
	
	//checkScroll();
	setTimeout("checkScroll()", 10);

});



 
function checkScroll(){
	var left_col = $('.vertright');
	var col_box = $('.vertcontent');
	//alert('bars '+left_col.length);
	if( left_col.length ){
			//alert(left_col.height());
			var col_height = left_col.height();
			if( col_height >= 523 ){
				if( col_box.height() > col_height){
					col_height = col_box.height();
					col_box.addClass('my_height');
					//alert('new '+col_height);
				} else {
					//alert(col_height);
					col_box.addClass('col_height');
				}
				left_col.addClass('fixed');
				left_col.css('height', col_height+'px');
				left_col.jScrollPane();
				//left_col.css('max-height', '');
				//alert('fix');
				//$('#content_left_inner').jScrollPane();
				
			}
	}
}

$(window).load(function () {
    
  /*$('#next2, #prev2').animate({ opacity: 0.0}, 1000);*/
  close_mainContent();
	
	minimise_mainContent();
	
  setTimeout("  $('#next2, #prev2').stop().animate({ opacity: 0.0}, 1000);", 500);
	
	window_resise = setTimeout("doTimedResize()", 100);
});

function testVerticalPage(){
	if( $('.vertleft').length ){
		if( $('.vertright').length ){
			
			left_pad = paddingAmount('.vertleft'); //60
			left_height = $('.vertleft').height() + left_pad;
			right_pad = paddingAmount('.vertright'); // 20
			right_height = $('.vertright').height() + right_pad;	
			if(left_height == right_height){
				return false;
			} else if(left_height > right_height){
				//alert('fix right' + left_height + '|' + right_height);
				$('.vertright').css("max-height", "none");
				$('.vertright').height(left_height - right_pad);
				//$('.front .vertright').height(left_height - right_pad + 10);
			} else {
				//alert('fix left' + left_height + '|' + right_height);
				$('.vertleft').height(right_height - left_pad );
			}
		} 
	} 
}

function paddingAmount($item_id){
	
	padding_top = parseInt($($item_id).css('padding-top').slice(0,-2));
	padding_bottom = parseInt($($item_id).css('padding-bottom').slice(0,-2));
	//alert(padding_top+padding_bottom);
	return padding_top+padding_bottom;
}


//var current_mainContent_height = 0;
//var current_mainContent_width = 0;
//var current_projecttabs_width = 0;
function minimise_mainContent(){

	$('#maxamise_mainContent').hide();
	$('#mainContentInner').show();
	
	
	$('#minimise_mainContent').click( function(){
		// hide the more background button
		$(this).hide()
		// show the expand button 
		$('#maxamise_mainContent').show()

		//$('.vertright').hide();
		$('.projecttabs').hide();			
		$('#mainContentInner').hide();

	});
	$('#maxamise_mainContent').click( function(){

		// hide the expand button
		$(this).hide();
		// show the more background button again
		$('#minimise_mainContent').show();

		//reload the tabs and the main content
		$('.projecttabs').show();
		$('#mainContentInner').show();

	});
	

	
}
var slide_timer;
function close_mainContent(){
	
	$('#open_mainContent_wrapper').fadeTo(0,0);
	$('#open_mainContent_wrapper').stop().animate({
          opacity: 0.0
  }, 0);
	$('#open_mainContent_wrapper').removeClass('start_hidded');
	$('#close_mainContent').click(function(){
		//$('#mainContent').hide();
		
		$('#mainContent').stop().animate({
          opacity: 0.0
        }, 750);
		$('#open_mainContent_wrapper').stop().animate({
          opacity: 1
  	}, 750, 'linear', function(){
					$('#open_mainContent_wrapper').animate({
	          opacity: 0.6
  				}, 750);
		});
		
		if( $('#next2').length ){
			restSlideControls('')
			slide_timer = setInterval("$('#next2').click();",4000);	
		}
		
		
	});
	
	$('#open_mainContent').click(function(){
			clearInterval(slide_timer);	
			restSlideControls('');
			$('#open_mainContent_wrapper').stop().animate({
          opacity: 0.0
		  }, 150);	
			$('#mainContent').stop().animate({
          opacity: 1.0
      }, 750);
	});
	
	
	$('#slide_stop').click(function(){
		restSlideControls('#slide_stop');															
		$('#open_mainContent').click();															
		return false;
	});
	$('#slide_pause').click(function(){
			if( $(this).hasClass('active') ){
				$('#slide_play').click();
			} else {														 
				restSlideControls('#slide_pause');
				clearInterval(slide_timer);														 
			}
			return false;
	});
	$('#slide_slow').click(function(){
			if( $(this).hasClass('active') ){
				$('#slide_pause').click();
			} else {														 
				restSlideControls('#slide_slow');
				clearInterval(slide_timer);	
				slide_timer = setInterval("$('#next2').click();",8000);															
			}
			return false;
	});
	$('#slide_play').click(function(){
			if( $(this).hasClass('active') ){
				$('#slide_pause').click();
			} else {
				restSlideControls('#slide_play');
				clearInterval(slide_timer);															
				slide_timer = setInterval("$('#next2').click();",4000);															
			}
			return false;
	});
	$('#slide_fast').click(function(){
			if( $(this).hasClass('active') ){
				$('#slide_pause').click();
			} else {														 
				restSlideControls('#slide_fast');													
				clearInterval(slide_timer);	
				slide_timer = setInterval("$('#next2').click();",2000);					
			}
			return false;
	});
																				 
	
}

function restSlideControls(item_id){
	$('#slide_controls a').removeClass('active');
		
	if( item_id != "" ){
		$(item_id).addClass('active');
	} else {
		$('#slide_play').addClass('active');
	}
}


$(window).resize(function() {
	
	clearTimeout(window_resise);
	window_resise = setTimeout("doTimedResize()",50);
	
  doResize();
	
  positionContent();
  //alert($(window).height());
});


