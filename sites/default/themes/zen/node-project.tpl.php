<?php
// $Id: node.tpl.php,v 1.4.2.1 2009/05/12 18:41:54 johnalbin Exp $

/**
 * @file node.tpl.php
 *
 * Theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: Node body or teaser depending on $teaser flag.
 * - $picture: The authors picture of the node output from
 *   theme_user_picture().
 * - $date: Formatted creation date (use $created to reformat with
 *   format_date()).
 * - $links: Themed links like "Read more", "Add new comment", etc. output
 *   from theme_links().
 * - $name: Themed username of node author output from theme_user().
 * - $node_url: Direct url of the current node.
 * - $terms: the themed list of taxonomy term links output from theme_links().
 * - $submitted: themed submission information output from
 *   theme_node_submitted().
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $teaser: Flag for the teaser state.
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */

/* */
?>

 <?php
      $view = views_get_view('childs');
      $view->set_arguments(array($node->nid));
      $view->execute_display('default');

      $firstnode = node_load($view->result[0]->nid);
?>

<h2 class="univers14"><?php print $node->title ?></h2>
<div class="projecturl">
  <?php
    if($node->field_type[0]['value'] == 'Web') {
      if($node->field_url[0]['value'] != '')
        print '<a class="btnvisit" href="'.$node->field_url[0]['value'] .'" target="_blank">&nbsp;</a>';
      else
        print '<div class="btnnovisit">&nbsp;</div>';
    }
  ?>

</div>
<div class="projectdetails">
  <div class="projectdescription inlineleft"><span class="projectdescriptioninner"><?php 
	print strip_tags($firstnode->body); 
	
	?></span>
  <?php
    $cpath = drupal_get_path_alias($_GET['q']);
    $cpath = explode('/', $cpath);
    array_pop($cpath);
    $cpath = implode($cpath);
  ?>
    <p><a href="/<?php print $cpath ?>" class="bkbtn">Back</a></p>
  </div>
    <div id="nav" class="projectthumbs projectthumbs_new inlineright">
    <?php
	//print '<div class="clearboth"></div>';
		$edit_links = '';
		$link_counter = 0;
      foreach($view->result as $r) {
        $thumb = node_load($r->nid);

        if($thumb->field_images[0]['filepath'] != '')
          $p = $thumb->field_images[0]['filepath'];
        else
          $p = 'sites/default/files/imagefield_default_images/default.jpg';

        print '<div class="thumbcontainer inlineleft thumbcontainer_'.$link_counter.' thumbcontainer_5_'.($link_counter%5).' "><a href="#" class="thumblink">';

        if($thumb->field_thumb[0]['filepath'] != '')
          print '<img class="imagecache imagecache-slidethumb dummy-imagecache" src="/'.$thumb->field_thumb[0]['filepath'].'" alt="'.$thumb->title.'" title="'.$thumb->title.'" />';
        else
          print theme('imagecache', 'slidethumb_new', $p, $thumb->title, $thumb->title);
        print '<div class="nid">'.$thumb->nid.'</div></a>';
		global $user;
		//krumo($user);
		if($user->uid == 1){
			//http://effusion.co.uk/admin_menu/flush-cache?destination=node%2F185
	        $edit_links .= '<a href="/node/'.$r->nid.'/edit?destination=node/'.$node->nid.'" class="special-edit">Edit_'.$r->nid.'</a> ';
		}
		
		print '</div>';
		$link_counter++;
      }
      print '<div class="clearboth"></div>';


    ?>
  </div>
  <div class="clearboth"></div>
  <?php print $edit_links; ?>
</div>


<script type="text/javascript">
  var current_slide = 1;	
  $(document).ready(function () {


    $('.thumbcontainer img:first').animate({
      opacity: 1
    });

    if($('body').hasClass('print')) {
      $('.menu-3631, .menu-3631 a').addClass('active');
    }
    if($('body').hasClass('web')) {
      $('.menu-3630, .menu-3630 a').addClass('active');
    }
    if($('body').hasClass('identity')) {
      $('.menu-3963, .menu-3963 a').addClass('active');
    }
    if($('body').hasClass('miscellaneous')) {
      $('.menu-3632, .menu-3632 a').addClass('active');
    }
  });
    
</script>