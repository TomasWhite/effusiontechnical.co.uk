<?php


$include_string = 20;//rand(0, 9999);

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Original, innovative and creative designer and web developer</title>
<link type="image/x-icon" href="/sites/default/files/zen_favicon_0.ico" rel="shortcut icon">
<link href="/holding/holding.css?<?php print $include_string; ?>" media="all" rel="stylesheet" type="text/css">
<link href="/holding/holding_print.css?<?php print $include_string; ?>" media="print" rel="stylesheet" type="text/css">
<script src="/misc/jquery.js?<?php print $include_string; ?>" type="text/javascript"></script>
<script src="/holding/holding.js?<?php print $include_string; ?>" type="text/javascript"></script>

</head>
<body>
<div id="box-outer">
    <div id="image">
	    <!--div class="grid">&nbsp;</div-->
    	<div class='imgbox image-current' id="imgbox0" ><img id="img0" class='image-current' src="/holding/images/Effusion_holding_page.jpg" alt="Sugar Loaf Walk" style="opacity: 1;"/></div>
    </div>
    
        <div id="box-wrapper"><div id="box">
	    <div id="box-inner">
            <h1>Effusion</h1>
            <h2 id='same'></h2>
            <p class="para" id="same-1">We’re a full service design agency, tucked away in deepest Bethnal Green, working across sectors and disciplines to produce stunningly creative things in print, online and elsewhere. And, of course, we work for a variety of public, third sector and private clients.</p>
            
            <h2 id='different'></h2>
            <p class="para" id="different-1">We do one thing that you might not understand. We charge less; because we can and not because we have to.
            We offer unimaginably good value because we believe it’s the way things should be. We think things would work better if more people thought the same way.</p>

            <h2 id='contact_2'>Contact</h2>
            <p class="para" id="address">
            	18 Victoria Park Square, 	
            	Bethnal Green, 	
            	London E2 9PF, 	
            	+44 [0]20 8880 6315
            </p>   
	        <p class="para" id="email-address">
 				<a href="mailto:contact@effusion.co.uk">contact@effusion.co.uk</a>
            </p>
            <p class="para" id="portfolio"><a href='/holding/effusionportfolio.pdf'>Portfolio</a></p>

        </div>
    </div></div>
</div>
<div id="print-box">
	<img src="/holding/images/print.gif" alt="Image of the holding page for printing"/>
</div>
</body>
</html>